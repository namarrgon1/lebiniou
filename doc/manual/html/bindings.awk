function pmod(mod) {
    if (mod == "-")
	return;
    if (mod == "A")
	return "Alt-";
    if (mod == "C")
	return "Ctrl-";
    if (mod == "S")
	return "Shift-";
    if (mod == "CS")
	return "Ctrl-Shift-";
    if (mod == "AS")
	return "Alt-Shift-";
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "*") || ($1 == "-"))
	next;

    printf "__%s__|**[%s%s]**\n", $1, pmod($2), $3;
}
