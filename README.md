# [Le Biniou](https://biniou.net)

[![pipeline status](https://gitlab.com/lebiniou/lebiniou/badges/master/pipeline.svg)](https://gitlab.com/lebiniou/lebiniou/commits/master)

# Pre-built packages

Packages exist for various GNU/Linux distributions:

 * [Debian](https://tracker.debian.org/pkg/lebiniou), [Ubuntu](https://packages.ubuntu.com/search?keywords=lebiniou&searchon=names&suite=all&section=all)
 * [Arch Linux](https://aur.archlinux.org/packages/lebiniou)

# Building from sources

## GNU/Linux-based systems

  1. Install required dependencies

### Debian-based distributions

  ```sh
  sudo apt-get -qq update
  sudo apt-get -qq install autoconf pkg-config gcc make libglib2.0-dev libfftw3-dev libxml2-dev libfreetype6-dev libswscale-dev libsdl2-ttf-dev libcaca-dev libjack-dev pandoc libsndfile1-dev libmagickwand-dev libjansson-dev libulfius-dev
  ```

  2. Configure, compile and install

  The configure script has several [build options](BUILD.md).

  ```sh
  autoreconf -fi
  ./configure
  make
  sudo make install
  ```

  3. Follow the same steps for [lebiniou-data](https://gitlab.com/lebiniou/lebiniou-data) package, then

  4. Run

  ```sh
  lebiniou
  ```

  5. Get more options
  ```sh
  lebiniou --help
  man lebiniou
  ```

## Arch Linux

```sh
pacman --noconfirm -Sy autoconf automake grep pkg-config gcc make pandoc fftw libxml2 freetype2 libmagick6 ffmpeg sdl2_ttf libcaca libffi
autoreconf -i
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/lib/imagemagick6/pkgconfig"
export PATH="$PATH:/usr/bin/core_perl"
./configure
make
sudo make install
```

## BSD-based systems

If you want to build the [documentation](https://biniou.net/manual.html),
make sure you have the [pandoc](https://pandoc.org) package installed:
```sh
pandoc --version
pandoc 2.2
```

1. Fetch dependencies

* FreeBSD (12.0)
  ```sh
  pkg install autoconf automake pkgconf glib fftw3 libxml2 ffmpeg sdl2_ttf libcaca jackit ImageMagick
  ```

* NetBSD (8.0)
  ```sh
  pkg_add autoconf automake pkg-config glib2 fftw libxml2 ffmpeg4 SDL2_ttf libcaca jack ImageMagick
  ```

* OpenBSD (6.5)
  ```sh
  pkg_add gcc-8.3.0 glib2 fftw3 libxml ffmpeg sdl2-ttf libcaca jack ImageMagick
  ```

2. Configure, compile and install
  ```sh
  autoreconf -fi
  ./configure
  # for OpenBSD
  CC=/usr/local/bin/egcc ./configure
  make
  make install
  ```

# Running a local build

Make sure you have the data files at least downloaded !

* if they are installed
```sh
LD_LIBRARY_PATH=$PWD/src ./src/lebiniou -b plugins/stable
```
* else
```sh
LD_LIBRARY_PATH=$PWD/src ./src/lebiniou -b plugins/stable -d /path/to/datafiles
```

# Support

Join us on [IRC](irc://freenode/biniou) for general questions.
