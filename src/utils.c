/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"


void
xerror(const char *fmt, ...)
{
  va_list ap;

  fprintf(stderr, "O_o ");
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);

#ifdef DEBUG
  assert(0); /* to get a backtrace */
#else
  exit(1);
#endif
}


void
xperror(const char *why)
{
  fprintf(stderr, "[!] System error: ");
  perror(why);
  exit(1);
}


void
okdone(const char *what)
{
  if (libbiniou_verbose) {
    printf("[+] %s\n", what);
  }
}


void *
xmalloc(const size_t size)
{
  void *pouet = malloc(size);

  if (NULL == pouet) {
    xperror("malloc");
  }

  return pouet;
}


void *
xcalloc(const size_t nmemb, const size_t size)
{
  void *pouet = calloc(nmemb, size);

  if (NULL == pouet) {
    xperror("calloc");
  }

  return pouet;
}


void *
xrealloc(void *ptr, size_t size)
{
  void *pouet = realloc(ptr, size);

  if (NULL == pouet) {
    xperror("realloc");
  }

  return pouet;
}


double
xatof(const char *value)
{
  double f;

  errno = 0;
  f = strtod(value, NULL);
  if (errno != 0) {
    xperror("strtod");
  }

  return f;
}


long
xatol(const char *value)
{
  long l;

  errno = 0;
  l = strtol(value, NULL, 10);
  if (errno != 0) {
    xperror("strtol");
  }

  return l;
}


static void
rmkdir2(const char *dir)
{
  const mode_t omode = S_IRWXU|S_IRWXG|S_IRWXO;

  if (access(dir, R_OK) == -1) {
    if (mkdir(dir, omode) == -1) {
      xperror("mkdir");
    }
  }
}


void
rmkdir(const char *p)
{
  /* recursive mkdir */
  char *slash = NULL;
  char *path;

  assert(NULL != p);
  path = strdup(p);

  if (path[0] == '/') {
    slash = strchr(path+sizeof(char), '/');
  } else {
    slash = strchr(path, '/');
  }

  while (NULL != slash) {
    *slash = '\0';
    rmkdir2(path);
    *slash = '/';
    slash = strchr(slash+sizeof(char), '/');
  }
  rmkdir2(path);

  xfree(path);
}


uint32_t
FNV_hash(const char* str)
{
  const unsigned int fnv_prime = 0x811C9DC5;
  unsigned int hash = 0;
  int c;

  while ((c = *str++)) {
    hash *= fnv_prime;
    hash ^= c;
  }

  return (uint32_t)hash;
}


void
ms_sleep(const u_long msec)
{
#if 0 /* nanosleep seems NOK */
  struct timespec ts;

  ts.tv_sec = (msec / 1000);
  ts.tv_nsec = (msec % 1000) * 1000000;

  nanosleep(&ts, NULL);
#else
  struct timeval tv;

  tv.tv_sec = (msec / 1000);
  tv.tv_usec = (msec % 1000) * 1000;

  select(0, 0, 0, 0, &tv);
#endif
}


/*!
 * Parses a string containing 2 shorts
 * eg "960x540" => 960, 540 (for resolution)
 * "15,30" => 15, 30 (for timers)
 */
int
parse_two_shorts(const char *str, const int delim, short *a, short *b)
{
  int rc = 0;
  char *dup = NULL;
  char *found = NULL;
  long a0, b0;

  if ((NULL == a) && (NULL == b)) {
    xerror("%s: No variable to set !\n", __FUNCTION__, a, b);
  }

  dup = strdup(str);
  if (NULL == dup) {
    xperror("strdup");
  }

  found = strchr(str, delim);
  if (NULL == found) {
    xerror("%s: Did not find delimiter '%c' in \"%s\"\n", __FUNCTION__, delim, str);
  }
  *found++ = '\0';

  if (NULL != a) {
    a0 = xatol(dup);
    if ((a0 >= SHRT_MIN) && (a0 <= SHRT_MAX)) {
      *a = a0;
    } else {
      rc = -1;
    }
  }

  if (NULL != b) {
    b0 = xatol(found);
    if ((b0 >= SHRT_MIN) && (b0 <= SHRT_MAX)) {
      *b = b0;
    } else {
      rc = -1;
    }
  }

  xfree(dup);

  return rc;
}


/* returns 0 if command was executed, -1 otherwise */
int
check_command(const char *cmd)
{
  if (NULL != cmd) {
    int res = system(cmd);
    if (!res) {
      return 0;
    }
  }
  return -1;
}


time_t
unix_timestamp()
{
  struct timeval t;

  gettimeofday(&t, NULL);
  return t.tv_sec;
}
