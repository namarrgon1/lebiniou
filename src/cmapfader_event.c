/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmapfader.h"


int
CmapFader_event(CmapFader_t *cf, const Event_t *e)
{
  switch (e->cmd) {
    case BC_SELECT:
      if (e->arg0 == BA_PREV) {
        CmapFader_prev(cf);
        return 1;
      } else if (e->arg0 == BA_NEXT) {
        CmapFader_next(cf);
        return 1;
      } else if (e->arg0 == BA_RANDOM) {
        CmapFader_random(cf);
        return 1;
      }
      break;

    default:
      break;
  }

  return 0;
}
