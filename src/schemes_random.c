/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "schemes.h"
#include "globals.h"
#include "brandom.h"

// number of times to get a random sequence including the locked plugin
#define LOCKED_MAX_TRIES 1000 // max: uint16_t


void
Schemes_random(Context_t *ctx)
{
  Sequence_t *new = ctx->sm->next;
  u_short random;
  char ok;
  uint16_t try = 0;

  if ((NULL == schemes) || !schemes->size) {
    printf("[!] No schemes available, leaving unchanged\n");
    return;
  }

  do {
    int i;
    struct timeval t;

    gettimeofday(&t, NULL);

    ok = 1;
    if ((NULL != schemes->shuffler) && !Shuffler_ok(schemes->shuffler)) {
      printf("[!] Can not create a scheme, leaving unchanged\n");
      return;
    }

    Sequence_clear(new, t.tv_sec);

    random = (NULL != schemes->shuffler) ? Shuffler_get(schemes->shuffler) : 0;
    for (i = 0; ok && schemes->schemes[random][i].type; i++) {
      /* check if we should insert a lens or a normal plugin */
      if (drand48() <= schemes->schemes[random][i].p) {
        const int not_lens = ((short)schemes->schemes[random][i].type < 0) ? 1 : 0;
        const int res = Context_add_rand(new, (enum PluginOptions)(schemes->schemes[random][i].type), not_lens, ctx->locked);

        /* if ((res == -1) && (schemes->schemes[random][i].p == 1.0)) { */
        /* this is the correct way to check with floats: */
        if ((res == -1) && (fabs(schemes->schemes[random][i].p - 1.0) <= FLT_EPSILON)) {
          /* could not find a mandatory plugin */
#ifdef DEBUG
          printf("[!] Marking scheme %d as invalid.\n", random);
#endif
          if (NULL != schemes) {
            Shuffler_disable(schemes->shuffler, random);
          }
          ok = 0;
        }
      }
    }

    if (ok) {
      if (NULL != ctx->locked) {
        try++;
#ifdef DEBUG
        fprintf(stderr, "=== Checking for locked plugin '%s' (try: %d)... ", ctx->locked->name, try);
#endif
        if (Sequence_find_position(new, ctx->locked) == -1) {
#ifdef DEBUG
          fprintf(stderr, "NOT FOUND... ");
#endif
          if (try == LOCKED_MAX_TRIES) {
#ifdef DEBUG
              fprintf(stderr, "max tries (%d) reached, giving up !\r\n", LOCKED_MAX_TRIES);
#endif
            } else {
#ifdef DEBUG
            fprintf(stderr, "Retrying...\r\n");
#endif
            ok = 0;
          }
        } else {
#ifdef DEBUG
          fprintf(stderr, "FOUND\r\n");
#endif
        }
      } else {
#ifdef DEBUG
        fprintf(stderr, "=== No plugin locked\r\n");
#endif
      }
    }

  } while (!ok);

  Context_randomize(ctx); /* TODO rajouter auto_colormaps / auto_images dans les schemes */
  Context_set(ctx);
}
