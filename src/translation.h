/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_TRANSLATION_H
#define __BINIOU_TRANSLATION_H

/*
 * Translation effects come from Cthugha
 * http://www.afn.org/~cthugha/
 * background computing and fading of new translations
 * added by oliv3
 */
/* TODO add an Alarm to restart a new translation */

#include "biniou.h"


/* -- Map -- */
typedef struct Map_s {
  short map_x, map_y;
} Map_t;


/* -- wPoint -- */
typedef struct M_wPoint_s {
  float x, y;
  float dx, dy;
} M_wPoint_t;


/* -- Translation -- */
typedef struct Translation_s {
  Map_t      (*f)(const short, const short);
  M_wPoint_t *point;
  int        line;
  u_char     fading;
  void       (*init)();
} Translation_t;


Translation_t *Translation_new(Map_t (*)(const short, const short), void (*)());
void   Translation_delete(Translation_t *);

int    Translation_run(Translation_t *, const Context_t *);
void   Translation_batch_init(Translation_t *);
u_char Translation_batch_line(Translation_t *);

u_char Translation_batch_done(const Translation_t *);

#endif /* __BINIOU_TRANSLATION_H */
