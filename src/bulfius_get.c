/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"


int
callback_get_parameters(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const struct _u_map *u_map = request->map_url;
  Context_t *ctx = user_data;
  assert(NULL != ctx);

  if (u_map_has_key(u_map, "name")) {
    Plugin_t *p = Plugins_find(u_map_get(u_map, "name"));
    json_t *res = NULL;

    if (NULL != p) {
      if (NULL != p->parameters) {
        res = p->parameters(ctx, NULL);
        if (u_map_has_key(u_map, "param")) {
          const char *param_name = u_map_get(u_map, "param");
          json_t *new_res = NULL;

          json_t *param = json_object_get(res, param_name);
          if (NULL != param) {
            new_res = json_object();
            json_object_set(new_res, param_name, json_object_get(param, "value"));

            assert(NULL != new_res);
            ulfius_set_json_body_response(response, 200, new_res); // value
            json_decref(res);
            res = new_res;
          } else {
            ulfius_set_string_body_response(response, 404, "Parameter not found");
          }
        } else {
          ulfius_set_json_body_response(response, 200, res); // all parameters
        }
      } else {
        ulfius_set_json_body_response(response, 200, res); // no parameters
      }
      json_decref(res);
    } else {
      ulfius_set_string_body_response(response, 404, "Plugin not found");
    }
  } else {
    ulfius_set_string_body_response(response, 400, "Bad request");
  }

  return U_CALLBACK_COMPLETE;
}


int
callback_get_sequence(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = user_data;

  assert(NULL != ctx);
  json_t *payload = Sequence_to_json(ctx->sm->cur, 1);
  ulfius_set_json_body_response(response, 200, payload);
  json_decref(payload);

  return U_CALLBACK_COMPLETE;
}


int
callback_get_statistics(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = user_data;

  assert(NULL != ctx);
  json_t *payload = json_object();
  json_object_set_new(payload, "frames", json_integer(ctx->frames));
  if (NULL != ctx->input) {
    json_object_set_new(payload, "volume", json_real(ctx->input->volume));
  }
  ulfius_set_json_body_response(response, 200, payload);
  json_decref(payload);

  return U_CALLBACK_COMPLETE;
}
