/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "imagefader.h"


int
ImageFader_event(ImageFader_t *pf, const Event_t *e)
{
  switch (e->cmd) {
    case BC_SELECT:
      if (e->arg0 == BA_PREV) {
        ImageFader_prev(pf);
        return 1;
      } else if (e->arg0 == BA_NEXT) {
        ImageFader_next(pf);
        return 1;
      } else if (e->arg0 == BA_RANDOM) {
        ImageFader_random(pf);
        return 1;
      }

    default:
      break;
  }

  return 0;
}
