/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "btimer.h"
#include "utils.h"

#define GETTIME(v) do { gettimeofday(&v, NULL); } while (0)

BTimer_t *
b_timer_new(void)
{
  BTimer_t *timer;

  timer = xcalloc(1, sizeof(BTimer_t));
  GETTIME(timer->start);
  return timer;
}


void
b_timer_delete(BTimer_t *timer)
{
  xfree(timer);
}


void
b_timer_start(BTimer_t *timer)
{
  GETTIME(timer->start);
}


void
b_timer_stop(BTimer_t *timer)
{
  GETTIME(timer->end);
}


void
b_timer_restart(BTimer_t *timer)
{
  b_timer_stop(timer);
  b_timer_start(timer);
}


float
b_timer_elapsed(BTimer_t *timer)
{
  struct timeval elapsed;

  GETTIME(timer->end);
  if (timer->start.tv_usec > timer->end.tv_usec) {
    timer->end.tv_usec += 1000000;
    timer->end.tv_sec--;
  }
  elapsed.tv_usec = timer->end.tv_usec - timer->start.tv_usec;
  elapsed.tv_sec = timer->end.tv_sec - timer->start.tv_sec;

  return (float)(elapsed.tv_sec + ((float)elapsed.tv_usec / 1e6));
}
