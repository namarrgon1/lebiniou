/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SCHEMES_H
#define __BINIOU_SCHEMES_H

#include "shuffler.h"
#include "context.h"


typedef struct SchemeItem_s {
  float              p;
  enum PluginOptions type;
} SchemeItem_t;


typedef struct Schemes_s {
  SchemeItem_t **schemes;
  u_short      size;
  Shuffler_t   *shuffler;
} Schemes_t;


void Schemes_new(const char *);
void Schemes_new_default();
void Schemes_delete();

void Schemes_random(Context_t *);

#endif /* __BINIOU_SCHEMES_H */
