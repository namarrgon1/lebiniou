/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_PATHS_H
#define __BINIOU_PATHS_H

#define RADIUS_MAX 10.0f

typedef struct Path_point_s {
  float x;       /* between 0.0f and 1.0f */
  float y;       /* between 0.0f and 1.0f */
  float z;       /* between 0.0f and 1.0f */
  float connect; /* 1.0f if connect to previous point or 0.0f */
  float radius;  /* between 0.0f and RADIUS_MAX, in pixel */
  float c;
} Path_point_t;

typedef struct Path_s {
  uint32_t      id;       /* our unique hash */
  char         *name;     /* path name */
  char         *filename; /* where on the filesystem it was loaded from */
  uint16_t      size;     /* number of points */
  Path_point_t *data;     /* points data */
} Path_t;

typedef struct Paths_s {
  Path_t **paths;
  uint16_t size;
} Paths_t;

extern Paths_t *paths;
char **paths_list;

void Paths_new(const char *);
void Paths_delete();
int Path_event(const Event_t *e);
void Path_scale_and_center(Path_point_t *dst, Path_point_t *src, uint32_t size, float user_scale);

#endif /* __BINIOU_PATHS_H */
