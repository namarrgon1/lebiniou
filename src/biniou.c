/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "biniou.h"
#include "sequence.h"
#include "brandom.h"
#include "globals.h"
#include "images.h"
#include "colormaps.h"
#include "paths.h"


/* Auto-change timers and modes */
static char *delay_names[MAX_TIMERS] = {
  "colormaps",
  "images",
  "sequences"
#ifdef WITH_WEBCAM
  , "webcams"
#endif
};

static int delays[MAX_TIMERS][2] = {
  { DELAY_MIN, DELAY_MAX },  // colormaps
  { DELAY_MIN, DELAY_MAX },  // images
  { DELAY_MIN, DELAY_MAX }   // sequences
#ifdef WITH_WEBCAM
  , { DELAY_MIN, DELAY_MAX } // webcams
#endif
};

uint8_t start_with_first_sequence = 0;

static pthread_t images_thread;
static struct Images_thread_args {
  char *buf;
  const char *themes;
} images_thread_args;


static void *
biniou_images_thread(void *_args)
{
  struct Images_thread_args *args = (struct Images_thread_args *)_args;

  Images_new(args->buf, args->themes);
  g_free(args->buf);

  return NULL;
}


static pthread_t colormaps_thread;
static struct Colormaps_thread_args {
  char *buf;
} colormaps_thread_args;


static void *
biniou_colormaps_thread(void *_args)
{
  struct Colormaps_thread_args *args = (struct Colormaps_thread_args *)_args;

  Colormaps_new(args->buf);
  g_free(args->buf);

  return NULL;
}


static pthread_t paths_thread;
static struct Paths_thread_args {
  char *buf;
} paths_thread_args;


static void *
biniou_paths_thread(void *_args)
{
  struct Paths_thread_args *args = (struct Paths_thread_args *)_args;

  Paths_new(args->buf);
  g_free(args->buf);

  return NULL;
}


void
biniou_new(const char *datadir, const char *pluginsdir,
           const char *schemes, const char *themes,
           const u_char options,
           const uint32_t input_size,
           const int webcams)
{
  if (options & B_INIT_VERBOSE) {
    libbiniou_verbose = 1;
  }

  if (NULL != datadir) {
    VERBOSE(printf("[+] Loading data files\n"));

    /* Images */
    images_thread_args.buf = g_strdup_printf("%s/images/", datadir);
    images_thread_args.themes = themes;
    pthread_create(&images_thread, NULL, biniou_images_thread, (void *)&images_thread_args);

    /* Colormaps */
    colormaps_thread_args.buf = g_strdup_printf("%s/colormaps/", datadir);
    pthread_create(&colormaps_thread, NULL, biniou_colormaps_thread, (void *)&colormaps_thread_args);

    /* Paths */
    paths_thread_args.buf = g_strdup_printf("%s/paths/", datadir);
    pthread_create(&paths_thread, NULL, biniou_paths_thread, (void *)&paths_thread_args);

    pthread_join(images_thread, NULL);
    pthread_join(colormaps_thread, NULL);
    pthread_join(paths_thread, NULL);
  } else {
    Colormaps_new(NULL);
  }

  /* Create images fader and timer */
  if (NULL != images) {
    VERBOSE(printf("[+] Creating images fader (%i images)\n", images->size));
    context->imgf = ImageFader_new(images->size);

    VERBOSE(printf("[+] Creating images timer (%d..%d)\n", delays[BD_IMAGES][0], delays[BD_IMAGES][1]));
    context->a_images = Alarm_new(delays[BD_IMAGES][0], delays[BD_IMAGES][1]);
  }

  /* Create colormaps fader and timer */
  if (NULL != colormaps) {
    VERBOSE(printf("[+] Creating colormaps fader (%i colormaps)\n", colormaps->size));
    context->cf = CmapFader_new(colormaps->size);

    VERBOSE(printf("[+] Creating colormaps timer (%d..%d)\n", delays[BD_COLORMAPS][0], delays[BD_COLORMAPS][1]));
    context->a_cmaps = Alarm_new(delays[BD_COLORMAPS][0], delays[BD_COLORMAPS][1]);
  }

  /* Create sequences timer */
  VERBOSE(printf("[+] Creating sequences timer (%d..%d)\n", delays[BD_SEQUENCES][0], delays[BD_SEQUENCES][1]));
  context->a_random = Alarm_new(delays[BD_SEQUENCES][0], delays[BD_SEQUENCES][1]);
  context->random_mode = BR_NONE;

#ifdef WITH_WEBCAM
  /* Create webcams timer */
  if (webcams > 1) {
    VERBOSE(printf("[+] Creating webcams timer (%d..%d)\n", delays[BD_WEBCAMS][0], delays[BD_WEBCAMS][1]));
    context->a_webcams = Alarm_new(delays[BD_WEBCAMS][0], delays[BD_WEBCAMS][1]);
    Alarm_init(context->a_webcams);
  }
#endif

  /* Schemes */
  if (options & B_INIT_SCHEMES) {
    VERBOSE(printf("[+] Loading schemes from %s\n", schemes));
    Schemes_new(schemes);
  } else {
    VERBOSE(printf("[+] Creating default scheme\n"));
    Schemes_new_default();
  }

  /* Plugins */
  VERBOSE(printf("[+] Loading plugins\n"));
  plugins = Plugins_new(pluginsdir);
  Plugins_load(plugins, context);

  /* Sequences */
  if (options & B_INIT_SEQUENCES) {
    VERBOSE(printf("[+] Loading sequences\n"));
    Sequences_new();
  }

  VERBOSE(printf("[+] Biniou initialized\n"));
}


/*!
 * Starts the biniou
 */
void
biniou_start()
{
  GSList *outputs = context->outputs;

  Plugins_init(plugins);

  if ((NULL == sequences) || !sequences->size) {
    /* Random boot sequence */
    VERBOSE(printf("[i] No user sequences found\n"));
    Schemes_random(context);
  } else {
    GList *item;
    Sequence_t *start;

    context->sm->curseq = item = (start_with_first_sequence ? g_list_first(sequences->seqs) : g_list_last(sequences->seqs));
    start = (Sequence_t *)item->data;

    Sequence_copy(context, start, context->sm->next);
    Shuffler_used(sequences->shuffler, 0);
  }

  assert(NULL != context->sm->cur);
  assert(NULL != context->sm->next);
  Context_set(context);

  /* XXX */
  for ( ; NULL != outputs; outputs = g_slist_next(outputs)) {
    Plugin_t *output = (Plugin_t *)outputs->data;
    if (NULL != output->fullscreen) {
      output->fullscreen(context->fullscreen);
    }
  }
}


void
biniou_delete()
{
  if (NULL == context) {
    xerror("biniou_end() called but context is NULL\n");
  } else {
    context->running = 0;  /* This will stop plugins running as threads */
  }

  VERBOSE(printf("[+] Freeing context\n"));
  Context_delete(context);

  VERBOSE(printf("[+] Freeing schemes\n"));
  Schemes_delete();

  VERBOSE(printf("[+] Freeing sequences\n"));
  Sequences_free();

  VERBOSE(printf("[+] Freeing plugins\n"));
  Plugins_delete(plugins);

  if (NULL != images) {
    VERBOSE(printf("[+] Freeing %d images(s)\n", images->size));
    Images_delete(images);
  }

  if (NULL != colormaps) {
    VERBOSE(printf("[+] Freeing %d colormap(s)\n", colormaps->size));
    Colormaps_delete(colormaps);
  }

  if (NULL != paths) {
    VERBOSE(printf("[+] Freeing %d paths(s)\n", paths->size));
    Paths_delete();
  }

  VERBOSE(printf("[+] Freeing PRNG\n"));
  b_rand_free();
}


void
biniou_loop()
{
  biniou_go(0);
}


void
biniou_run()
{
  biniou_start();
  biniou_loop();
}


int
biniou_stop()
{
  if (NULL == context) {
    return 0;
  } else {
    context->running = 0;

    return 1;
  }
}


void
biniou_load_input(const char *dir, const char *name, const double volume_scale)
{
  Plugin_t *p = NULL;

  p = Plugin_new(dir, name, PL_INPUT);
  if (NULL != p) {
    if (Plugin_init(p)) {
      context->input_plugin = p;
      Context_set_volume_scale(context, volume_scale);
    } else {
      xerror("Failed to initialize %s input_n", name);
    }
  }
}


void
biniou_set_input(Input_t *input)
{
  assert(NULL != context);
  context->input = input;
}


void
biniou_load_output(const char *dir, const char *name)
{
  Plugin_t *p = NULL;
  gchar **tokens, **output;

  assert(NULL != context);

  tokens = g_strsplit(name, ",", 0);
  output = tokens;

  for ( ; NULL != *output; output++) {
    p = Plugin_new(dir, *output, PL_OUTPUT);
    if (NULL != p) {
      if (Plugin_init(p)) {
        context->outputs = g_slist_prepend(context->outputs, p);
      } else {
        xerror("Failed to initialize %s output\n", name);
      }
    }
  }

  g_strfreev(tokens);
}


void
biniou_set_full_screen(const u_char full_screen)
{
  assert(NULL != context);
  context->fullscreen = full_screen;
}


void
biniou_set_max_fps(const u_short fps)
{
  Context_set_max_fps(context, fps);
}


void
biniou_verbosity(const u_char v)
{
  libbiniou_verbose = v;
}


void
biniou_set_random_mode(const enum RandomMode r)
{
  Context_set_engine_random_mode(context, r);
}


void
biniou_go(const u_long nb_frames)
{
  u_long frames = nb_frames;

  while (context->running && ((nb_frames == 0) || frames)) {
    Context_run(context);

    if (nb_frames != 0) {
      frames--;
      if (!frames) {
        context->running = 0;
      }
    }
  }
}


void
biniou_set_delay(const enum RandomDelays what, const int min, const int max)
{
  if ((max <= min) || (min < 1) || (max < 1)) {
    xerror("Set random delay for %s: max (%d) must be > min (%d), both values must be > 1\n",
           delay_names[what], max, min);
  }

#ifdef DEBUG
  printf("[i] Setting min..max delays for %s: %d..%d\n", delay_names[what], min, max);
#endif
  delays[what][0] = min;
  delays[what][1] = max;
}
