/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_IMAGE_FILTER_H
#define __BINIOU_IMAGE_FILTER_H

#include "buffer_8bits.h"

enum FilterType { FT_GENERIC = 0,
                  FT_BLUR1_3x3,
                  FT_BLUR2_3x3,
                  FT_BLUR4_3x3,
                  FT_VBLUR_3x3,
                  FT_HBLUR_3x3,
                  FT_DBLUR1_3x3,
                  FT_DBLUR2_3x3,
                  FT_BLUR_GAUSSIAN_3x3
                } FilterType_e;

enum BorderMode { BM_NONE = 0, BM_CLEAR, BM_LOCAL, BM_TOROIDAL, BM_NB } BorderMode_e;
const char *border_list[BM_NB] = { "None", "Clear", "Local", "Toroidal" };

/* optimized filters */
#define FILTER_BLUR1_3x3_SIZE 3
#define FILTER_BLUR1_3x3_SUM 8
u_short filter_BLUR1_3x3[3*3] = {
  0, 1, 0,
  1, 4, 1,
  0, 1, 0
};

#define FILTER_BLUR2_3x3_SIZE 3
#define FILTER_BLUR2_3x3_SUM 8
u_short filter_BLUR2_3x3[3*3] = {
  0, 1, 0,
  2, 2, 2,
  0, 1, 0
};

#define FILTER_BLUR4_3x3_SIZE 3
#define FILTER_BLUR4_3x3_SUM 32
u_short filter_BLUR4_3x3[3*3] = {
  1, 2,  1,
  2, 16, 2,
  1, 2,  1
};

#define FILTER_VBLUR_3x3_SIZE 3
#define FILTER_VBLUR_3x3_SUM 10
u_short filter_VBLUR_3x3[3*3] = {
  0, 2, 0,
  1, 4, 1,
  0, 2, 0
};

#define FILTER_HBLUR_3x3_SIZE 3
#define FILTER_HBLUR_3x3_SUM 10
u_short filter_HBLUR_3x3[3*3] = {
  0, 1, 0,
  2, 4, 2,
  0, 1, 0
};

#define FILTER_DBLUR1_3x3_SIZE 3
#define FILTER_DBLUR1_3x3_SUM 10
u_short filter_DBLUR1_3x3[3*3] = {
  2, 0, 1,
  0, 4, 0,
  1, 0, 2
};

#define FILTER_DBLUR2_3x3_SIZE 3
#define FILTER_DBLUR2_3x3_SUM 10
u_short filter_DBLUR2_3x3[3*3] = {
  1, 0, 2,
  0, 4, 0,
  2, 0, 1
};

#define FILTER_BLUR_GAUSSIAN_3x3_SIZE 3
#define FILTER_BLUR_GAUSSIAN_3x3_SUM 16
u_short filter_BLUR_GAUSSIAN_3x3[3*3] = {
  1, 2, 1,
  2, 4, 2,
  1, 2, 1
};

/* filter size must be size*size */
void image_filter_average(Buffer8_t *dst, const Buffer8_t *src,
                          enum FilterType type, enum BorderMode borders, u_short size, u_short *filter);

#endif /* __BINIOU_IMAGE_FILTER_H */
