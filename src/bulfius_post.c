/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"


int
callback_post_parameters(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const struct _u_map *map_url = request->map_url;
  Context_t *ctx = user_data;
  assert(NULL != ctx);

  // check if we have a "?name=..."
  if (u_map_has_key(map_url, "name")) {
    Plugin_t *p = Plugins_find(u_map_get(map_url, "name"));

    if (NULL != p) {
      if (NULL != p->parameters) {
        // plugin exists and has parameters, get them
        json_t *params = p->parameters(ctx, NULL);
        json_error_t jerror;
        // parse request body
        json_t *body = ulfius_get_json_body_request(request, &jerror);

        if (NULL == body) {
          // error
          VERBOSE(printf("[!] %s: JSON error: %s\n", __FILE__, jerror.text));
        } else {
          // iterate over json post body
          const char *key;
          json_t *value;
          json_object_foreach(body, key, value) {
            json_t *p = json_object_get(params, key);
            if (NULL != p) {
              // get old value, will be used for type checking
              const json_t *old_value = json_object_get(p, "value");
              assert(NULL != old_value);
              json_t *new_value = NULL;

              // convert new value to the right type
              if (json_is_integer(old_value)) {
                if (json_is_integer(value)) {
                  new_value = value;
                } else {
                  new_value = json_integer(xatol(json_string_value(value)));
                }
              } else if (json_is_real(old_value)) {
                if (json_is_real(value)) {
                  new_value = value;
                } else if (json_is_integer(value)) {
                  new_value = json_real(json_integer_value(value));
                } else {
                  new_value = json_real(xatof(json_string_value(value)));
                }
              } else if (json_is_boolean(old_value)) {
                if (json_is_boolean(value)) {
                  new_value = value;
                } else {
                  new_value = (!strcmp("true", json_string_value(value))) ? json_true() : json_false();
                }
              } else {
                assert(json_is_string(old_value));
                new_value = value;
              }

              assert(NULL != new_value);
              // update new parameter and parameters object
              // probable memory leak here
              json_object_set(p, "value", new_value);
            }
          }
          json_decref(p->parameters(ctx, params));
        }
        ulfius_set_string_body_response(response, 204, NULL);
      } else {
        ulfius_set_string_body_response(response, 404, "Plugin not found");
      }
    }
  } else {
    ulfius_set_string_body_response(response, 400, "Bad request");
  }

  return U_CALLBACK_COMPLETE;
}
