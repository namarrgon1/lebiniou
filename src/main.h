/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MAIN_H
#define __MAIN_H

#include "biniou.h"
#include "defaults.h"

extern char *data_dir;
extern char *base_dir;
extern char *schemes_file;
extern char *input_plugin;
extern char *output_plugin;
extern char fullscreen;
extern long max_fps;
extern char window_decorations;
extern int32_t x_origin;
extern int32_t y_origin;
#ifndef FIXED
extern u_short width;
extern u_short height;
#endif
extern enum RandomMode random_mode;
extern char *pid_file;
extern char *themes;
extern float fade_delay;

void register_signals(void);
void getargs(int, char **);
void read_keyfile();
void set_configuration(gchar *);

#endif /* __MAIN_H */
