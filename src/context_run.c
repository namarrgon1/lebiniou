/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef WITH_GL
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include "biniou.h"
#include "schemes.h"
#include "images.h"
#include "colormaps.h"
#include "brandom.h"

#define BARSIZE (HEIGHT/20) /* Colormap bar size */


static void
Context_boundary(Context_t *ctx)
{
  switch (ctx->params3d.draw_boundary) {
    case 1:
      draw_cube_3d(&ctx->params3d, active_buffer(ctx), 250);
      break;

    case 2:
      draw_sphere_3d(&ctx->params3d, active_buffer(ctx), 250);
      break;

    case 3:
      draw_sphere_wireframe_3d(&ctx->params3d, active_buffer(ctx), 250);
      break;

    default:
      break;
  }
}


static void
Context_sync(Context_t *ctx)
{
  int i;
  const float rrt = b_timer_elapsed(ctx->fps_timer);
  const float sleep = ctx->i_max_fps - rrt;

  for (i = 0; i < NFPS-1; i++) {
    ctx->fps[i] = ctx->fps[i+1];
  }
  ctx->fps[i] = (rrt > 0) ? (1.0/rrt) : 0.0;

  if (sleep > 0) {
    ms_sleep(sleep*MFACTOR);
  }

  b_timer_start(ctx->fps_timer);
}


static void
Context_sequence(Context_t *ctx)
{
  const GList *tmp;
  uint16_t count = 0;

  tmp = ctx->sm->cur->layers;

#ifdef WITH_GL
  ctx->texture_ready = ctx->texture_used = ctx->gl_done = 0;
#endif

  while (NULL != tmp) {
    Layer_t *layer = (Layer_t *)tmp->data;
    Plugin_t *P = layer->plugin;

    assert(NULL != P);
    if (P == ctx->sm->cur->lens) {
      push_buffer(ctx);
    }

    if (NULL != P->run)
      if (!ctx->bypass || (ctx->bypass && !(*P->options & BEQ_MUTE_CAM))) {
        P->run(ctx);
        P->calls++;
        count++;
      }

    switch (layer->mode) {
      case NONE:
        break;

      case NORMAL:
        swap_buffers(ctx);
        break;

      case OVERLAY:
        Buffer8_overlay(active_buffer(ctx), passive_buffer(ctx));
        break;

      case XOR:
        Buffer8_XOR(active_buffer(ctx), passive_buffer(ctx));
        break;

      case AVERAGE:
        Buffer8_average(active_buffer(ctx), passive_buffer(ctx));
        break;

      case RANDOM: {
        Buffer8_t *buffs[2] = { active_buffer(ctx), passive_buffer(ctx) };
        Context_mix_buffers(ctx, buffs);
      }
      break;

      default:
        xerror("Unsupported layer mode %d\n", layer->mode);
        break;
    }

    tmp = g_list_next(tmp);
  }

  if (!count && ctx->random_mode) {
    assert(NULL != ctx->target_pic);
    Buffer8_copy(ctx->target_pic->buff, active_buffer(ctx));
  }
}


void
Context_run(Context_t *ctx)
{
  Plugin_t *input = ctx->input_plugin;
  GSList *outputs = ctx->outputs;

  /* sync fps */
  if (ctx->sync_fps) {
    Context_sync(ctx);
  }

  /* Auto-change sequence */
  if (Alarm_ring(ctx->a_random)) {
    if (ctx->random_mode == BR_SCHEMES) {
      if ((NULL != schemes) && (schemes->size > 1)) {
        Schemes_random(ctx);
      }
    } else if (ctx->random_mode == BR_SEQUENCES) {
      if ((NULL != sequences) && (sequences->size > 1)) {
        Context_random_sequence(ctx);
      }
    } else if (ctx->random_mode == BR_BOTH) {
      if (b_rand_boolean()) {
        if ((NULL != schemes) && (schemes->size > 1)) {
          printf("[+] Random scheme\n");
          Schemes_random(ctx);
        }
      } else {
        if ((NULL != sequences) && (sequences->size > 1)) {
          printf("[+] Random sequence\n");
          Context_random_sequence(ctx);
        }
      }
    }
  }

  /* Auto-change image */
  if (NULL != ctx->imgf) {
    /* Timer elapsed ? */
    if (ctx->imgf->on && Alarm_ring(ctx->a_images)) {
      ImageFader_random(ctx->imgf);
    }
    /* Fade image */
    if (ImageFader_ring(ctx->imgf)) {
      ImageFader_run(ctx->imgf);
      ctx->sm->cur->image_id = ctx->imgf->dst->id;
    }
  }

  /* Auto-change colormap */
  /* Timer elapsed ? */
  if (ctx->cf->on && Alarm_ring(ctx->a_cmaps)) {
    CmapFader_random(ctx->cf);
  }
  /* Fade colormap */
  if (CmapFader_ring(ctx->cf)) {
    CmapFader_run(ctx->cf);
    ctx->sm->cur->cmap_id = ctx->cf->dst->id;
  }

#ifdef WITH_WEBCAM
  /* Auto-change webcam */
  if ((ctx->webcams > 1) && Alarm_ring(ctx->a_webcams)) {
    u_short cam_no = Shuffler_get(ctx->webcams_shuffler);
    ctx->cam = cam_no;
#ifdef DEBUG
    printf("[i] Using webcam #%d\n", cam_no);
#endif
  }
#endif

  /* If we have an input plugin that runs in non-threaded mode,
   * call it now */
  if ((NULL != input) && (NULL != input->run)) {
    input->run(ctx);
    input->calls++;
  }
  if (NULL != ctx->input) {
    Input_process(ctx->input);
  }

  /* 3D stuff */
  Params3d_change_rotations(&ctx->params3d);

#ifdef WITH_GL
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  /* Initialize projection matrix */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(ctx->params3d.gl_fov, (float)WIDTH/(float)HEIGHT,
                 0.1, 10); //ZMAX);
  gluLookAt(0, 0, 5, //3.14,
            0, 0, -1, //-3.14,
            0, 1, 0);

  glShadeModel(GL_SMOOTH);

  glClearDepth(ZMAX),
               glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  glEnable(GL_BLEND);

  glRotatef(ctx->params3d.gl_rotations[0], 1.0, 0.0, 0.0);
  glRotatef(ctx->params3d.gl_rotations[1], 0.0, 1.0, 0.0);
  glRotatef(ctx->params3d.gl_rotations[2], 0.0, 0.0, 1.0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  if (ctx->pulse_3d) {
    float scale = Input_get_volume(ctx->input) + 1.0;
    glScalef(scale, scale, scale);
  }
#endif

  /* The sequence */
  Context_sequence(ctx);

  /* draw 3D boundary */
  Context_boundary(ctx);

  /* show the current colormap */
  if (ctx->display_colormap) {
    if (NULL == ctx->sm->cur->lens) {
      push_buffer(ctx);
    }
    Buffer8_color_bar(active_buffer(ctx), BARSIZE);
  }

  for ( ; NULL != outputs; outputs = g_slist_next(outputs)) {
    Plugin_t *output = (Plugin_t *)outputs->data;

    /* TODO print a warning: an output plugin without run() is a bug */
    if (NULL != output->run) {
      output->run(ctx);
      output->calls++;
    }
  }

  /* All outputs should have refreshed their cmap by now */
  ctx->cf->refresh = 0;

  /* Screenshot */
  if (ctx->take_screenshot) {
    Context_screenshot(ctx);
    ctx->take_screenshot = 0;
  }

  /* Restore possibly saved buffer */
  if ((NULL != ctx->sm->cur->lens) || ctx->display_colormap) {
    pop_buffer(ctx);
  }

  /* Process events */
  Context_process_events(ctx);

  ++ctx->frames;
}
