/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "brandom.h"


static GRand *brand = NULL;


void
b_rand_init(void)
{
  uint32_t seed;
  char *seedstr;

  if (NULL != (seedstr = getenv("LEBINIOU_SEED"))) {
    seed = xatol(seedstr);
    VERBOSE(printf("[i] Random seed set to %d\n", seed));
  } else {
    struct timeval t;

    gettimeofday(&t, NULL);
    seed = t.tv_sec;
    VERBOSE(printf("[i] No random seed, using %d\n", seed));
  }
  brand = g_rand_new_with_seed(seed);
}


void
b_rand_free(void)
{
  if (NULL != brand) {
    g_rand_free(brand);
  }
}


uint32_t
b_rand_int(void)
{
  return g_rand_int(brand);
}


uint32_t
b_rand_int_range(int32_t begin, int32_t end)
{
  return g_rand_int_range(brand, begin, end);
}


double
b_rand_double_range(double begin, double end)
{
  return g_rand_double_range(brand, begin, end);
}


int
b_rand_boolean()
{
  return g_rand_boolean(brand);
}
