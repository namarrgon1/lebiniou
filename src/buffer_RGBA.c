/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "buffer_RGBA.h"

/*!
 * \brief Create a pixel buffer
 */
BufferRGBA_t *
BufferRGBA_new()
{
  BufferRGBA_t *buff = xcalloc(1, sizeof(BufferRGBA_t));

  buff->buffer = xcalloc(BUFFSIZE, sizeof(RGBA_t));

  return buff;
}


void
BufferRGBA_delete(BufferRGBA_t *buff)
{
  if (NULL != buff) {
    xfree(buff->buffer);
  }
  xfree(buff);
}
