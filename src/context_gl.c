/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


void
Context_make_GL_RGBA_texture(Context_t *ctx, const u_char screen)
{
  GLuint texture;
  const RGBA_t *data = NULL;

  texture = ctx->textures[screen];
  data = export_RGBA_buffer(ctx, screen);

  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0,
               GL_RGBA, GL_UNSIGNED_BYTE, data);
}


#ifdef WITH_WEBCAM
void
Context_make_GL_gray_texture(Context_t *ctx, const u_char cam)
{
  GLuint texture;
  const Pixel_t *data = NULL;

  texture = ctx->cam_textures[cam];
  pthread_mutex_lock(&ctx->cam_mtx[cam]);
  data = ctx->cam_save[cam][0]->buffer;

  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, WIDTH, HEIGHT, 0,
               GL_LUMINANCE, GL_UNSIGNED_BYTE, data);
  pthread_mutex_unlock(&ctx->cam_mtx[cam]);
}
#endif
