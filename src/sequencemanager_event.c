/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "sequencemanager.h"

void
plugins_parameters_change(Context_t *ctx, const char *delta, const float factor)
{
  SequenceManager_t *sm = ctx->sm;
  json_t *params = plugin_parameter_change_selected(ctx, delta, factor);
  const GList *ptr = Sequence_find(sm->cur, plugins->selected);
  if (NULL != ptr) {
    Layer_t *layer = (Layer_t *)ptr->data;
    json_decref(layer->plugin_parameters);
    layer->plugin_parameters = params;
  } else {
    json_decref(params);
  }
}


int
SequenceManager_event(Context_t *ctx, const Event_t *e,
                      const char auto_colormaps, const char auto_images)
{
  SequenceManager_t *sm = ctx->sm;
  switch (e->cmd) {
    case BC_SWITCH:
      if (e->arg0 == BA_LENS) {
        SequenceManager_toggle_lens(sm->cur);
        return 1;
      }
      break;

    case BC_MOVE:
      if (e->arg0 == BA_UP) {
        SequenceManager_move_selected_front(sm->cur);
        return 1;
      } else if (e->arg0 == BA_DOWN) {
        SequenceManager_move_selected_back(sm->cur);
        return 1;
      } else {
        return 0;
      }
      break;

    case BC_PREV:
      if (e->arg0 == BA_LAYER_MODE) {
        SequenceManager_prev_layer_mode(sm->cur);
      } else {
        SequenceManager_select_previous_plugin(sm->cur);
      }
      return 1;
      break;

    case BC_NEXT:
      if (e->arg0 == BA_LAYER_MODE) {
        SequenceManager_next_layer_mode(sm->cur);
      } else {
        SequenceManager_select_next_plugin(sm->cur);
      }
      return 1;
      break;

    case BC_PARAM_PREV:
      if (NULL != plugins->selected->parameters) {
        json_t *j_params = plugins->selected->parameters(ctx, NULL);
        uint8_t nb_params = plugin_parameter_number(j_params);
        json_decref(j_params);

        /* authorised underlow */
        plugins->selected->selected_param = MIN((uint8_t)(plugins->selected->selected_param-1), nb_params-1);
      }
      return 1;
      break;

    case BC_PARAM_NEXT:
      if (NULL != plugins->selected->parameters) {
        json_t *j_params = plugins->selected->parameters(ctx, NULL);
        uint8_t nb_params = plugin_parameter_number(j_params);
        json_decref(j_params);

        plugins->selected->selected_param = (plugins->selected->selected_param + 1) % nb_params;
      }
      return 1;
      break;

    case BC_PARAM_DEC:
      plugins_parameters_change(ctx, "dec", 1);
      return 1;
      break;

    case BC_PARAM_INC:
      plugins_parameters_change(ctx, "inc", 1);
      return 1;
      break;

    case BC_PARAM_DEC_FAST:
      plugins_parameters_change(ctx, "dec", 10);
      return 1;
      break;

    case BC_PARAM_INC_FAST:
      plugins_parameters_change(ctx, "inc", 10);
      return 1;
      break;

    case BC_RESET:
      if (e->arg0 == BA_LAYER_MODE) {
        SequenceManager_default_layer_mode(sm->cur);
        return 1;
      } else if (e->arg0 == BA_SEQUENCE) {
        Sequence_clear(sm->cur, 0);
        return 1;
      } else {
        return 0;
      }
      break;

    case BC_SAVE:
      if (e->arg0 == BA_SEQUENCE_FULL) {
#ifdef DEBUG
        printf("[i] Save full sequence\n");
#endif
        Sequence_save(ctx, sm->cur, 0, SequenceManager_is_transient(sm, sm->cur),
                      TRUE, auto_colormaps, auto_images);
        sm->curseq = sequences->seqs;
        Shuffler_grow_one_left(sequences->shuffler);
        return 1;
      } else if (e->arg0 == BA_OVERWRITE_FULL) {
#ifdef DEBUG
        printf("[i] Update full sequence\n");
#endif
        Sequence_save(ctx, sm->cur, 1, SequenceManager_is_transient(sm, sm->cur),
                      TRUE, auto_colormaps, auto_images);
        return 1;
      } else if (e->arg0 == BA_SEQUENCE_BARE) {
#ifdef DEBUG
        printf("[i] Save bare sequence\n");
#endif
        Sequence_save(ctx, sm->cur, 0, SequenceManager_is_transient(sm, sm->cur),
                      FALSE, auto_colormaps, auto_images);
        sm->curseq = sequences->seqs;
        Shuffler_grow_one_left(sequences->shuffler);
        return 1;
      } else if (e->arg0 == BA_OVERWRITE_BARE) {
#ifdef DEBUG
        printf("[i] Update bare sequence\n");
#endif
        Sequence_save(ctx, sm->cur, 1, SequenceManager_is_transient(sm, sm->cur),
                      FALSE, auto_colormaps, auto_images);
        return 1;
      }
      break;

    default:
      break;
  }

  return 0;
}
