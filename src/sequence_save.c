/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include "globals.h"
#include "sequence.h"
#include "images.h"
#include "colormaps.h"

#define SEQUENCE_VERSION 0


json_t *
Sequence_to_json(const Sequence_t *s, const uint8_t full)
{
  json_t *sequence_j = json_object();
  json_object_set_new(sequence_j, "version", json_integer(SEQUENCE_VERSION));
  json_object_set_new(sequence_j, "id", json_integer(s->id));

  if (full) {
    json_object_set_new(sequence_j, "auto_colormaps", json_boolean(s->auto_colormaps));
    if (!s->auto_colormaps) {
      json_object_set_new(sequence_j, "colormap", json_string(Colormaps_name(s->cmap_id)));
    }

    json_object_set_new(sequence_j, "auto_images", json_boolean(s->auto_images));
    if (!s->auto_images) {
      json_object_set_new(sequence_j, "image", json_string(Images_name(s->image_id)));
    }
  }

  json_t *plugins_j = json_array();

  /* iterate over plugins list */
  for (GList *layers = g_list_first(s->layers); NULL != layers; layers = g_list_next(layers)) {
    Layer_t *layer = (Layer_t *)layers->data;
    Plugin_t *p = layer->plugin;

    json_t *j_plugin = json_object();
    json_object_set_new(j_plugin, "lens", json_boolean((NULL != s->lens) && (p == s->lens)));
    json_object_set_new(j_plugin, "name", json_string(p->name));
    json_object_set_new(j_plugin, "version", json_integer(p->version));
    json_object_set_new(j_plugin, "mode", json_string(LayerMode_to_string(layer->mode)));

    if (NULL != p->parameters) {
      json_t *j_params = (json_t *)p->parameters(NULL, NULL);
      json_object_set_new(j_plugin, "parameters", plugin_parameters_to_saved_parameters(j_params));
      json_decref(j_params);
    }

    json_array_append_new(plugins_j, j_plugin);
  }

  json_object_set_new(sequence_j, "plugins", plugins_j);

  return sequence_j;
}


static int
Sequence_write_json(const Sequence_t *s, const char *filename, const uint8_t full)
{
  json_t *sequence_j = Sequence_to_json(s, full);
  int ret = json_dump_file(sequence_j, filename, JSON_INDENT(4));

  if (!ret) {
    printf("[s] Saved sequence %s\n", filename);
  } else {
    printf("[s] ERROR: could not save sequence %s\n", filename);
  }
  json_decref(sequence_j);

  return ret;
}


void
Sequence_save(Context_t *ctx, Sequence_t *s, int overwrite, const int is_transient,
              const uint8_t bare, const char auto_colormaps, const char auto_images)
{
  Sequence_t *store = NULL;

  if (g_list_length(s->layers) == 0) {
    printf("[!] *NOT* saving an empty sequence !\n");
    return;
  }

  if (s->broken) {
    printf("[!] Sequence is broken, won't save !\n");
    return;
  }

  if (overwrite && (s->id == 0)) {
    printf("[!] Overwriting a NEW sequence == saving\n");
    overwrite = 0;
  }

  uint32_t old_id = s->id; // to restore if Sequence_write_json() fails
  char *old_name = (NULL != s->name) ? strdup(s->name) : NULL; // ditto

  if (!overwrite || is_transient) {
    xfree(s->name);
    s->id = unix_timestamp();
  }

  if (NULL == s->name) {
    s->name = g_strdup_printf("%" PRIu32, s->id);
  }

  const gchar *blah = Sequences_get_dir();
  rmkdir(blah);

  /* set auto_colormaps from context if needed */
  if (s->auto_colormaps == -1) {
    s->auto_colormaps = auto_colormaps;
  }
  /* set auto_images from context if needed */
  if (s->auto_images == -1) {
    s->auto_images = auto_images;
  }

  char *filename_json = NULL;
  char *filename_xml = NULL;
  if (overwrite) {
    filename_json = g_strdup_printf("%s/%s.json", blah, s->name);
    filename_xml = g_strdup_printf("%s/%s.xml", blah, s->name);
  } else {
    filename_json = g_strdup_printf("%s/%" PRIu32 ".json", blah, s->id);
  }

  if (!Sequence_write_json(s, filename_json, bare)) {
    if (overwrite) {
      /* Erase old XML sequence */
      FILE *fp = fopen(filename_xml, "r");
      if (NULL != fp) {
        fclose(fp);
        if (remove(filename_xml) == 0) {
          printf("[s] Removed old XML sequence %s\n", filename_xml);
        }
      }
    }
    xfree(old_name);
  } else { // saving failed, restore old id/name
    s->id = old_id;
    s->name = old_name;
  }
  g_free(filename_json);
  g_free(filename_xml);

  s->changed = 0;

  if (overwrite) {
    GList *oldp = g_list_find_custom(sequences->seqs, (gpointer)s, Sequence_sort_func);
    Sequence_t *old;

    if (NULL != oldp) {
      old = (Sequence_t *)oldp->data;
      Sequence_copy(ctx, s, old);
    } else {
      overwrite = 0;
    }
  }

  if (overwrite == 0) {
    /* new sequence */
    store = Sequence_new(0);
    Sequence_copy(ctx, s, store);

    sequences->seqs = g_list_prepend(sequences->seqs, (gpointer)store);
    sequences->size++;
  }
}
