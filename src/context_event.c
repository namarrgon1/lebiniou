/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "context.h"
#include "colormaps.h"
#include "images.h"


void
Context_add_event(Context_t *ctx, const Event_t *e)
{
  ctx->events = g_list_append(ctx->events, (gpointer)e);
}


static void
Context_process_event(Context_t *ctx, const Event_t *e)
{
  /* find out the recipient */
  switch (e->to) {
    case BT_NONE:
      break;

    case BT_IMAGEFADER:
      if (ImageFader_event(ctx->imgf, e)) {
        Sequence_changed(ctx->sm->cur);
      }
      break;

    case BT_CMAPFADER:
      if (CmapFader_event(ctx->cf, e)) {
        Sequence_changed(ctx->sm->cur);
      }
      break;

    case BT_CONTEXT:
      (void)Context_event(ctx, e);
      break;

    case BT_SEQMGR:
      (void)SequenceManager_event(ctx, e, ctx->auto_colormaps, ctx->auto_images);
      break;

    case BT_PLUGINS:
      (void)Plugins_event(plugins, e);
      break;

    default:
      break;
  }

  ctx->nb_events++;
}


void
Context_process_events(Context_t *ctx)
{
  GList *t;

  for (t = ctx->events; NULL != t; t = g_list_next(t)) {
    Event_t *e = (Event_t *)t->data;

    Context_process_event(ctx, e);
    xfree(e);
  }
  g_list_free(ctx->events);
  ctx->events = NULL;
}


static void
Context_display_random(const Context_t *ctx)
{
  printf("[A] Auto random is ");
  if (ctx->random_mode == BR_NONE) {
    printf("off\n");
  } else if (ctx->random_mode == BR_SEQUENCES) {
    printf("auto sequences\n");
  } else if (ctx->random_mode == BR_SCHEMES) {
    printf("auto schemes\n");
  } else if (ctx->random_mode == BR_BOTH) {
    printf("auto schemes/sequences\n");
  }
}


int
Context_event(Context_t *ctx, const Event_t *e)
{
  GSList *outputs = ctx->outputs;

  switch (e->cmd) {
    case BC_QUIT:
      if (e->arg0 == BA_SAVE)
        Sequence_save(ctx, ctx->sm->cur, 0, SequenceManager_is_transient(ctx->sm, ctx->sm->cur),
                      TRUE, ctx->auto_colormaps, ctx->auto_images);
      ctx->running = 0;
      return 1;

    case BC_SAVE:
      if (e->arg0 == BA_SCREENSHOT) {
        ctx->take_screenshot = 1;
        return 1;
      } else {
        return 0;
      }

    case BC_SWITCH:
      switch (e->arg0) {
        case BA_SELECTED:
          if (NULL != Sequence_find(ctx->sm->cur, plugins->selected)) {
            Context_remove_plugin(ctx, plugins->selected);
          } else {
            if (g_list_length(ctx->sm->cur->layers) < MAX_SEQ_LEN) {
              Context_insert_plugin(ctx, plugins->selected);
            }
          }
          return 1;

        case BA_FULLSCREEN:
          ctx->fullscreen = !ctx->fullscreen;
          printf("[S] Full-screen %s\n", ctx->fullscreen ? "off" : "on");

          for ( ; NULL != outputs; outputs = g_slist_next(outputs)) {
            Plugin_t *output = (Plugin_t *)outputs->data;

            if (NULL != output->fullscreen) {
              output->fullscreen(ctx->fullscreen);
            }
          }
          return 1;

        case BA_CURSOR:
          for ( ; NULL != outputs; outputs = g_slist_next(outputs)) {
            Plugin_t *output = (Plugin_t *)outputs->data;

            if (NULL != output->switch_cursor) {
              output->switch_cursor();
            }
          }
          return 1;

        case BA_COLORMAPS:
          if ((NULL != colormaps) && (colormaps->size > 1)) {
            ctx->sm->cur->auto_colormaps = ctx->auto_colormaps = !ctx->auto_colormaps;
            Sequence_changed(ctx->sm->cur);
            Context_update_auto(ctx);
            return 1;
          } else {
            return 0;
          }

        case BA_IMAGES:
          if ((NULL != images) && (images->size > 1)) {
            ctx->sm->cur->auto_images = ctx->auto_images = !ctx->auto_images;
            Sequence_changed(ctx->sm->cur);
            Context_update_auto(ctx);
            return 1;
          } else {
            return 0;
          }

        case BA_OSD_CMAP:
          ctx->display_colormap = 1 - ctx->display_colormap;
          return 1;

        case BA_ROTATIONS:
          if (Params3d_is_rotating(&ctx->params3d)) {
            zero_3d(&ctx->params3d);
          } else {
            /* By default, rotate around the Y axis */
            ctx->params3d.rotate_factor[X_AXIS] = 0;
            ctx->params3d.rotate_factor[Y_AXIS] = 1;
            ctx->params3d.rotate_factor[Z_AXIS] = 0;
          }
          return 1;

        case BA_BYPASS:
          ctx->bypass = !ctx->bypass;
          return 1;

#ifdef WITH_GL
        case BA_PULSE:
          ctx->pulse_3d = !ctx->pulse_3d;
          return 1;

        case BA_BOUNDARY:
          ctx->force_cube = !ctx->force_cube;
          return 1;
#endif

        case BA_MUTE:
          Input_toggle_mute(ctx->input);
          return 1;

        default:
          return 0;
      }

    case BC_PREV:
      switch (e->arg0) {
        case BA_SEQUENCE:
          Context_previous_sequence(ctx);
          return 1;

        case BA_RANDOM:
          if (ctx->random_mode == BR_NONE) {
            ctx->random_mode = BR_BOTH;
          } else {
            --ctx->random_mode;
          }

          if (ctx->random_mode == BR_SCHEMES) {
            if ((NULL == schemes) || (!Shuffler_ok(schemes->shuffler))) {
              printf("[i] Skipping random schemes since there are no schemes available\n");
              ctx->random_mode = BR_SEQUENCES;
            } else {
              Schemes_random(ctx);
              Alarm_init(ctx->a_random);
            }
          }

          if (ctx->random_mode == BR_SEQUENCES) {
            if ((NULL == sequences->seqs) || !g_list_length(sequences->seqs)) {
              printf("[i] Skipping random sequences since there are no sequences available\n");
              ctx->random_mode = BR_NONE;
            } else {
              Context_random_sequence(ctx);
              Alarm_init(ctx->a_random);
            }
          }

          Context_display_random(ctx);
          return 1;

        default:
          return 0;
      }
      break;

    case BC_NEXT:
      switch (e->arg0) {
        case BA_BOUNDARY:
          ctx->params3d.draw_boundary = (ctx->params3d.draw_boundary+1) % 4;
          return 1;

        case BA_SEQUENCE:
          Context_next_sequence(ctx);
          return 1;

        case BA_RANDOM:
          ctx->random_mode = (ctx->random_mode + 1) % BR_NB;

          if (ctx->random_mode == BR_SEQUENCES) {
            if ((NULL == sequences->seqs) || !g_list_length(sequences->seqs)) {
              printf("[i] Skipping random sequences since there are no sequences available\n");
              ctx->random_mode = BR_SCHEMES;
            } else {
              Context_random_sequence(ctx);
              Alarm_init(ctx->a_random);
            }
          }

          if (ctx->random_mode == BR_SCHEMES) {
            if ((NULL == schemes) || (!Shuffler_ok(schemes->shuffler))) {
              printf("[i] Skipping random schemes since there are no schemes available\n");
              ctx->random_mode = BR_BOTH;
            } else {
              Schemes_random(ctx);
              Alarm_init(ctx->a_random);
            }
          }

          Context_display_random(ctx);
          return 1;

#ifdef WITH_WEBCAM
        case BA_WEBCAM:
          if (ctx->webcams) {
            ctx->cam = (ctx->cam + 1) % ctx->webcams;
          } else {
            printf("[i] No webcams in use\n");
          }
          return 1;
#endif

        default:
          return 0;
      }
      break;

    case BC_RANDOM:
      switch (e->arg0) {
        case BA_SEQUENCE:
          assert(NULL != sequences);

          if (sequences->size > 1) {
            Context_random_sequence(ctx);
            Alarm_init(ctx->a_random);
          }
          return 1;

        case BA_SCHEME:
          if ((NULL != schemes) && (schemes->size > 1)) {
            Schemes_random(ctx);
            Alarm_init(ctx->a_random);
          }
          return 1;

        case BA_ROTATIONS:
          printf("[i] Randomizing rotations\n");
          Params3d_randomize(&ctx->params3d);
          return 1;

        default:
          return 0;
      }
      break;

    case BC_RESET:
      switch (e->arg0) {
        case BA_RANDOM:
          Buffer8_randomize(active_buffer(ctx));
          return 1;

        case BA_SEQUENCE:
          Buffer8_clear(active_buffer(ctx));
          return 1;

        case BA_NONE:
          printf("[!] *** EMERGENCY STOP *** button pressed !!!\n");
          /* Emergency stop, all auto_* modes -> disabled */
          ctx->auto_colormaps = ctx->auto_images = 0;
          ctx->sm->cur->auto_colormaps = ctx->sm->cur->auto_images = -1;
          Context_update_auto(ctx);
          Sequence_changed(ctx->sm->cur);
          ctx->random_mode = BR_NONE;
          ctx->locked = NULL;

          /* Stop and reset 3D rotations, remove boundary */
          zero_3d(&ctx->params3d);
          ctx->params3d.draw_boundary = 0;
          return 1;

        default:
          return 0;
      }

    case BC_USE_BANK:
      switch (ctx->bank_mode) {
        case SEQUENCES:
          Context_use_sequence_bank(ctx, e->arg0);
          return 1;

        case COLORMAPS: {
          uint32_t id = ctx->banks[COLORMAPS][ctx->bankset[COLORMAPS]][e->arg0];
          if (id) {
            printf("[i] Using colormap in bank #%d\n", (e->arg0+1));
            ctx->sm->next->cmap_id = id;
            ctx->bank[COLORMAPS] = e->arg0;
            Context_set_colormap(ctx);
          }
        }
        return 1;

        case IMAGES: {
          uint32_t id = ctx->banks[IMAGES][ctx->bankset[IMAGES]][e->arg0];
          if (id) {
            printf("[i] Using image in bank #%d\n", (e->arg0+1));
            ctx->sm->next->image_id = id;
            ctx->bank[IMAGES] = e->arg0;
            Context_set_image(ctx);
          }
        }
        return 1;

        default:
          return 0;
      }

    case BC_CLEAR_BANK:
      printf("[i] Clear sequence/colormap/image in bank #%d\n", (e->arg0+1));
      Context_clear_bank(ctx, e->arg0);
      return 1;

    case BC_RELOAD:
      if (e->arg0 == BA_SEQUENCE) {
        Context_latest_sequence(ctx);
        return 1;
      } else {
        return 0;
      }

#ifdef WITH_WEBCAM
    case BC_SET:
      if (e->arg0 == BA_WEBCAM) {
        if (ctx->webcams) {
          pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
          Buffer8_copy(ctx->cam_save[ctx->cam][0], ctx->cam_ref[ctx->cam]);
          pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);
        } else {
          printf("[i] No webcams in use\n");
        }
        return 1;
      } else {
        return 0;
      }
#endif

    case BC_STORE_BANK:
      printf("[i] Storing sequence/colormap/image in bank #%d\n", e->arg0+1);
      Context_store_bank(ctx, e->arg0);
      return 1;

    case BC_USE_BANKSET:
      printf("[i] Using bankset %d\n", (e->arg0+1));
      Context_use_bankset(ctx, e->arg0);
      return 1;

    case BC_SAVE_BANKS:
      printf("[i] Writing banks file\n");
      Context_save_banks(ctx);
      return 1;

    case BC_SET_BANKMODE:
      printf("[i] Set bank mode to ");
      switch (e->arg0) {
        case SEQUENCES:
          printf("sequences");
          break;
        case COLORMAPS:
          printf("colormaps");
          break;
        case IMAGES:
          printf("images");
          break;
        default:
          xerror("Invalid bank mode %d\n", e->arg0);
          break;
      }
      printf("\n");
      ctx->bank_mode = e->arg0;
      return 1;

    case BC_LOCK:
      if (e->arg0 == BA_SELECTED) {
        if (ctx->locked != plugins->selected) {
          ctx->locked = plugins->selected;
          printf("[i] Lock plugin '%s' (%s)\n", ctx->locked->name, plugins->selected->dname);
        } else {
          printf("[i] Unlock plugin '%s' (%s)\n", ctx->locked->name, plugins->selected->dname);
          ctx->locked = NULL;
        }
        return 1;
      } else {
        return 0;
      }

    case BC_VOLUME_SCALE:
      if (e->arg0 == BA_UP) {
        Input_volume_upscale(ctx->input);
        return 1;
      } else if (e->arg0 == BA_DOWN) {
        Input_volume_downscale(ctx->input);
        return 1;
      } else {
        return 0;
      }

    default:
      break;
  }

  return 0;
}


void
Context_send_event(Context_t *ctx, const enum RcptTo TO, const enum Command CMD,
                   const enum Arg ARG0)
{
  Event_t *e = xmalloc(sizeof(Event_t));
  e->to = TO;
  e->cmd = CMD;
  e->arg0 = ARG0;
  Context_add_event(ctx, e);
}
