/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SEQUENCE_H
#define __BINIOU_SEQUENCE_H

#include "layer.h"


typedef struct Sequence_s {
  uint32_t id;              /* sequence id */
  u_char   changed;
  u_char   broken;          /* sequence is broken (eg, no pictures available) */
  char     *name;

  GList    *layers;         /* list of plugins to run */
  Plugin_t *lens;           /* lens, NULL if none */

  char     auto_colormaps;  /* change colormaps at random interval (-1 == undefined) */
  uint32_t cmap_id;         /* colormap to use, 0 if use current */

  char     auto_images;     /* change images at random interval (-1 == undefined) */
  uint32_t image_id;        /* image to use, 0 if use current */
} Sequence_t;


Sequence_t *Sequence_new(const uint32_t);
void Sequence_delete(Sequence_t *);

void Sequence_display(const Sequence_t *);
void Sequence_copy(struct Context_s *, const Sequence_t *, Sequence_t *);
void Sequence_clear(Sequence_t *, const uint32_t);
void Sequence_changed(Sequence_t *);

void Sequence_insert(Sequence_t *, Plugin_t *);
void Sequence_insert_at_position(Sequence_t *, const u_short, Plugin_t *);
void Sequence_remove(Sequence_t *, const Plugin_t *);

GList *Sequence_find(const Sequence_t *, const Plugin_t *);
short Sequence_find_position(const Sequence_t *, const Plugin_t *);

void Sequence_save(struct Context_s *, Sequence_t *, int, const int, const uint8_t, const char, const char);
Sequence_t *Sequence_load(const char *);

gint Sequence_sort_func(gconstpointer, gconstpointer);

uint8_t Sequence_size(const Sequence_t *);

json_t *Sequence_to_json(const Sequence_t *, const uint8_t);

#endif /* __BINIOU_SEQUENCE_H */
