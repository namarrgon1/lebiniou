/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_OPTIONS_H
#define __BINIOU_OPTIONS_H

#include "utils.h"

enum PluginOptions {
  /* BE_ stands for "Biniou Effect" */
  BE_NONE       = 0,
  BE_SFX2D      = 1 << 0,  /* sound effect, 2d */
  BE_SFX3D      = 1 << 1,  /* sound effect, 3d */
  BE_GFX        = 1 << 2,  /* graphic effect */
  BE_BLUR       = 1 << 3,  /* blur effect */
  BE_DISPLACE   = 1 << 4,  /* displace effect */
  BE_LENS       = 1 << 5,  /* lens effect */
  BE_SCROLL     = 1 << 6,  /* scroll effect */
  BE_MIRROR     = 1 << 7,  /* mirror effect */
  BE_ROLL       = 1 << 8,  /* roll effect */
  BE_WARP       = 1 << 9,  /* warp effect */
  BE_CLEAN      = 1 << 10, /* obvious */

  /* BEQ_ stands for "Biniou Effect Qualifier" */
  BEQ_HOR        = 1 << 11,
  BEQ_VER        = 1 << 12,
  BEQ_DIAG       = 1 << 13,
  BEQ_UP         = 1 << 14, /* 2d effect hints */
  BEQ_DOWN       = 1 << 15,
  BEQ_LEFT       = 1 << 16,
  BEQ_RIGHT      = 1 << 17,
  BEQ_COLORMAP   = 1 << 18, /* changes colormap */
  BEQ_PARTICLES  = 1 << 19, /* generates particles */
  BEQ_SPLASH     = 1 << 20, /* splashes images */
  BEQ_THREAD     = 1 << 21, /* thread, do not use in sequences */
  BEQ_IMAGE      = 1 << 22, /* uses background image */
  BEQ_NORANDOM   = 1 << 23, /* don't select at random */
  BEQ_WEBCAM     = 1 << 24, /* uses a webcam input */
  BEQ_UNIQUE     = 1 << 25, /* plugin must be alone to be cool */
  BEQ_FIRST      = 1 << 26, /* insert plugin at the begining */
  BEQ_LAST       = 1 << 27, /* insert plugin at the end */
  BEQ_MUTE_CAM   = 1 << 28  /* some plugins (eg webcams) can be muted */
};

#define MAX_TYPES  29       /* all types available */

/* -- PluginType -- */

typedef struct PluginType_s {
  enum PluginOptions option;

  char    *name;
  char    *oname;
  u_short count;
} PluginType_t;


extern PluginType_t pTypes[MAX_TYPES];

#endif /* __BINIOU_OPTIONS_H */
