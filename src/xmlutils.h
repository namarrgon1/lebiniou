/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_XMLUTILS_H
#define __BINIOU_XMLUTILS_H

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include "utils.h"


xmlNodePtr xmlFindElement(const char *, xmlNodePtr);
int        xmlGetOptionalLong(const xmlDocPtr, const xmlNodePtr, long *);
char *     xmlGetMandatoryString(const xmlDocPtr, const char *, xmlNodePtr);

/* TODO rename as above -xml* */
long getintfield(xmlChar *);
int getfloatfield_optional(xmlChar *, float *);

#define xml_set_id(node, id)                                           \
  do {                                                                 \
    char idstr[20];                                                    \
    idstr[19] = '\0';                                                  \
    g_snprintf(idstr, 19, "%lu", (unsigned long)(id));                 \
    xmlSetProp((node), (const xmlChar *)"id", (const xmlChar *)idstr); \
  } while (0)

#endif /* __BINIOU_XMLUTILS_H */
