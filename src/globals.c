/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"

int32_t     WIDTH_ORIGIN, HEIGHT_ORIGIN;
#ifndef FIXED
u_short     WIDTH, HEIGHT;
#endif

Plugins_t   *plugins   = NULL;
Sequences_t *sequences = NULL;
Context_t   *context   = NULL;
Schemes_t   *schemes   = NULL;

u_char libbiniou_verbose = 1;

char *data_dir = NULL;
#ifdef WITH_WEBCAM
int webcams = 1;
int hflip = 1;
int vflip = 1;
char *video_base;
#else
int webcams = 0;
#endif

char *video_filename = NULL; // video plugin
