/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "xmlutils.h"
#include "images.h"
#include "colormaps.h"

#define SEQ_VERSION_MIN 0
#define SEQ_VERSION_MAX 0

Sequence_t *
Sequence_load_json(const char *file)
{
  if (NULL == file) {
    xerror("Attempt to load a sequence with a NULL filename\n");
  }

  char *dot = strrchr(file, '.');
  if ((NULL == dot) || strcasecmp(dot, ".json")) {
#ifdef DEBUG
    VERBOSE(printf("[!] Not a sequence filename: '%s'\n", file));
#endif
    return NULL;
  }

#ifdef DEBUG
  VERBOSE(printf("[i] Loading sequence from file '%s'\n", file));
#endif

  gchar *file_with_path = g_strdup_printf("%s/%s", Sequences_get_dir(), file);
  json_t *parsed_json = json_load_file(file_with_path, 0, NULL);
  g_free(file_with_path);

  if (NULL == parsed_json) {
    VERBOSE(printf("[!] Failed to parse JSON from '%s'\n", file));
    return NULL;
  }

  Sequence_t *s = NULL;
  json_t *j_plugins;

  json_t *j_sequence_version = json_object_get(parsed_json, "version");
  assert(NULL != j_sequence_version);
  int sequence_version = json_integer_value(j_sequence_version);
  if ((sequence_version < SEQ_VERSION_MIN) || (sequence_version > SEQ_VERSION_MAX)) {
    VERBOSE(printf("[!] Sequence version '%d' not supported\n", sequence_version));
    goto error;
  }

  json_t *j_id = json_object_get(parsed_json, "id");
  assert(NULL != j_id);
  s = Sequence_new(json_integer_value(j_id));
  *dot = '\0';
  s->name = strdup(file);
  *dot = '.';

  /* get auto_colormaps */
  json_t *j_auto_colormaps = json_object_get(parsed_json, "auto_colormaps");
  if (NULL != j_auto_colormaps) {
    s->auto_colormaps = json_is_boolean(j_auto_colormaps) ?
                        json_boolean_value(j_auto_colormaps) : json_integer_value(j_auto_colormaps);
  } else {
    s->auto_colormaps = 0;
    goto bare_sequence;
  }
  assert((s->auto_colormaps == 0) || (s->auto_colormaps == 1));
#ifdef DEBUG
  printf("[i] Random colormaps: %s\n", (s->auto_colormaps ? "on" : "off"));
#endif

  /* if not auto*, get colormap name */
  if (!s->auto_colormaps) {
    json_t *j_cmap = json_object_get(parsed_json, "colormap");
    assert(NULL != j_cmap);
    const char *cmap = json_string_value(j_cmap);
#ifdef DEBUG
    printf("[i] Need colormap: '%s'\n", cmap);
#endif
    s->cmap_id = Colormaps_find(cmap);
    // xfree(cmap);
  } else {
    s->cmap_id = Colormaps_random_id();
  }

  /* get auto_images */
  json_t *j_auto_images = json_object_get(parsed_json, "auto_images");
  if (NULL != j_auto_images) {
    s->auto_images = json_is_boolean(j_auto_images) ?
                     json_boolean_value(j_auto_images) : json_integer_value(j_auto_colormaps);;
  } else {
    s->auto_images = 0;
  }
  assert((s->auto_images == 0) || (s->auto_images == 1));
#ifdef DEBUG
  printf("[i] Random images: %s\n", (s->auto_images ? "on" : "off"));
#endif

  /* if not auto*, get image name */
  if (!s->auto_images) {
    json_t *j_image = json_object_get(parsed_json, "image");
    assert(NULL != j_image);
    const char *image = json_string_value(j_image);
#ifdef DEBUG
    printf("[i] Need image: '%s'\n", image);
#endif
    if (NULL == images) {
      VERBOSE(printf("[!] No images are loaded, won't find '%s'\n", image));
      goto error;
    } else {
      s->image_id = Images_find(image);
      if (s->image_id == 0) {
        VERBOSE(printf("[!] Image '%s' not found, using default\n", image));
      }
    }
  } else {
    if (NULL == images) {
      s->broken      = 1;
      s->image_id    = -1;
      s->auto_images = 0;
    } else {
      s->image_id = Images_random_id();
    }
  }

bare_sequence:

  /* get plugins */
  j_plugins = json_object_get(parsed_json, "plugins");
  assert(NULL != j_plugins);
  unsigned n_plugins = json_array_size(j_plugins);

  for (u_short n = 0; n < n_plugins; n++) {
    json_t *j_p = json_array_get(j_plugins, n);

    json_t *j_p_name = json_object_get(j_p, "name");
    assert(NULL != j_p_name);

    json_t *j_p_version = json_object_get(j_p, "version");
    assert(NULL != j_p_version);
    uint32_t version = json_integer_value(j_p_version);

    Plugin_t *p = Plugins_find(json_string_value(j_p_name));
    if (NULL == p) {
      VERBOSE(printf("[!] Could not find plugin %s\n", json_string_value(j_p_name)));
      goto error;
    }

    if (p->version != version) {
      if ((NULL != p->check_version) && p->check_version(version) == 0) {
        VERBOSE(printf("[!] Plugin %s version is %d but trying to load from incompatible version %d\n",
                       p->name, p->version, version));
        goto error;
      }
    }

    json_t *j_p_mode = json_object_get(j_p, "mode");
    assert(NULL != j_p_mode);
    const char *mode_str = json_string_value(j_p_mode);

    Layer_t *layer = Layer_new(p);
    layer->mode    = LayerMode_from_string(mode_str);

    json_t *j_p_lens = json_object_get(j_p, "lens");
    assert(NULL != j_p_lens);
    if (json_is_boolean(j_p_lens) && (json_is_true(j_p_lens))) {
      s->lens = p;
    }
    if (json_is_integer(j_p_lens) && (json_integer_value(j_p_lens))) {
      s->lens = p;
    }

    layer->plugin_parameters = json_deep_copy(json_object_get(j_p, "parameters"));

    s->layers = g_list_append(s->layers, (gpointer)layer);
  }

  json_decref(parsed_json);
  return s;

error:
  VERBOSE(printf("[!] Failed to load sequence from file '%s'\n", file));
  Sequence_delete(s);
  json_decref(parsed_json);

  return NULL;
}

/*
 * Left as an exercise to the reader: this code is not robust at all
 * we expect to read files that have been writen by Sequence_write.
 * But we are talibans and are not kind with wrong inputs.
 */
Sequence_t *
Sequence_load_xml(const char *file)
{
  Sequence_t *s = NULL;
  xmlDocPtr doc = NULL; /* XmlTree */
  xmlNodePtr sequence_node = NULL, sequence_node_save = NULL, plugins_node = NULL;
  int res;
  long tmp;
  xmlChar *youhou;
  int legacy = 0;

  if (NULL == file) {
    xerror("Attempt to load a sequence with a NULL filename\n");
  }

  char *dot = strrchr(file, '.');
  if ((NULL == dot) || strcasecmp(dot, ".xml")) {
#ifdef DEBUG
    printf("[!] Not a sequence filename: '%s'\n", file);
#endif
    return NULL;
  }

#ifdef DEBUG
  printf("[i] Loading sequence from file '%s'\n", file);
#endif

  /* BLA ! */
  xmlKeepBlanksDefault(0);
  xmlSubstituteEntitiesDefault(1);

  /*
   * build an XML tree from the file
   */
  gchar *blah = g_strdup_printf("%s/%s", Sequences_get_dir(), file);

  doc = xmlParseFile(blah);
  g_free(blah);
  if (NULL == doc) {
    xerror("xmlParseFile error\n");
  }

  sequence_node = xmlDocGetRootElement(doc);
  if (NULL == sequence_node) {
    VERBOSE(printf("[!] Sequence %s: xmlDocGetRootElement error\n", file));
    goto error;
  }

  sequence_node = xmlFindElement("sequence", sequence_node);
  if (NULL == sequence_node) {
    VERBOSE(printf("[!] Sequence %s: no <sequence> found\n", file));
    goto error;
  }

  youhou = xmlGetProp(sequence_node, (const xmlChar *)"id");
  tmp    = getintfield(youhou);
  xmlFree(youhou);

  if (tmp <= 0) {
    VERBOSE(printf("[!] Sequence %s: id must be > 0\n", file));
    goto error;
  }

  s = Sequence_new(tmp);

  /* first, get <auto_colormaps> */
  sequence_node_save = sequence_node = sequence_node->xmlChildrenNode;

  sequence_node = xmlFindElement("auto_colormaps", sequence_node);
  if (NULL == sequence_node) {
    sequence_node = sequence_node_save;
    goto bare_sequence;
  }
  res = xmlGetOptionalLong(doc, sequence_node, &tmp);
  if (res == -1) {
    s->auto_colormaps = 0;
  } else {
    s->auto_colormaps = (u_char)tmp;
  }
  assert((s->auto_colormaps == 0) || (s->auto_colormaps == 1));
#ifdef DEBUG
  printf("[i] Random colormaps: %s\n", (s->auto_colormaps ? "on" : "off"));
#endif

  if (!s->auto_colormaps) {
    /* not auto*, get colormap name */
    char *cmap = xmlGetMandatoryString(doc, "colormap", sequence_node);
#ifdef DEBUG
    printf("[i] Need colormap: '%s'\n", cmap);
#endif
    s->cmap_id = Colormaps_find(cmap);
    xfree(cmap);
  } else {
    s->cmap_id = Colormaps_random_id();
  }

  /* then, get  <auto_images> or (legacy) <auto_pictures> */
  sequence_node_save = sequence_node = sequence_node->next;
  sequence_node = xmlFindElement("auto_images", sequence_node);
  if (NULL == sequence_node) {
    sequence_node = sequence_node_save;
    sequence_node = xmlFindElement("auto_pictures", sequence_node);
    if (NULL == sequence_node) {
      VERBOSE(printf("[!] Sequence %s: no <auto_images> or <auto_pictures> found\n", file));
      goto error;
    } else {
      legacy = 1;
    }
  }
  res = xmlGetOptionalLong(doc, sequence_node, &tmp);
  if (res == -1) {
    s->auto_images = 0;
  } else {
    s->auto_images = (u_char)tmp;
  }
  assert((s->auto_images == 0) || (s->auto_images == 1));
#ifdef DEBUG
  printf("[i] Random images: %s\n", (s->auto_images ? "on" : "off"));
#endif

  if (!s->auto_images) {
    /* not auto*, get image name */
    char *image = xmlGetMandatoryString(doc, legacy ? "picture" : "image", sequence_node);
#ifdef DEBUG
    printf("[i] Need image: '%s'\n", image);
#endif
    if (NULL == images) {
      VERBOSE(printf("[!] No images are loaded, won't find '%s'\n", image));
      xfree(image);
      goto error;
    } else {
      s->image_id = Images_find(image);
      if (s->image_id == 0) {
        VERBOSE(printf("[!] Image '%s' not found, using default\n", image));
      }
      xfree(image);
    }
  } else {
    if (NULL == images) {
      s->broken      = 1;
      s->image_id    = -1;
      s->auto_images = 0;
    } else {
      s->image_id = Images_random_id();
    }
  }

bare_sequence:
  /* now, get plugins */
  plugins_node = xmlFindElement("plugins", sequence_node);
  if (NULL == plugins_node) {
    VERBOSE(printf("[!] Sequence %s: no <plugins> found\n", file));
    goto error;
  }
  plugins_node = plugins_node->xmlChildrenNode;
  if (NULL == plugins_node) {
    VERBOSE(printf("[!] Sequence %s: no elements in <plugins>\n", file));
    goto error;
  }

  while (NULL != plugins_node) {
    u_char   lens;
    Layer_t *layer;
    Plugin_t *p;

    assert(NULL != plugins_node->name);
    lens = !xmlStrcmp(plugins_node->name, (const xmlChar *)"lens");

    youhou = xmlGetProp(plugins_node, (const xmlChar *)"name");
    if (NULL != youhou) {
      p = Plugins_find((const char *)youhou);
      xfree(youhou);
      if (NULL == p) {
        goto error;
      }
    } else {
      goto error;
    }

    enum LayerMode mode;

    layer = Layer_new(p);

    youhou = xmlGetProp(plugins_node, (const xmlChar *)"mode");
    if (NULL != youhou) {
      mode = LayerMode_from_string((const char *)youhou);
      xmlFree(youhou);
    } else {
      mode = NORMAL;
    }
    layer->mode = mode;

    s->layers = g_list_append(s->layers, (gpointer)layer);

    if (lens) {
      s->lens = (Plugin_t *)p;
    }

    plugins_node = plugins_node->next;
  }

  /* Check sequence length */
  assert(Sequence_size(s) <= MAX_SEQ_LEN);

  /* Clean up */
  xmlFreeDoc(doc);
  xmlCleanupParser(); /* FIXME is this ok ? */

  *dot = '\0'; /* spr0tch */
  s->name = strdup(file);

  return s;

error:
  VERBOSE(printf("[!] Failed to load sequence from file '%s'\n", file));

  /* Clean up */
  xmlFreeDoc(doc);
  xmlCleanupParser(); /* FIXME is this ok ? */

  Sequence_delete(s);

  return NULL;
}

Sequence_t *
Sequence_load(const char *filename)
{
  char *dot = strrchr(filename, '.');
  if (NULL != dot) {
    if (strncasecmp(dot, ".json", 5 * sizeof(char)) == 0) {
      return Sequence_load_json(filename);
    }

    if (strncasecmp(dot, ".xml", 4 * sizeof(char)) == 0) {
      return Sequence_load_xml(filename);
    }
  }

  return NULL;
}
