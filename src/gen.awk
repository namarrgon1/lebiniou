BEGIN {
    print "/*";
    print " *  Copyright 1994-2020 Olivier Girondel";
    print " *";
    print " *  This file is part of lebiniou.";
    print " *";
    print " *  lebiniou is free software: you can redistribute it and/or modify";
    print " *  it under the terms of the GNU General Public License as published by";
    print " *  the Free Software Foundation, either version 2 of the License, or";
    print " *  (at your option) any later version.";
    print " *";
    print " *  lebiniou is distributed in the hope that it will be useful,";
    print " *  but WITHOUT ANY WARRANTY; without even the implied warranty of";
    print " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the";
    print " *  GNU General Public License for more details.";
    print " *";
    print " *  You should have received a copy of the GNU General Public License";
    print " *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.";
    print " */";
    print;
    print "#include \"events.h\"";
    print;
    print "/*";
    print " * Automagically generated from events.c.in";
    print " * DO NOT EDIT !!!";
    print " */";
    print;
    print "void";
    print "on_key(Context_t *ctx, const BKey_t *k)";
    print "{";
}


function pmod(mod) {
    if (mod == "-")
	return;
    if (mod == "A")
	return "Alt-";
    if (mod == "C")
	return "Ctrl-";
    if (mod == "S")
	return "Shift-";
    if (mod == "CS")
	return "Ctrl-Shift-";
    if (mod == "AS")
	return "Alt-Shift-";
}


function cmod(mod) {
    if (mod == "-")
	return "BKEY";
    if (mod == "A")
	return "BALT";
    if (mod == "C")
	return "BCTRL";
    if (mod == "S")
	return "BSHIFT";
    if (mod == "CS")
	return "BCTRLSHIFT";
    if (mod == "AS")
	return "BALTSHIFT";
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "-"))
	next;
  
    if ($1 == "*") {
	print "";
	print "  /* =============== "$2" =============== */";
    } else {
	print "";
	tail = substr($0, (length($1 $2 $3 $4 $5 $6) + 7));

	if (tail != "")
	    printf "  /* [%s%s] - %s */\n", pmod($2), $3, tail;
	else
	    printf "  /* [%s%s] */\n", pmod($2), $3;

	printf "  if (%s(k, SDLK_%s)) {\n", cmod($2), $3;
	printf "    Context_send_event(ctx, %s, %s, %s);\n    return;\n  }\n", $4, $5, $6;
    }
}


END {
    print "}";
}
