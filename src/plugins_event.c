/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "plugins.h"

/* Number of plugins to skip when using page up/down */
#define SKIP 10


int
Plugins_event(Plugins_t *plugins, const Event_t *e)
{
  int rc = 0;

  switch (e->cmd) {
    case BC_NEXT:
      Plugins_next();
      rc = 1;
      break;

    case BC_PREV:
      Plugins_prev();
      rc = 1;
      break;

    case BC_SELECT:
      if (e->arg0 == BA_DOWN) {
        Plugins_next_n(SKIP);
        rc = 1;
      } else if (e->arg0 == BA_UP) {
        Plugins_prev_n(SKIP);
        rc = 1;
      }
      break;

    case BC_RELOAD:
      if (e->arg0 == BA_SELECTED) {
        Plugins_reload_selected(plugins);
        rc = 1;
      }
      break;

    default:
      break;
  }

  return rc;
}
