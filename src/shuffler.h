/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SHUFFLER_H
#define __BINIOU_SHUFFLER_H

#include "utils.h"


enum ShufflerMode { BS_NONE=0, BS_SHUFFLE, BS_CYCLE, BS_RANDOM };


typedef struct Shuffler_s {
  u_short           size;
  long              current;
  enum ShufflerMode mode;
  char              *used;
  char              *disabled;
  char              verbose;
} Shuffler_t;


Shuffler_t *Shuffler_new(const u_short);
void Shuffler_delete(Shuffler_t *);
void Shuffler_verbose(Shuffler_t *);

u_short Shuffler_get(Shuffler_t *);

void Shuffler_set_mode(Shuffler_t *, const enum ShufflerMode);
void Shuffler_next_mode(Shuffler_t *);

void Shuffler_enable(Shuffler_t *, const u_short);
void Shuffler_disable(Shuffler_t *, const u_short);

void Shuffler_used(Shuffler_t *, const u_short);

void Shuffler_reinit(Shuffler_t *);  /* reinitilize used AND disabled */
void Shuffler_restart(Shuffler_t *); /* only reinitialize used */

void Shuffler_grow_one_left(Shuffler_t *);

u_char Shuffler_ok(const Shuffler_t *);

enum ShufflerMode Shuffler_parse_mode(const char *);

#endif /* __BINIOU_SHUFFLER_H */
