/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"


PluginType_t pTypes[MAX_TYPES] = {

  /* --- this MUST be the same order as the enum 'PluginOptions' */
  { BE_NONE,       "- NULL -", "BE_NONE", 0 },

  { BE_SFX2D,     "SFX2D", "BE_SFX2D", 0 },
  { BE_SFX3D,     "SFX3D", "BE_SFX3D", 0 },
  { BE_GFX,       "GFX", "BE_GFX", 0 },
  { BE_BLUR,      "Blur", "BE_BLUR", 0 },
  { BE_DISPLACE,  "Displace", "BE_DISPLACE", 0 },
  { BE_LENS,      "Lens", "BE_LENS", 0 },
  { BE_SCROLL,    "Scroll", "BE_SCROLL", 0 },
  { BE_MIRROR,    "Mirror", "BE_MIRROR", 0 },
  { BE_ROLL,      "Roll", "BE_ROLL", 0 },
  { BE_WARP,      "Warp", "BE_WARP", 0 },
  { BE_CLEAN,     "Cleaner", "BE_CLEAN", 0 },

  /* --- if you can select them, it's a bug :( */
  { BEQ_HOR,       "- Horizontal -", "BEQ_HOR", 0 },
  { BEQ_VER,       "- Vertical -", "BEQ_VER", 0 },
  { BEQ_DIAG,      "- Diagonal -", "BEQ_DIAG", 0 },
  { BEQ_UP,        "- Up -", "BEQ_UP", 0 },
  { BEQ_DOWN,      "- Down -", "BEQ_DOWN", 0 },
  { BEQ_LEFT,      "- Left -", "BEQ_LEFT", 0 },
  { BEQ_RIGHT,     "- Right -", "BEQ_RIGHT", 0 },
  { BEQ_COLORMAP,  "- Colormap -", "BEQ_COLORMAP", 0 },
  { BEQ_PARTICLES, "- Particles -", "BEQ_PARTICLES", 0 },
  { BEQ_SPLASH,    "- Splash -", "BEQ_SPLASH", 0 },
  { BEQ_THREAD,    "- Thread -", "BEQ_THREAD", 0 },
  { BEQ_IMAGE,     "- Image -", "BEQ_IMAGE", 0 },
  { BEQ_NORANDOM,  "- No random -", "BEQ_NORANDOM", 0 },
  { BEQ_UNIQUE,    "- Unique -", "BEQ_UNIQUE", 0 },
  { BEQ_FIRST,     "- First plugin -", "BEQ_FIRST", 0 },
  { BEQ_LAST,      "- Last plugin -", "BEQ_LAST", 0 },
  { BEQ_MUTE_CAM,  "- Bypassable webcam plugin -", "BEQ_MUTE_CAM", 0 }
};
