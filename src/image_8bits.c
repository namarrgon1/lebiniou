/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <wand/magick_wand.h>
#include "image_8bits.h"


Image8_t *
Image8_new()
{
  Image8_t *p = NULL;

  p = xcalloc(1, sizeof(Image8_t));

  p->id = -1;
  p->buff = Buffer8_new();

  return p;
}


Image8_t *
Image8_new_default()
{
  uint32_t i;
  Image8_t *img = Image8_new();

  for (i = 0; i < BUFFSIZE; i++) {
    img->buff->buffer[i] = i;
  }

  return img;
}


void
Image8_delete(Image8_t *p)
{
  xfree(p->name);
  xfree(p->dname);

  if (p->buff) {
    Buffer8_delete(p->buff);
  }
  xfree(p);
}


int
Image8_load_any(Image8_t *img, const char *file)
{
  MagickWand *wand = NULL;
  MagickBooleanType status;
  int res = -1;

  MagickWandGenesis();
  wand = NewMagickWand();
  if (NULL != wand) {
    status = MagickReadImage(wand, file);
    if (status == MagickTrue) {
      status = MagickResizeImage(wand, WIDTH, HEIGHT, LanczosFilter, 1);
      if (status == MagickTrue) {
        status = MagickFlipImage(wand);
        if (status == MagickTrue) {
          status = MagickExportImagePixels(wand, 0, 0, WIDTH, HEIGHT, "I", CharPixel, (void *)img->buff->buffer);
          if (status == MagickTrue) {
            res = 0;
          }
        }
      }
    }
    DestroyMagickWand(wand);
  }
  MagickWandTerminus();

  return res;
}


int
Image8_load(Image8_t *pic, const uint32_t id, const char *dir, const char *filename)
{
  char *file = g_strdup_printf("%s/%s", dir, filename);

  if (Image8_load_any(pic, file) != 0) {
    g_free(file);
    return -1;
  }
  g_free(file);

  xfree(pic->name);
  pic->name = strdup(filename);
  xfree(pic->dname);
  pic->dname = strdup(pic->name);

  if (NULL != (file = strchr(pic->dname, '.'))) {
    *file = '\0';  /* spr0tch */
  }

  pic->id = id;

  return 0;
}


void
Image8_copy(const Image8_t *from, Image8_t *to)
{
  assert(NULL != from);
  assert(NULL != to);

  assert(NULL != from->name);

  xfree(to->name);

  to->name = strdup(from->name);

  to->id = from->id;

  Buffer8_copy(from->buff, to->buff);
}
