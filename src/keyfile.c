/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"

/* check doc/lebiniourc.in for the complete list of options */

/* configuration file */
gchar *keyfile = NULL;

/* groups */
#define PLUGINS  "Plugins"
#define SCREEN   "Screen"
#define ENGINE   "Engine"
#define INPUT    "Input"
#define WEBCAM   "Webcam"
#define PARAMS3D "3D"

extern float phase;
#ifdef WITH_WEBCAM
extern int hflip, vflip, webcams;
extern char *video_base;
#endif
extern char *video_filename;
extern char *data_dir;
extern uint32_t input_size;
extern double volume_scale;
extern double scale_factor; // 3D scale factor
extern double rot_amount;
extern uint8_t rotation_factor;
extern uint8_t start_with_first_sequence;


static void process_timers(GKeyFile *);
static void process_change_modes(GKeyFile *);
static void process_3d_parameters(GKeyFile *);


static void
process(GKeyFile *kf)
{
  GError *error = NULL;
  gchar *input = NULL, *output = NULL, *kf_themes = NULL;
  gint kf_input_size;
  gchar **blacklist;
  gsize blacklist_length = 0;
  gchar fs = FALSE, antiphase = FALSE;
#ifndef FIXED
  gint kf_width, kf_height;
#endif
  gint kf_fps, kf_rnd;
  gdouble kf_fade_delay;
#ifdef WITH_WEBCAM
  gchar whflip = FALSE, wvflip = FALSE;
  gint kf_webcams;
  gchar *kf_video_base;
#endif
  gchar *kf_sequences_dir = NULL;
  gchar *kf_data_dir = NULL;
  double kf_volume_scale = 0;
  gchar first_sequence = FALSE;

  input = g_key_file_get_string(kf, PLUGINS, "Input", NULL);
  if (NULL != input) {
    input_plugin = input;
    VERBOSE(printf("[k] Setting input plugin: %s\n", input_plugin));
  }

  kf_input_size = g_key_file_get_integer(kf, PLUGINS, "InputSize", &error);
  if (NULL == error) {
    if (kf_input_size > 0) {
      input_size = kf_input_size;
      VERBOSE(printf("[k] Setting input size to %d\n", input_size));
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  output = g_key_file_get_string(kf, PLUGINS, "Output", NULL);
  if (NULL != output) {
    output_plugin = output;
    VERBOSE(printf("[k] Setting output plugin: %s\n", output_plugin));
  }

  video_filename = g_key_file_get_string(kf, PLUGINS, "Video", NULL);
  if (NULL != video_filename) {
    VERBOSE(printf("[k] Setting video file: %s\n", video_filename));
  }

  fs = g_key_file_get_boolean(kf, SCREEN, "FullScreen", &error);
  if (NULL == error) {
    if (fs != fullscreen) {
      fullscreen = fs;
      VERBOSE(printf("[k] Setting full-screen to: %s\n", (fs ? "on" : "off")));
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  first_sequence = g_key_file_get_boolean(kf, ENGINE, "StartWithFirstSequence", &error);
  if (NULL == error) {
    start_with_first_sequence = first_sequence;
    VERBOSE(printf("[k] Starting with the %s sequence\n", (start_with_first_sequence ? "first" : "last")));
  } else {
    g_error_free(error);
    error = NULL;
  }

#ifndef FIXED
  kf_width = g_key_file_get_integer(kf, SCREEN, "Width", &error);
  if (NULL == error) {
    if (kf_width > 0)
      if (width != kf_width) {
        width = kf_width;
        VERBOSE(printf("[k] Setting screen width to %d\n", width));
      }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_height = g_key_file_get_integer(kf, SCREEN, "Height", &error);
  if (NULL == error) {
    if (kf_height > 0)
      if (height != kf_height) {
        height = kf_height;
        VERBOSE(printf("[k] Setting screen height to %d\n", height));
      }
  } else {
    g_error_free(error);
    error = NULL;
  }
#endif

  kf_fps = g_key_file_get_integer(kf, ENGINE, "Fps", &error);
  if (NULL == error) {
    if (kf_fps > 0)
      if (max_fps != kf_fps) {
        max_fps = kf_fps;
        VERBOSE(printf("[k] Setting FPS to %li\n", max_fps));
      }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_fade_delay = g_key_file_get_double(kf, ENGINE, "FadeDelay", &error);
  if (NULL == error) {
    if (kf_fade_delay >= 0 && fade_delay != kf_fade_delay) {
      fade_delay = kf_fade_delay;
      VERBOSE(printf("[k] Setting fade delay to %.2f s\n", fade_delay));
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_rnd = g_key_file_get_integer(kf, ENGINE, "RandomMode", &error);
  if (NULL == error) {
    if ((kf_rnd >= BR_NONE) && (kf_rnd <= BR_BOTH))
      if ((gint)random_mode != kf_rnd) {
        random_mode = kf_rnd;
        VERBOSE(printf("[k] Setting random mode to %d\n", kf_rnd));
      }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_themes = g_key_file_get_string(kf, ENGINE, "Themes", NULL);
  if (NULL != kf_themes) {
    themes = kf_themes;
    VERBOSE(printf("[k] Setting themes to: %s\n", themes));
  }

  kf_sequences_dir  = g_key_file_get_string(kf, ENGINE, "SequencesDir", NULL);
  if (NULL != kf_sequences_dir) {
    VERBOSE(printf("[k] Setting sequences directory to: %s\n", kf_sequences_dir));
    Sequences_set_dir(kf_sequences_dir);
  }

  kf_data_dir  = g_key_file_get_string(kf, ENGINE, "DataDir", NULL);
  if (NULL != kf_data_dir) {
    VERBOSE(printf("[k] Setting data directory to: %s\n", kf_data_dir));
    data_dir = kf_data_dir;
  }

  antiphase = g_key_file_get_boolean(kf, INPUT, "AntiPhase", &error);
  if (NULL == error) {
    if (antiphase == TRUE) {
      phase = -1.0;
      VERBOSE(printf("[k] Setting antiphase\n"));
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_volume_scale = g_key_file_get_double(kf, INPUT, "VolumeScale", &error);
  if (NULL == error) {
    if (kf_volume_scale > 0) {
      VERBOSE(printf("[k] Setting volume scale to %.1f\n", kf_volume_scale));
      volume_scale = kf_volume_scale;
    } else {
      xerror("VolumeScale must be > 0\n");
    }
  } else if (G_KEY_FILE_ERROR_INVALID_VALUE == error->code) {
    xerror("Invalid VolumeScale\n");
  } else {
    g_error_free(error);
    error = NULL;
  }

#ifdef WITH_WEBCAM
  whflip = g_key_file_get_boolean(kf, WEBCAM, "HorizontalFlip", &error);
  if (NULL == error) {
    if (whflip == TRUE) {
      VERBOSE(printf("[k] Setting webcam horizontal flip\n"));
      hflip = !hflip;
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  wvflip = g_key_file_get_boolean(kf, WEBCAM, "VerticalFlip", &error);
  if (NULL == error) {
    if (wvflip == TRUE) {
      VERBOSE(printf("[k] Setting webcam vertical flip\n"));
      vflip = !vflip;
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_webcams = g_key_file_get_integer(kf, WEBCAM, "Webcams", &error);
  if (NULL == error) {
    if ((kf_webcams >= 0) && (kf_webcams <= MAX_CAMS)) {
      webcams = kf_webcams;
      VERBOSE(printf("[k] Webcam: grabbing %d device%s\n", webcams, (webcams == 1 ? "": "s")));
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_video_base = g_key_file_get_string(kf, WEBCAM, "Device", NULL);
  if (NULL != kf_video_base) {
    free(video_base);
    video_base = kf_video_base;
    VERBOSE(printf("[k] Webcam: device base %s\n", video_base));
  }
#endif /* WITH_WEBCAM */

  blacklist = g_key_file_get_string_list(kf, PLUGINS, "Blacklist",
                                         &blacklist_length, NULL);
  if (blacklist_length) {
    VERBOSE(printf("[k] Plugins: blacklist of %"G_GSIZE_FORMAT" plugins\n", blacklist_length));
  }
  Plugins_set_blacklist(blacklist);
}


void
read_keyfile()
{
  const gchar *home_dir = NULL;
  g_autoptr(GKeyFile) kf = g_key_file_new();
  GKeyFileFlags flags = G_KEY_FILE_NONE;
  g_autoptr(GError) error = NULL;

  if (NULL == keyfile) {
    home_dir = g_get_home_dir();
    keyfile = g_strdup_printf("%s/%s", home_dir, KEYFILE);
    VERBOSE(printf("[k] Using default configuration file: %s\n", keyfile));
  }

  if (TRUE != g_key_file_load_from_file(kf, keyfile, flags, &error)) {
    if (G_FILE_ERROR_NOENT == error->code) {
      g_free(keyfile);
      return;
    } else {
      xerror("Failed to load %s\n", keyfile);
    }
  }

  process(kf);
  process_timers(kf);
  process_change_modes(kf);
  process_3d_parameters(kf);

  g_free(keyfile);
}


static void
process_timers(GKeyFile *kf)
{
  GError *error = NULL;
  gint colormaps_min, colormaps_max;
  gint images_min, images_max;
  gint sequences_min, sequences_max;

  colormaps_min = g_key_file_get_integer(kf, ENGINE, "ColormapsMin", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting ColormapsMin to %d\n", colormaps_min));
  } else {
    colormaps_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for ColormapsMin, using default= %d\n", colormaps_min));
    }
    g_error_free(error);
    error = NULL;
  }

  colormaps_max = g_key_file_get_integer(kf, ENGINE, "ColormapsMax", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting ColormapsMax to %d\n", colormaps_max));
  } else {
    colormaps_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for ColormapsMax, using default= %d\n", colormaps_max));
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_COLORMAPS, colormaps_min, colormaps_max);

  images_min = g_key_file_get_integer(kf, ENGINE, "ImagesMin", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting ImagesMin to %d\n", images_min));
  } else {
    images_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for ImagesMin, using default= %d\n", images_min));
    }
    g_error_free(error);
    error = NULL;
  }

  images_max = g_key_file_get_integer(kf, ENGINE, "ImagesMax", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting ImagesMax to %d\n", images_max));
  } else {
    images_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for ImagesMax, using default= %d\n", images_max));
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_IMAGES, images_min, images_max);

  sequences_min = g_key_file_get_integer(kf, ENGINE, "SequencesMin", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting SequencesMin to %d\n", sequences_min));
  } else {
    sequences_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for SequencesMin, using default= %d\n", sequences_min));
    }
    g_error_free(error);
    error = NULL;
  }

  sequences_max = g_key_file_get_integer(kf, ENGINE, "SequencesMax", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting SequencesMax to %d\n", sequences_max));
  } else {
    sequences_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for SequencesMax, using default= %d\n", sequences_max));
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_SEQUENCES, sequences_min, sequences_max);

#ifdef WITH_WEBCAM
  gint webcams_min, webcams_max;

  webcams_min = g_key_file_get_integer(kf, WEBCAM, "WebcamsMin", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting WebcamsMin to %d\n", webcams_min));
  } else {
    webcams_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for WebcamsMin, using default= %d\n", webcams_min));
    }
    g_error_free(error);
    error = NULL;
  }

  webcams_max = g_key_file_get_integer(kf, WEBCAM, "WebcamsMax", &error);
  if (NULL == error) {
    VERBOSE(printf("[k] Setting WebcamsMax to %d\n", webcams_max));
  } else {
    webcams_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
      VERBOSE(printf("[!] Invalid value for WebcamsMax, using default= %d\n", webcams_max));
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_WEBCAMS, webcams_min, webcams_max);
#endif
}


void
set_configuration(gchar *file)
{
  keyfile = strdup(file);
  VERBOSE(printf("[k] Setting configuration file: %s\n", keyfile));
}


static void
process_change_modes(GKeyFile *kf)
{
  gchar *colormaps_mode = NULL;
  gchar *images_mode = NULL;
  gchar *sequences_mode = NULL;

  colormaps_mode = g_key_file_get_string(kf, ENGINE, "ColormapsMode", NULL);
  if (NULL != colormaps_mode) {
    VERBOSE(printf("[k] Setting colormaps mode: %s\n", colormaps_mode));
    Context_set_shuffler_mode(BD_COLORMAPS, Shuffler_parse_mode(colormaps_mode));
    xfree(colormaps_mode);
  }

  images_mode = g_key_file_get_string(kf, ENGINE, "ImagesMode", NULL);
  if (NULL != images_mode) {
    VERBOSE(printf("[k] Setting images mode: %s\n", images_mode));
    Context_set_shuffler_mode(BD_IMAGES, Shuffler_parse_mode(images_mode));
    xfree(images_mode);
  }

  sequences_mode = g_key_file_get_string(kf, ENGINE, "SequencesMode", NULL);
  if (NULL != sequences_mode) {
    VERBOSE(printf("[k] Setting sequences mode: %s\n", sequences_mode));
    Context_set_shuffler_mode(BD_SEQUENCES, Shuffler_parse_mode(sequences_mode));
    xfree(sequences_mode);
  }

#ifdef WITH_WEBCAM
  gchar *webcams_mode = NULL;
  webcams_mode = g_key_file_get_string(kf, WEBCAM, "WebcamsMode", NULL);
  if (NULL != webcams_mode) {
    VERBOSE(printf("[k] Setting webcams mode: %s\n", webcams_mode));
    Context_set_shuffler_mode(BD_WEBCAMS, Shuffler_parse_mode(webcams_mode));
  }
#endif
}


static void
process_3d_parameters(GKeyFile *kf)
{
  GError *error = NULL;
  double kf_scale_factor = 0;
  double kf_rot_amount = 0;
  int kf_rotation_factor = 0;

  kf_scale_factor = g_key_file_get_double(kf, PARAMS3D, "ScaleFactor", &error);
  if (NULL == error) {
    if (kf_scale_factor >= SCALE_FACTOR_MIN) {
      VERBOSE(printf("[k] Setting scale factor to %.2f\n", kf_scale_factor));
      scale_factor = kf_scale_factor;
    } else {
      xerror("ScaleFactor must be >= %.2f\n", SCALE_FACTOR_MIN);
    }
  } else if (G_KEY_FILE_ERROR_INVALID_VALUE == error->code) {
    xerror("Invalid ScaleFactor\n");
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_rot_amount = g_key_file_get_double(kf, PARAMS3D, "RotationAmount", &error);
  if (NULL == error) {
    if (kf_rot_amount >= ROT_AMOUNT_MIN) {
      VERBOSE(printf("[k] Setting rotation amount to %.4f\n", kf_rot_amount));
      rot_amount = kf_rot_amount;
    } else {
      xerror("RotationAmount must be >= %.4f\n", ROT_AMOUNT_MIN);
    }
  } else if (G_KEY_FILE_ERROR_INVALID_VALUE == error->code) {
    xerror("Invalid RotationAmount\n");
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_rotation_factor = g_key_file_get_integer(kf, PARAMS3D, "RotationFactor", &error);
  if (NULL == error) {
    if (kf_rotation_factor > 1) {
      rotation_factor = kf_rotation_factor;
      VERBOSE(printf("[k] Setting rotation factor to %d\n", rotation_factor));
    } else {
      xerror("RotationFactor must be > 1\n");
    }
  } else if (G_KEY_FILE_ERROR_INVALID_VALUE == error->code) {
    xerror("Invalid RotationFactor\n");
  } else {
    g_error_free(error);
    error = NULL;
  }
}
