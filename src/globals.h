/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_GLOBALS_H
#define __BINIOU_GLOBALS_H

#include "sequences.h"
#include "plugins.h"
#include "schemes.h"
#include "context.h"

extern int32_t     WIDTH_ORIGIN;
extern int32_t     HEIGHT_ORIGIN;
#ifndef FIXED
extern u_short     WIDTH;
extern u_short     HEIGHT;
#endif
extern Plugins_t   *plugins;
extern Sequences_t *sequences;
extern Schemes_t   *schemes;
extern Context_t   *context;

#ifdef WITH_WEBCAM
#define MAX_TIMERS 4
#else
#define MAX_TIMERS 3
#endif
extern int webcams;

#endif /* __BINIOU_GLOBALS_H */
