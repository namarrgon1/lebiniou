/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_ULFIUS_H
#define __BINIOU_ULFIUS_H

#include <ulfius.h>

#define ULFIUS_PORT 30543 // erlang:phash2("lebiniou", 64511) + 1024.

#define BULFIUS_GET  "GET"
#define BULFIUS_POST "POST"

#define BULFIUS_PARAMETERS "/parameters"
#define BULFIUS_SEQUENCE   "/sequence"
#define BULFIUS_STATISTICS "/statistics"

// GET
int callback_get_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_get_sequence(const struct _u_request *, struct _u_response *, void *);
int callback_get_statistics(const struct _u_request *, struct _u_response *, void *);

// POST
int callback_post_parameters(const struct _u_request *, struct _u_response *, void *);

#endif /* __BINIOU_ULFIUS_H */
