/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_BUFFER_RGBA_H
#define __BINIOU_BUFFER_RGBA_H

#include "rgba.h"
#include "utils.h"
#include "constants.h"


typedef struct BufferRGBA_s {
  RGBA_t *buffer;
} BufferRGBA_t;


BufferRGBA_t *BufferRGBA_new(void);
void BufferRGBA_delete(BufferRGBA_t *);


static inline void
BufferRGBA_clear(BufferRGBA_t *buff)
{
  memset(buff->buffer, 0, BUFFSIZE*sizeof(RGBA_t));
}


#endif /* __BINIOU_BUFFER_RGBA_H */
