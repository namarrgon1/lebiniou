/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_UTILS_H
#define __BINIOU_UTILS_H

#if HAVE_CONFIG_H
#include "config.h"
#include "includes.h"
#else
#error "No config.h"
#endif

/*
#ifdef ICC_COMPILER
#pragma warning(disable:981)
#endif
*/

#if __STDC_VERSION__ < 199901L
#if __GNUC__ >= 2
#define __func__ __FUNCTION__
#else
#define __func__ "<unknown>"
#endif
#endif

void xerror(const char *, ...);
void xperror(const char *);
void okdone(const char *);
void *xmalloc(const size_t);
void *xcalloc(const size_t, const size_t);

#define xfree(ptr) do { free(ptr); ptr = NULL; } while (0)

void *xrealloc(void *, size_t);
double xatof(const char *);
long xatol(const char *);
void rmkdir(const char *);

uint32_t FNV_hash(const char*);
void ms_sleep(const u_long);
int parse_two_shorts(const char *, const int, short *, short *);

extern u_char       libbiniou_verbose;
#define VERBOSE(X) if (libbiniou_verbose) { X; fflush(stdout); }

int check_command(const char *);

time_t unix_timestamp();

#endif /* __BINIOU_UTILS_H */
