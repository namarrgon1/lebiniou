/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SEQUENCE_MANAGER_H
#define __BINIOU_SEQUENCE_MANAGER_H

#include "sequence.h"
#include "event.h"
#include "constants.h"


enum BankMode { SEQUENCES=0, COLORMAPS, IMAGES };

typedef struct SequenceManager_s {
  Sequence_t      *cur, *next, *transient;
  GList           *curseq;
  pthread_mutex_t mutex;
} SequenceManager_t;


SequenceManager_t *SequenceManager_new();
void SequenceManager_delete(SequenceManager_t *);

void SequenceManager_toggle_lens(Sequence_t *);

void SequenceManager_select_previous_plugin(Sequence_t *);
void SequenceManager_select_next_plugin(Sequence_t *);

void SequenceManager_move_selected_front(Sequence_t *);
void SequenceManager_move_selected_back(Sequence_t *);

void SequenceManager_default_layer_mode(const Sequence_t *);
void SequenceManager_prev_layer_mode(const Sequence_t *);
void SequenceManager_next_layer_mode(const Sequence_t *);

int SequenceManager_event(struct Context_s *, const Event_t *, const char, const char);

int SequenceManager_is_transient(const SequenceManager_t *, const Sequence_t *);

void SequenceManager_lock(SequenceManager_t *);
void SequenceManager_unlock(SequenceManager_t *);

#endif /* __BINIOU_SEQUENCE_MANAGER_H */
