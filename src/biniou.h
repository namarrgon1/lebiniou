/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_H
#define __BINIOU_H

#include "globals.h"


#define B_INIT_NONE         0
#define B_INIT_SEQUENCES    1
#define B_INIT_SCHEMES      2
#define B_INIT_VERBOSE      4
#define B_INIT_ALL          (B_INIT_SEQUENCES|B_INIT_SCHEMES)

void biniou_new(const char *, const char *, const char *, const char *,
                const u_char, const uint32_t, const int);
void biniou_delete();

/* vla le bordel */
void biniou_loop();
void biniou_go(const u_long);
void biniou_run();
void biniou_start();
void biniou_end();
int  biniou_stop();

void biniou_load_input(const char *, const char *, const double);
void biniou_set_input(Input_t *);
void biniou_load_output(const char *, const char *);
void biniou_set_full_screen(const u_char);
void biniou_set_max_fps(const u_short);
void biniou_verbosity(const u_char);
void biniou_set_random_mode(const enum RandomMode);
void biniou_set_delay(const enum RandomDelays, const int, const int);

#endif /* __BINIOU_H */
