/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "sequences.h"

char *sequences_dir = NULL; // Sequences directory


void
Sequences_new()
{
  DIR *dir;
  struct dirent *entry;
  const gchar *blah = Sequences_get_dir();

  sequences = xcalloc(1, sizeof(Sequences_t));
  sequences->seqs = NULL;

  dir = opendir(blah);

  if (NULL == dir) {
#ifdef DEBUG
    printf("[!] No user sequences found: %s: %s\n", blah, strerror(errno));
#endif
    // g_free(blah);
  } else {
    // g_free(blah);
    while (NULL != (entry = readdir(dir))) {
      if (strcmp(entry->d_name, "..")) {
        Sequence_t *s = NULL;

        s = Sequence_load(entry->d_name);

        if (NULL != s) {
          sequences->seqs = g_list_insert_sorted(sequences->seqs, (gpointer)s, Sequence_sort_func);
        }
      }
    }

    if (closedir(dir) == -1) {
      xperror("closedir");
    }
  }

  sequences->size = g_list_length(sequences->seqs);
  sequences->shuffler = Shuffler_new(sequences->size);
  Shuffler_set_mode(sequences->shuffler, Context_get_shuffler_mode(BD_SEQUENCES));
  Shuffler_verbose(sequences->shuffler);
}


void
Sequences_free()
{
  GList *tmp;

  if (NULL == sequences) {
    return;
  }

  tmp = sequences->seqs;

  while (NULL != tmp) {
    Sequence_t *killme = (Sequence_t *)tmp->data;

    Sequence_delete(killme);
    tmp = g_list_next(tmp);
  }

  g_list_free(sequences->seqs);
  Shuffler_delete(sequences->shuffler);
  xfree(sequences);
  xfree(sequences_dir);
}


/* TODO un g_list_find */
Sequence_t *
Sequences_find(const uint32_t id)
{
  GList *tmp;

  assert(NULL != sequences);
  tmp = sequences->seqs;

  while (NULL != tmp) {
    Sequence_t *s = (Sequence_t *)tmp->data;

    if (s->id == id) {
      return s;
    }

    tmp = g_list_next(tmp);
  }

  return NULL;
}


void
Sequences_set_dir(char *dir)
{
  sequences_dir = dir;
}


#define SAVEDIR ".lebiniou/sequences"

/* FIXME check for possible memory leak (sequences_dir),
   should be freed by Sequences_delete() */
const char *
Sequences_get_dir()
{
  if (NULL == sequences_dir) {
    const gchar *home_dir;

    home_dir = g_get_home_dir();
    sequences_dir = g_strdup_printf("%s/%s", home_dir, SAVEDIR);
#ifdef DEBUG
    printf("[i] Setting default sequences directory: '%s'\n", sequences_dir);
#endif
  } else {
#ifdef DEBUG
    printf("[i] Using sequences directory: '%s'\n", sequences_dir);
#endif
  }

  return sequences_dir;
}
