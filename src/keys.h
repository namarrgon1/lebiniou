/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_EVENTS_H
#define __BINIOU_EVENTS_H

#include <SDL2/SDL.h>
#include "biniou.h"


static inline int
BKEY(const BKey_t *k, const uint32_t v)
{
  if (k->mod == KMOD_NONE) {
    return (k->val == v);
  }
  return ((k->val == v) && (k->mod & KMOD_NUM)
          && (!(k->mod & KMOD_SHIFT))
          && (!(k->mod & KMOD_CTRL))
          && (!(k->mod & KMOD_ALT)));
}


static inline int
BSHIFT(const BKey_t *k, const uint32_t v)
{
  return ((k->val == v) && (k->mod & KMOD_SHIFT) && !(k->mod & KMOD_CTRL) && !(k->mod & KMOD_ALT));
}


static inline int
BCTRL(const BKey_t *k, const uint32_t v)
{
  return ((k->val == v) && (k->mod & KMOD_CTRL) && !(k->mod & KMOD_SHIFT) && !(k->mod & KMOD_ALT));
}


static inline int
BALT(const BKey_t *k, const uint32_t v)
{
  return ((k->val == v) && (k->mod & KMOD_ALT) && !(k->mod & KMOD_SHIFT) && !(k->mod & KMOD_CTRL));
}


static inline int
BCTRLSHIFT(const BKey_t *k, const uint32_t v)
{
  return ((k->val == v) && (k->mod & KMOD_CTRL) && (k->mod & KMOD_SHIFT) && !(k->mod & KMOD_ALT));
}


static inline int
BALTSHIFT(const BKey_t *k, const uint32_t v)
{
  return ((k->val == v) && (k->mod & KMOD_ALT) && (k->mod & KMOD_SHIFT));
}

#endif /* __BINIOU_EVENTS_H */
