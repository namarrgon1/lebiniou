/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_IMAGES_H
#define __BINIOU_IMAGES_H

#include "image_8bits.h"


typedef struct Images_s {
  Image8_t **imgs;
  uint16_t size;
} Images_t;

extern Images_t *images;

void Images_new(const char *, const char *);
void Images_delete();

const char *Images_name(const uint32_t);
int32_t Images_index(const uint32_t);
uint32_t Images_find(const char *);
uint32_t Images_random_id();
const Image8_t *Images_random();

#endif /* __BINIOU_IMAGES_H */
