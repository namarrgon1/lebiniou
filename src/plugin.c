/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include "biniou.h"
#include "plugin.h"

/* Note: dlsym prevents us from fully compiling in -pedantic..
 * see http://www.trilithium.com/johan/2004/12/problem-with-dlsym/
 */

#ifdef DEBUG
#define B_DLSYM(VAR, SYM)                                         \
  (VAR) = (void (*)(struct Context_s *)) dlsym(p->handle, (SYM)); \
  if (libbiniou_verbose && (NULL != (VAR)))                       \
    printf("[p] >> %s has '%s'\n", p->name, (SYM))
#define B_GOTSYM(VAR, SYM) if (NULL != (VAR))                     \
    printf("[p] >> %s has '%s'\n", p->name, (SYM));
#else
#define B_DLSYM(VAR, SYM)                                         \
  (VAR) = (void (*)(struct Context_s *)) dlsym(p->handle, (SYM))
#define B_GOTSYM(VAR, SYM) { }
#endif


static Plugin_t *
Plugin_load(Plugin_t *p)
{
  const char *error = NULL;
  u_long *_tmp;

  assert(NULL != p);

  p->handle = dlopen(p->file, RTLD_NOW);

  if (NULL == p->handle) {
    error = dlerror();
    VERBOSE(fprintf(stderr, "[!] Failed to load plugin '%s': %s\n", p->name, error));
    xfree(p->name);
    xfree(p->file);
    xfree(p);

    return NULL;
  } else {
    VERBOSE(printf("[p] Loading plugin '%s'", p->name));
  }
  fflush(stdout);

  uint32_t *version_ptr = (uint32_t *) dlsym(p->handle, "version");
  if (NULL != version_ptr) {
    p->version = *version_ptr;
#ifdef DEBUG
    printf("plugin version: %d\n", p->version);
#endif
  }

  _tmp = (u_long *) dlsym(p->handle, "options");
  if (NULL == _tmp) {
    error = dlerror();
    xerror("Plugin MUST define options (%s)\n", error);
  } else {
    p->options = _tmp;
  }

  _tmp = (u_long *) dlsym(p->handle, "mode");
  p->mode = _tmp;

  /* get display name */
  p->dname = (char *) dlsym(p->handle, "dname");
  B_GOTSYM(p->dname, "dname");
  if (NULL == p->dname) {
    p->dname = p->name;
  }

  /* get description */
  p->desc = (char *) dlsym(p->handle, "desc");
  B_GOTSYM(p->desc, "desc");

  p->create = (int8_t (*)(struct Context_s *)) dlsym(p->handle, "create");
  B_DLSYM(p->destroy, "destroy");
  p->check_version = (int8_t (*)(uint32_t)) dlsym(p->handle, "check_version");
  B_DLSYM(p->run, "run");
  B_DLSYM(p->on_switch_on, "on_switch_on");
  B_DLSYM(p->on_switch_off, "on_switch_off");

  /* Output plugin stuff */
  p->fullscreen = (void (*)(int)) dlsym(p->handle, "fullscreen");
  B_GOTSYM(p->fullscreen, "fullscreen");
  p->switch_cursor = (void (*)(void)) dlsym(p->handle, "switch_cursor");
  B_GOTSYM(p->switch_cursor, "switch_cursor");

  /* Input plugin stuff (? mainly -to check --oliv3) */
  p->jthread = (void *(*)(void *)) dlsym(p->handle, "jthread");
  B_GOTSYM(p->jthread, "jthread");

  p->parameters = (json_t *(*)(struct Context_s *, json_t *)) dlsym(p->handle, "parameters");

  return p;
}


static void
Plugin_unload(Plugin_t *p)
{
  assert (NULL != p);

  /* FIXME error checking there, ie if plugin fails to destroy */
  if (NULL != p->jthread) {
    VERBOSE(printf("[p] Joining thread from plugin '%s'... ", p->name));
    pthread_join(p->thread, NULL);
  } else {
    if (p->calls) {
      VERBOSE(printf("[p] Unloading plugin '%s' (%li call%s)... ", p->name, p->calls,
                     ((p->calls == 1) ? "" : "s")));
    } else {
      VERBOSE(printf("[p] Unloading plugin '%s'... ", p->name));
    }
  }

  if (NULL != p->destroy) {
    p->destroy(context);
  }

#ifndef DISABLE_DLCLOSE
  VERBOSE(printf("dlclose... "));
  dlclose(p->handle);
#endif

  VERBOSE(printf("done.\n"));
}


Plugin_t *
Plugin_new(const char *directory, const char *name, const enum PluginType type)
{
  Plugin_t *p = xcalloc(1, sizeof(Plugin_t));

  assert(NULL != name);
  assert(NULL != directory);

  p->name = strdup(name);
  p->calls = 0;

  if (type == PL_INPUT) {
    p->file = g_strdup_printf("%s/input/%s/%s.so", directory, name, name);
  } else if (type == PL_MAIN) {
    p->file = g_strdup_printf("%s/main/%s/%s.so", directory, name, name);
  } else if (type == PL_OUTPUT) {
    p->file = g_strdup_printf("%s/output/%s/%s.so", directory, name, name);
  }

  return Plugin_load(p);
}


void
Plugin_delete(Plugin_t *p)
{
  assert(NULL != p);
  Plugin_unload(p);
  xfree(p->name);
  g_free(p->file);
  xfree(p);
}


void
Plugin_reload(Plugin_t *p)
{
  assert(NULL != p);
  Plugin_unload(p);
  Plugin_load(p);
  VERBOSE(printf("[p] Reloaded plugin '%s'\n", p->name));
}


int8_t
Plugin_init(Plugin_t *p)
{
  int8_t res = 1;
  assert(NULL != p);

  if (NULL != p->create) {
    VERBOSE(printf("[+] Initializing plugin %s\n", p->name));
    res = p->create(context);
  }

  if ((NULL != p->jthread) && res) {
    pthread_create(&p->thread, NULL, p->jthread, (void *)context);
    VERBOSE(printf("[p] Launched thread %s\n", p->name));
  }

  return res;
}


static char *
Plugin_uppercase(const char *str)
{
  char *tmp;

  assert(NULL != str);
  tmp = strdup(str);

  assert(NULL != tmp);
  assert(tmp[0] != '\0');
  tmp[0] = (char)toupper((int)tmp[0]);

  return tmp;
}


char *
Plugin_name(const Plugin_t *p)
{
  assert(NULL != p);
  return Plugin_uppercase(p->name);
}


char *
Plugin_dname(const Plugin_t *p)
{
  assert(NULL != p);
  return Plugin_uppercase(p->dname);
}


uint8_t
plugin_parameter_number(json_t *in_parameters)
{
  uint8_t n = 0;
  for (void *iter = json_object_iter(in_parameters); NULL != iter; n++) {
    iter = json_object_iter_next(in_parameters, iter);
  }
  return n;
}


void
plugin_parameters_add_int(json_t *params, const char *name, int v, int dec, int inc)
{
  json_t *param = json_object();
  json_object_set_new(param, "value", json_integer(v));
  json_object_set_new(param, "dec", json_integer(dec));
  json_object_set_new(param, "inc", json_integer(inc));
  json_object_set_new(params, name, param);
}


void
plugin_parameters_add_double(json_t *params, const char *name, double v, double dec, double inc)
{
  json_t *param = json_object();
  json_object_set_new(param, "value", json_real(v));
  json_object_set_new(param, "dec", json_real(dec));
  json_object_set_new(param, "inc", json_real(inc));
  json_object_set_new(params, name, param);
}


void
plugin_parameters_add_string_list(json_t *params, const char *name, uint32_t nb_elems, const char **elems, uint32_t elem_id)
{
  json_t *param = json_object();
  json_object_set_new(param, "value", json_string(elems[elem_id]));
  json_object_set_new(param, "dec", json_integer(-1));
  json_object_set_new(param, "inc", json_integer(1));

  json_t *value_list = json_array();
  for (uint32_t n = 0; n < nb_elems; n++) {
    json_array_append_new(value_list, json_string(elems[n]));
  }
  json_object_set_new(param, "value_list", value_list);

  json_object_set_new(params, name, param);
}


uint8_t
plugin_parameter_parse_int(const json_t *in_parameters, const char *name, int *value)
{
  json_t *p = json_object_get(in_parameters, name);

  if (NULL != p) {
    json_t *j_value = json_object_get(p, "value");
    if (json_is_integer(j_value)) {
      *value = json_integer_value(j_value);
      return 1;
    }
  }

  return 0;
}


uint8_t
plugin_parameter_parse_int_range(const json_t *in_parameters, const char *name, int *value, int vmin, int vmax)
{
  int new_value = 0;
  uint8_t ret = plugin_parameter_parse_int(in_parameters, name, &new_value);

  if (new_value >= vmin && new_value <= vmax && ret == 1) {
    if (*value != new_value) {
      ret |= PLUGIN_PARAMETER_CHANGED;
    }

    *value = new_value;
  }

  return ret;
}


uint8_t
plugin_parameter_parse_double(const json_t *in_parameters, const char *name, double *value)
{
  json_t *p = json_object_get(in_parameters, name);

  if (NULL != p) {
    json_t *j_value = json_object_get(p, "value");
    if (json_is_real(j_value)) {
      *value = json_real_value(j_value);
      return 1;
    }
  }

  return 0;
}


uint8_t
plugin_parameter_parse_double_range(const json_t *in_parameters, const char *name, double *value, double vmin, double vmax)
{
  double new_value = 0;
  uint8_t ret = plugin_parameter_parse_double(in_parameters, name, &new_value);

  if (new_value >= vmin && new_value <= vmax && ret == 1) {
    if (*value != new_value) {
      ret |= PLUGIN_PARAMETER_CHANGED;
    }

    *value = new_value;
  }

  return ret;
}


uint8_t
plugin_parameter_parse_string(const json_t *in_parameters, const char *name, char **value)
{
  json_t *p = json_object_get(in_parameters, name);

  if (NULL != p) {
    json_t *j_value = json_object_get(p, "value");
    if (json_is_string(j_value)) {
      const char *str = json_string_value(j_value);
      *value = (char *)str;
      return 1;
    }
  }

  return 0;
}


uint8_t
plugin_parameter_parse_string_list_as_int_range(const json_t *in_parameters, const char *name,
    uint32_t nb_elems, const char **elems, int *value, int vmin, int vmax)
{
  uint8_t ret       = 0;
  int     new_value = 0;
  char   *str       = NULL;

  if (plugin_parameter_parse_string(in_parameters, name, &str)) {
    for (uint32_t n = 0; n < nb_elems; n++) {
      if (strcmp(elems[n], str) == 0) {
        new_value = n;

        if (new_value >= vmin && new_value <= vmax) {
          ret = 1;
          if (*value != new_value) {
            ret |= PLUGIN_PARAMETER_CHANGED;
          }

          *value = new_value;
        }
      }
    }
  }

  return ret;
}


uint8_t
plugin_parameter_find_string_in_list(const json_t *in_parameters, const char *name, int *value)
{
  uint8_t ret = 0;

  json_t *j_value = json_object_get(in_parameters, "value");
  const char *str = json_string_value(j_value);

  json_t *j_value_list = json_object_get(in_parameters, "value_list");
  if (json_is_array(j_value_list) && (NULL != str)) {
    for (uint32_t n = 0; n < (uint32_t)json_array_size(j_value_list); n++) {
      const char *list_str = json_string_value(json_array_get(j_value_list, n));

      if (strcmp(list_str, str) == 0) {
        *value = n;
        ret = 1;
        break;
      }
    }
  }

  return ret;
}


json_t *
plugin_parameter_change_selected(Context_t *ctx, const char *delta, const float factor)
{
  json_t *ret = NULL;

  if (NULL != plugins->selected->parameters) {
    json_t *j_params = plugins->selected->parameters(ctx, NULL);
    void *params_iter = json_object_iter(j_params);
    for (uint8_t n = 0; n < plugins->selected->selected_param; n++) {
      params_iter = json_object_iter_next(j_params, params_iter);
    }
    json_t *p = json_object_iter_value(params_iter);
    json_t *j_delta = json_object_get(p, delta);
    json_t *j_value = json_object_get(p, "value");

    if ((NULL != j_delta) && (NULL != j_value)) {
      if (json_is_integer(j_delta) && json_is_integer(j_value)) {
        int delta = json_integer_value(j_delta) * factor;
        int value = json_integer_value(j_value);

        json_object_del(p, "value");
        json_object_set_new(p, "value", json_integer(value + delta));
        ret = plugins->selected->parameters(ctx, j_params);

      } else if (json_is_real(j_delta) && json_is_real(j_value)) {
        double delta = json_real_value(j_delta) * factor;
        double value = json_real_value(j_value);

        json_object_del(p, "value");
        json_object_set_new(p, "value", json_real(value + delta));
        ret = plugins->selected->parameters(ctx, j_params);

      } else if (json_is_integer(j_delta) && json_is_string(j_value)) {
        int delta = json_integer_value(j_delta) * factor;
        int list_index = 0;

        if (plugin_parameter_find_string_in_list(p, "value", &list_index)) {
          json_t *j_value_list = json_object_get(p, "value_list");
          int N = (int)json_array_size(j_value_list);

          int new_value = list_index + delta;
          if (new_value >= N) {
            new_value = new_value % N;
          } else if (new_value < 0) {
            new_value = N + new_value % N;
          }

          const char *str = json_string_value(json_array_get(j_value_list, new_value));
          json_object_del(p, "value");
          json_object_set_new(p, "value", json_string(str));
          ret = plugins->selected->parameters(ctx, j_params);
        }
      }
    }

    json_decref(j_params);
  }

  return ret;
}


json_t *
plugin_parameters_to_saved_parameters(json_t *in_parameters)
{
  json_t *out_parameters = json_object();

  const char *in_name;
  json_t *in_param;
  json_object_foreach(in_parameters, in_name, in_param) {
#ifdef DEBUG
    printf("param name : %s\n", in_name);
#endif
    json_t *value = json_object_get(in_param, "value");
    if (NULL != value) {
      json_t *param = json_object();
      json_object_set(param, "value", value);
      json_object_set_new(out_parameters, in_name, param);
    }
  }

  return out_parameters;
}
