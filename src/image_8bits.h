/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_IMAGE_8BITS_H
#define __BINIOU_IMAGE_8BITS_H

#include "buffer_8bits.h"

typedef struct Image8_s {
  uint32_t  id;
  char      *name;
  char      *dname;
  Buffer8_t *buff;
} Image8_t;

Image8_t *Image8_new(void);
Image8_t *Image8_new_default(void);
void Image8_delete(Image8_t *);
int Image8_load(Image8_t *, const uint32_t, const char *, const char *);
int Image8_load_any(Image8_t *, const char *);
void Image8_copy(const Image8_t *, Image8_t *);

#endif /* __BINIOU_IMAGE_8BITS_H */
