/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_CONTEXT_H
#define __BINIOU_CONTEXT_H

#ifdef WITH_GL
#include <GL/gl.h>
#endif
#include "input.h"
#include "sequencemanager.h"
#include "params3d.h"
#include "particles.h"
#include "imagefader.h"
#include "cmapfader.h"
#include "alarm.h"
#include "brandom.h"
#include "buffer_RGBA.h"
#ifdef WITH_ULFIUS
#include "bulfius.h"
#endif

#define NFPS     25 /* to get mean fps */

enum RandomMode { BR_NONE = 0, BR_SEQUENCES, BR_SCHEMES, BR_BOTH, BR_NB };

/* Random changes delays */
#define DELAY_MIN 15
#define DELAY_MAX 30
enum RandomDelays { BD_COLORMAPS = 0, BD_IMAGES, BD_SEQUENCES
#ifdef WITH_WEBCAM
, BD_WEBCAMS
#endif
                  };


#define ACTIVE_BUFFER  0
#define PASSIVE_BUFFER 1
#define SAVE_BUFFER    2

#ifdef WITH_WEBCAM
#define NSCREENS 6 /* <--- ??? oliv3 */
#else
#define NSCREENS 3
#endif

#define MAX_CAMS       6

/* store the last N frames from the webcam -same as EffectTV */
#define CAM_SAVE 32

typedef struct BKey_s {
  uint32_t val;
  uint16_t mod;
} BKey_t;

typedef struct Context_s {
  u_char    running:1;

  Plugin_t  *input_plugin;
  Input_t   *input;

  /* Buffers */
  Buffer8_t *buffers[NSCREENS];
  BufferRGBA_t *rgba_buffers[NSCREENS];
  /* 0 = active_buffer
   * 1 = passive_buffer - for double-buffering
   * 2 = save_buffer    - push/pop screen for lens effects
   */

  int webcams;
#ifdef WITH_WEBCAM
  /* webcam */
  Buffer8_t *cam_save[MAX_CAMS][CAM_SAVE];
  Buffer8_t *cam_ref[MAX_CAMS];
  Buffer8_t *cam_ref0[MAX_CAMS]; /* reference picture taken on program start */
  u_char ref_taken[MAX_CAMS];

  /* auto-change webcams */
  Shuffler_t *webcams_shuffler;
  Alarm_t    *a_webcams;

  /* Webcam */
  pthread_mutex_t cam_mtx[MAX_CAMS];
  u_char cam; /* active webcam */
#endif

  /* Faders */
  ImageFader_t *imgf;
  Alarm_t *a_images;

  CmapFader_t *cf;
  Alarm_t *a_cmaps;

  GSList    *outputs;

  SequenceManager_t *sm;

  Params3d_t params3d;

  /* auto-change sequences (user or generated) */
  enum RandomMode random_mode;
  Alarm_t  *a_random;

  u_long   frames;

  GList    *events;
  u_long   nb_events;

  BTimer_t *timer;

  /* FPS stuff */
  u_char   sync_fps:1;
  u_short  max_fps;
  float    i_max_fps; /* inverse of the maximum frames per second */
  int      fps[NFPS];
  BTimer_t  *fps_timer;

  uint8_t  window_decorations:1;
  u_char   fullscreen:1;
  u_char   display_colormap:1;
  u_char   take_screenshot:1;
  u_char   bypass:1;

  /* Banks */
  u_char   bank_mode; /* will default to SEQUENCES */
  uint32_t banks[3][MAX_BANKS][MAX_BANKS];
  u_char   bankset[3];
  u_char   bank[3]; /* the bank used */

  /* OpenGL */
#ifdef WITH_GL
  u_char texture_ready:1;
  u_char texture_used:1;
  u_char gl_done:1;
  GLuint textures[NSCREENS];
  GLuint cam_textures[MAX_CAMS];
  u_char pulse_3d:1;
  u_char force_cube:1;
#endif

  /* Target */
  Image8_t *target_pic;

  Plugin_t *locked; // Locked plugin feature

  uint32_t input_size;

  char auto_colormaps;
  char auto_images;

  /* used to randomly mix buffers */
  Buffer8_t *random;

  /* ulfius */
#ifdef WITH_ULFIUS
  struct _u_instance instance;
#endif
} Context_t;


Context_t *Context_new(const int);
void Context_delete(Context_t *);

void Context_set(Context_t *);
void Context_set_colormap(Context_t *);
void Context_set_image(Context_t *);
void Context_run(Context_t *);
void Context_update(Context_t *);
void Context_update_auto(Context_t *);

int  Context_add_rand(Sequence_t *, const enum PluginOptions, const int, const Plugin_t *);
void Context_randomize(Context_t *);

void Context_set_max_fps(Context_t *, const u_short);
void Context_set_engine_random_mode(Context_t *, const enum RandomMode);

void Context_insert_plugin(Context_t *, Plugin_t *);
void Context_remove_plugin(Context_t *, Plugin_t *);

void Context_screenshot(const Context_t *);
float Context_fps(const Context_t *);

void Context_previous_sequence(Context_t *);
void Context_next_sequence(Context_t *);
void Context_latest_sequence(Context_t *);
void Context_random_sequence(Context_t *);
void Context_set_sequence(Context_t *, const uint32_t);

/* Events */
int Context_event(Context_t *, const Event_t *); /* TODO void ? */
void Context_process_events(Context_t *);
void Context_add_event(Context_t *, const Event_t *);
void Context_send_event(Context_t *, const enum RcptTo, const enum Command,
                        const enum Arg);

Buffer8_t *active_buffer(const Context_t *);
Buffer8_t *passive_buffer(const Context_t *);


static inline Buffer8_t *
save_buffer(const Context_t *ctx)
{
  return ctx->buffers[SAVE_BUFFER];
}

static inline void
swap_buffers(Context_t *ctx)
{
  Buffer8_t *tmp = ctx->buffers[ACTIVE_BUFFER];
  ctx->buffers[ACTIVE_BUFFER] = ctx->buffers[PASSIVE_BUFFER];
  ctx->buffers[PASSIVE_BUFFER] = tmp;
}

static inline void
push_buffer(const Context_t *ctx)
{
  Buffer8_copy(active_buffer(ctx), save_buffer(ctx));
}

static inline void
pop_buffer(const Context_t *ctx)
{
  Buffer8_copy(save_buffer(ctx), active_buffer(ctx));
}


/* TODO: deprecate export_RGB*, use export RGBA* */
Pixel_t *export_RGB_buffer(const Context_t *, const u_char, const u_char);
Pixel_t *export_BGR_buffer(const Context_t *, const u_char, const u_char);

Pixel_t *export_RGB_active_buffer(const Context_t *, const u_char);
Pixel_t *export_BGR_active_buffer(const Context_t *, const u_char);

Pixel_t *export_YUV_buffer(const Context_t *, const u_char, const u_char);
Pixel_t *export_YUV_active_buffer(const Context_t *, const u_char);

const RGBA_t *export_RGBA_buffer(const Context_t *, const u_char);
const RGBA_t *export_RGBA_active_buffer(const Context_t *);

void Context_save_banks(const Context_t *);
void Context_load_banks(Context_t *);
void Context_use_sequence_bank(Context_t *, const u_char);
void Context_clear_bank(Context_t *, const u_char);
void Context_store_bank(Context_t *, const u_char);
void Context_use_bankset(Context_t *, const u_char);

void Context_push_webcam(Context_t *, Buffer8_t *, const int);

void Context_make_GL_RGBA_texture(Context_t *ctx, const u_char);
#ifdef WITH_WEBCAM
void Context_make_GL_gray_texture(Context_t *ctx, const u_char);
#endif

void Context_set_shuffler_mode(const enum RandomDelays, const enum ShufflerMode);
enum ShufflerMode Context_get_shuffler_mode(const enum RandomDelays);

void Context_set_input_size(Context_t *, const uint32_t);
uint32_t Context_get_input_size(const Context_t *);

/* volume scaling */
void Context_set_volume_scale(Context_t *, const double);
double Context_get_volume_scale(const Context_t *);

void Context_mix_buffers(const Context_t *, Buffer8_t *[2]);

#ifdef WITH_ULFIUS
void Context_start_ulfius(Context_t *);
void Context_stop_ulfius(Context_t *);
#endif

#endif /* __BINIOU_CONTEXT_H */
