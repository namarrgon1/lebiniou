/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include "context.h"
#include "xmlutils.h"


void
Context_store_bank(Context_t *ctx, const u_char bank)
{
  assert(bank < MAX_BANKS);
  if (ctx->bank_mode == SEQUENCES) {
    ctx->banks[SEQUENCES][ctx->bankset[SEQUENCES]][bank] = ctx->sm->cur->id;
  } else if (ctx->bank_mode == COLORMAPS) {
    ctx->banks[COLORMAPS][ctx->bankset[COLORMAPS]][bank] = ctx->sm->cur->cmap_id;
  } else if (ctx->bank_mode == IMAGES) {
    ctx->banks[IMAGES][ctx->bankset[IMAGES]][bank] = ctx->sm->cur->image_id;
  }
}


void
Context_use_bankset(Context_t *ctx, const u_char bs)
{
  assert(bs < MAX_BANKS);
  ctx->bankset[ctx->bank_mode] = bs;
}


void
Context_save_banks(const Context_t *ctx)
{
  xmlDoc *doc;
  xmlNode *node;
  const gchar *home_dir = NULL;
  char *filename;
  int m, bs, b;

  home_dir = g_get_home_dir();
  filename = g_strdup_printf("%s/." PACKAGE_NAME, home_dir);
  rmkdir(filename);
  g_free(filename);
  filename = g_strdup_printf("%s/." PACKAGE_NAME "/banks.xml", home_dir);

  /* FIXME check return code of xml* functions */
  doc = xmlNewDoc((const xmlChar *)"1.0");
  node = doc->children = xmlNewDocNode(doc, NULL, (const xmlChar *)"banks", NULL);

  for (m = 0; m < 3; m++)
    for (bs = 0; bs < MAX_BANKS; bs++)
      for (b = 0; b < MAX_BANKS; b++)
        if (ctx->banks[m][bs][b]) {
          char str[20];
          xmlNode *nd;

          memset(str, '\0', 20);
          g_snprintf(str, 19, "%"PRIu32, ctx->banks[m][bs][b]);

          nd = xmlNewChild(node, NULL, (const xmlChar *)"bank", (const xmlChar *)str);

          memset(str, '\0', 20);
          g_snprintf(str, 19, "%d", bs);
          xmlSetProp(nd, (const xmlChar *)"set", (const xmlChar *)str);

          memset(str, '\0', 20);
          g_snprintf(str, 19, "%d", m);
          xmlSetProp(nd, (const xmlChar *)"mode", (const xmlChar *)str);

          xml_set_id(nd, b);
        }

  xmlKeepBlanksDefault(0);
  xmlSaveFormatFile(filename, doc, 1);
  xmlFreeDoc(doc);
  g_free(filename);
}


void
Context_load_banks(Context_t *ctx)
{
  xmlDocPtr doc = NULL; /* XmlTree */
  const gchar *home_dir = NULL;
  char *filename;
  xmlNodePtr node = NULL, bank_node = NULL;
  int m, i, j;
  int res;
  struct stat dummy;


  for (m = 0; m < 3; m++)
    for (i = 0; i < MAX_BANKS; i++)
      for (j = 0; j < MAX_BANKS; j++) {
        ctx->banks[m][i][j] = 0;
      }

  home_dir = g_get_home_dir();
  filename = g_strdup_printf("%s/." PACKAGE_NAME "/banks.xml", home_dir);

  res = stat(filename, &dummy);
  if (-1 == res) {
    g_free(filename);
    return;
  }

  /* BLA ! */
  xmlKeepBlanksDefault(0);
  xmlSubstituteEntitiesDefault(1);

  doc = xmlParseFile(filename);
  g_free(filename);
  if (NULL == doc) {
    return;
  }

  node = xmlDocGetRootElement(doc);
  if (NULL == node) {
    xerror("Banks: xmlDocGetRootElement error\n");
  }

  node = xmlFindElement("banks", node);
  if (NULL == node) {
    xerror("Banks: no <banks> found\n");
  }

  bank_node = node->xmlChildrenNode;
  if (NULL == bank_node) { /* Empty banks.xml file */
    return;
  }

  while (NULL != bank_node) {
    long tmp0, tmp1, tmp2;
    xmlChar *youhou;
    int res;
    long tmp3;

    youhou = xmlGetProp(bank_node, (const xmlChar *)"mode");
    tmp0 = getintfield(youhou);
    xmlFree(youhou);
    if (-1 == tmp0) {
      tmp0 = SEQUENCES;
    } else {
      assert(tmp0 >= 0);
      assert(tmp0 < 3);
    }

    youhou = xmlGetProp(bank_node, (const xmlChar *)"set");
    tmp1 = getintfield(youhou);
    xmlFree(youhou);
    assert(tmp1 >= 0);
    assert(tmp1 < MAX_BANKS);

    youhou = xmlGetProp(bank_node, (const xmlChar *)"id");
    tmp2 = getintfield(youhou);
    xmlFree(youhou);
    assert(tmp2 >= 0);
    assert(tmp2 < MAX_BANKS);

    res = xmlGetOptionalLong(doc, bank_node, &tmp3);
    if (res == -1) {
      xerror("Banks: no value set\n");
    } else {
      ctx->banks[tmp0][tmp1][tmp2] = tmp3;
#ifdef DEBUG
      printf("[i] Mode: %li Bankset: %li Bank: %li id: %li\n", tmp0, tmp1, tmp2, tmp3);
#endif
    }

    bank_node = bank_node->next;
  }

  xmlFreeDoc(doc);
}
