/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmlutils.h"


xmlNodePtr
xmlFindElement(const char *element, xmlNodePtr start)
{
  assert(NULL != element); /* faudrait vraiment le chercher */

  if (NULL == start) {
    xerror("xmlFindElement(%s): got a NULL start\n", element);
  }

  while ((NULL != start) && (xmlStrcmp(start->name, (const xmlChar *)element))) {
    start = start->next;
  }

  if (NULL == start) {
#ifdef DEBUG
    printf("[!] xmlFindElement(%s): not found\n", element);
#endif
    return NULL;
  }

  /* normalement on est bon */
  assert(!xmlStrcmp(start->name, (const xmlChar *)element));

  return start;
}


int
xmlGetOptionalLong(const xmlDocPtr doc, const xmlNodePtr node, long *value)
{
  xmlChar *data;
  int res = 0;

  assert(NULL != doc);

  data = xmlNodeListGetString(doc, node->xmlChildrenNode, 1);
  if (NULL != data) {
    long tmp;

    errno = 0;
    tmp = strtol((const char *)data, NULL, 10);
    if (errno != 0) {
      *value = res = -1;
    } else {
      *value = tmp;
    }

    xmlFree(data);
  }

  return res;
}


char *
xmlGetMandatoryString(const xmlDocPtr doc, const char *element, xmlNodePtr start)
{
  xmlChar *data;
  xmlNodePtr ptr;

  assert(NULL != doc);

  ptr = xmlFindElement(element, start);
  if (NULL == ptr) {
    xerror("xmlGetMandatoryString: failed to find element <%s>\n", element);
  }

  data = xmlNodeListGetString(doc, ptr->xmlChildrenNode, 1);
  if (NULL == data) {
    xerror("xmlGetMandatoryString: failed to get data in <%s>\n", element);
  } else {
    return (char *)data;
  }

  return NULL; /* not reached */
}


long
getintfield(xmlChar *field)
{
  if (NULL != field) {
    long int i = 0;
    char *ret = malloc((xmlStrlen(field)+1)*sizeof(char));

    while (*field==' ') {
      field++;
    }

    for (i=0; field[i] && (field[i]!=' ') && (field[i]!='"'); i++) {
      ret[i]=field[i];
    }

    ret[i] = '\0';
    i=atol(ret);
    free(ret);

    return i;
  } else {
    return -1;
  }
}


int
getfloatfield_optional(xmlChar *field, float *f)
{
  if (NULL != field) {
    int i;
    char *ret = malloc((xmlStrlen(field)+1)*sizeof(char));

    while (*field==' ') {
      field++;
    }

    for (i=0; field[i] && (field[i]!=' ') && (field[i]!='"'); i++) {
      ret[i]=field[i];
    }

    ret[i] = '\0';
    *f=atof(ret);
    free(ret);
    return 0;
  } else {
    return -1;
  }
}
