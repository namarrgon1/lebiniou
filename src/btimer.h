/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_BTIMER_H
#define __BINIOU_BTIMER_H

#include "utils.h"


typedef struct BTimer_s {
  struct timeval start;
  struct timeval end;
} BTimer_t;


BTimer_t *b_timer_new(void);
void b_timer_delete(BTimer_t *);
void b_timer_start(BTimer_t *);
void b_timer_stop(BTimer_t *);
void b_timer_restart(BTimer_t *);
float b_timer_elapsed(BTimer_t *);

#endif /* __BINIOU_BTIMER_H */
