/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_OSCILLO_H
#define __BINIOU_OSCILLO_H

#include "biniou.h"

/* Generic oscillo-style drawing tools */

typedef struct Transform_s {
  Point2d_t v_before;
  Point2d_t v_i;
  Point2d_t v_after;
  u_short   v_j_factor;
  float     cos_alpha;
  float     sin_alpha;
} Transform_t;

void Transform_init(Transform_t *);

typedef struct Porteuse_s {
  uint32_t    size;
  Point2d_t   origin;
  Transform_t *trans;
  u_char      channel;
  Pixel_t     *color;
  uint8_t     *connect;
} Porteuse_t;

Porteuse_t *Porteuse_new(uint32_t, u_char);
void Porteuse_delete(Porteuse_t *);

void Porteuse_init_alpha(Porteuse_t *);
void Porteuse_draw(const Porteuse_t *, Context_t *, const int);

#endif /* __BINIOU_OSCILLO_H */
