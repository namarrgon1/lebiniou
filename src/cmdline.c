/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"
#include "webcam.h"


#ifdef WITH_WEBCAM
extern int hflip, vflip, webcams;
extern char *video_base;
#endif
extern uint8_t statistics;

#define MIN_WIDTH  80
#define MIN_HEIGHT 60


static void
usage()
{
  printf("Usage: " PACKAGE_NAME " [options]\n\n"
         "\t-b, --basedir <path>\tSet base directory [" DEFAULT_PLUGINSDIR "]\n"
         "\t-d, --datadir <path>\tSet data directory [" DEFAULT_DATADIR "]\n"
         "\t-z, --schemes <file>\tSet the schemes file [" DEFAULT_SCHEMES_FILE "]\n"
         "\t-f, --fullscreen\tSet fullscreen\n"
         "\t-h, --help\t\tDisplay this help\n"
         "\t-i, --input <plugin>\tSet input plugin [" DEFAULT_INPUT_PLUGIN "]\n"
         "\t-m, --maxfps <fps>\tSet maximum framerate [%ld]\n"
         "\t-o, --output <plugin>\tSet output plugin [" DEFAULT_OUTPUT_PLUGIN "]\n"
         "\t-r, --random <mode>\tSet auto-random mode\n"
         "\t-v, --version\t\tDisplay the version and exit\n"
         "\t-q, --quiet\t\tSuppress messages\n"
         "\t-c, --config <file>\tSet configuration file [~/" KEYFILE "]\n"
         "\t-X, --xpos <x position>\tSet left position of the window\n"
         "\t-Y, --ypos <y position>\tSet top position of the window\n"
#ifndef FIXED
         "\t-x, --width <width>\tSet width [%d]\n"
         "\t-y, --height <height>\tSet height [%d]\n"
#endif
         "\t-n, --noborder\tDisable window borders\n"
         "\t-p, --pidfile <file>\tSet the PID file [%s]\n"
         "\t-t, --themes <themes>\tComma-separated list of themes to use [biniou]\n"
#ifdef WITH_WEBCAM
         "\t-W, --webcams <webcams>\tNumber of webcams [%d]\n"
         "\t-D, --device <device>\tWebcam base [" DEFAULT_VIDEO_DEVICE "]\n"
         "\t-C, --camflip <h|v>\tFlip webcam image horizontally/vertically\n"
#endif
         "\t-S, --stats\t\tDisplay statistics\n"
         "\t-F, --fade <delay>\tSet fading delay (s)\n"
         "\n"
         "This version of " PACKAGE_STRING " was compiled with:\n"
         "\tInput plugins: " INPUT_PLUGINS "\n"
         "\tOutput plugins: " OUTPUT_PLUGINS "\n"
         "\n"
#ifdef MANLINK
         "Manual: file://" MANLINK "\n"
         "\n"
#endif
         , max_fps
#ifndef FIXED
         , DEFAULT_WIDTH
         , DEFAULT_HEIGHT
#endif
         , pid_file
#ifdef WITH_WEBCAM
         , webcams
#endif
        );

  /* TODO print default values for all options */
  exit(0);
}


void
getargs(int argc, char **argv)
{
  int ch;
#ifndef FIXED
  int w, h;
#endif

  static const char *arg_list = "b:c:C:d:D:fF:hi:m:no:p:qr:St:x:X:y:Y:vW:z:";

#if HAVE_GETOPT_LONG
  static struct option long_opt[] = {
    {"basedir", required_argument, NULL, 'b'},
    {"datadir", required_argument, NULL, 'd'},
    {"schemes", required_argument, NULL, 'z'},
    {"fullscreen", no_argument, NULL, 'f'},
    {"help", no_argument, NULL, 'h'},
    {"input", required_argument, NULL, 'i'},
    {"maxfps", required_argument, NULL, 'm'},
    {"noborder", no_argument, NULL, 'n'},
    {"output", required_argument, NULL, 'o'},
    {"random", required_argument, NULL, 'r'},
    {"quiet", no_argument, NULL, 'q'},
    {"config", required_argument, NULL, 'c'},
    {"xpos", required_argument, NULL, 'X'},
    {"ypos", required_argument, NULL, 'Y'},
#ifndef FIXED
    {"width", required_argument, NULL, 'x'},
    {"height", required_argument, NULL, 'y'},
#endif
    {"version", no_argument, NULL, 'v'},
    {"pidfile", required_argument, NULL, 'p'},
    {"themes", required_argument, NULL, 't'},
#ifdef WITH_WEBCAM
    {"camflip", required_argument, NULL, 'C'},
    {"webcams", required_argument, NULL, 'W'},
    {"device", required_argument, NULL, 'D'},
#endif
    {"stats", no_argument, NULL, 'S'},
    {"fade", required_argument, NULL, 'F'},
    {0, 0, 0, 0}
  };


  /* Get command line arguments */
  while ((ch = getopt_long(argc, argv, arg_list, long_opt, NULL)) != -1)
#else
  while ((ch = getopt(argc, argv, arg_list)) != -1)
#endif
    switch (ch) {
      case 'b':
        if (NULL == base_dir) {
          base_dir = optarg;
        }
        break;

      case 'c':
        /* re-read configuration file */
        set_configuration(optarg);
        read_keyfile();
        break;

#ifdef WITH_WEBCAM
      case 'C':
        if (*optarg == 'h') {
          hflip=!hflip;
        }
        if (*optarg == 'v') {
          vflip=!vflip;
        }
        break;
#endif

      case 'd':
        if (NULL == data_dir) {
          data_dir = optarg;
        }
        break;

      case 'z':
        if (NULL == schemes_file) {
          schemes_file = optarg;
        }
        break;

      case 'f':
        fullscreen = 1;
        break;

      case 'h':
        usage();
        break;

      case 'n':
        window_decorations = 0;
        VERBOSE(printf("[c] Deactivate window decorations\n"));
        break;

      case 'i':
        if (NULL != input_plugin) {
          xfree(input_plugin);
        }
        input_plugin = optarg;
        VERBOSE(printf("[c] Setting input plugin: %s\n", input_plugin));
        break;

      case 'o':
        if (NULL != output_plugin) {
          xfree(output_plugin);
        }
        output_plugin = optarg;
        VERBOSE(printf("[c] Setting output plugin: %s\n", output_plugin));
        break;

      case 'm':
        max_fps = xatol(optarg);
        if (max_fps > 0) {
          VERBOSE(printf("[c] Maximum fps set to %li\n", max_fps));
        } else {
          xerror("Invalid max_fps (%li)\n", max_fps);
        }
        break;

      case 'r':
        random_mode = (enum RandomMode)xatol(optarg);
        if (random_mode > BR_BOTH) {
          xerror("Invalid random_mode (%d)\n", random_mode);
        } else {
          VERBOSE(printf("[c] Random mode set to %d\n", random_mode));
        }
        break;

      case 't':
        if (NULL != themes) {
          xfree(themes);
        }
        themes = strdup(optarg);
        VERBOSE(printf("[c] Using themes: %s\n", themes));
        break;

      case 'v':
        printf("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
        exit(0);
        break;

      case 'q':
        libbiniou_verbose = 0;
        break;

      case 'x':
#ifndef FIXED
        w = xatol(optarg);
        if (w >= MIN_WIDTH) {
          width = w;
          VERBOSE(printf("[c] Width set to %i\n", width));
        } else {
          xerror("Invalid width: %d (min: %d)\n", w, MIN_WIDTH);
        }
#else
        VERBOSE(fprintf(stderr, "[!] Compiled with fixed buffers, ignoring width= %li\n", xatol(optarg)));
#endif
        break;

      case 'X':
        x_origin = xatol(optarg);
        VERBOSE(printf("[c] X origin set to %i\n", x_origin));
        break;

      case 'y':
#ifndef FIXED
        h = xatol(optarg);
        if (h >= MIN_HEIGHT) {
          height = h;
          VERBOSE(printf("[c] Height set to %i\n", height));
        } else {
          xerror("Invalid height: %d (min: %d)\n", h, MIN_HEIGHT);
        }
#else
        VERBOSE(fprintf(stderr, "[!] Compiled with fixed buffers, ignoring height= %li\n", xatol(optarg)));
#endif
        break;

      case 'Y':
        y_origin = xatol(optarg);
        VERBOSE(printf("[c] Y origin set to %i\n", y_origin));
        break;

      case 'p':
        if (NULL == pid_file) {
          pid_file = optarg;
        }
        break;

#ifdef WITH_WEBCAM
      case 'W': /* webcams */
        webcams = xatol(optarg);
        if ((webcams >= 0) && (webcams <= MAX_CAMS)) {
          VERBOSE(printf("[c] webcam: grabbing %d device%s\n", webcams, (webcams == 1 ? "": "s")));
        } else {
          webcams = 1;
        }
        break;

      case 'D': /* video_base */
        if (NULL != video_base) {
          xfree(video_base);
        }
        video_base = strdup(optarg);
        VERBOSE(printf("[c] webcam: first device is %s\n", video_base));
        break;
#endif

      case 'S':
        libbiniou_verbose = 0;
        statistics = 1;
        break;

      case 'F':
        fade_delay = xatof(optarg);
        VERBOSE(printf("[c] Fading delay set to %f\n", fade_delay));
        break;

      default:
        usage();
        break;
    }

  if (NULL == base_dir) {
    base_dir = DEFAULT_PLUGINSDIR;
  }

  if (NULL == data_dir) {
    data_dir = DEFAULT_DATADIR;
  }

  if (NULL == schemes_file) {
    schemes_file = DEFAULT_SCHEMES_FILE;
  }

  if (NULL == input_plugin) {
    input_plugin = DEFAULT_INPUT_PLUGIN;
  } else if (!strcmp(input_plugin, "NULL")) {
    input_plugin = NULL;
  }

  if (NULL == output_plugin) {
    output_plugin = DEFAULT_OUTPUT_PLUGIN;
  } else if (!strcmp(output_plugin, "NULL")) {
    output_plugin = NULL;
  }

  if (NULL == themes) {
    themes = strdup("biniou");
  }

#ifdef WITH_WEBCAM
  if (NULL == video_base) {
    video_base = strdup(DEFAULT_VIDEO_DEVICE);
  }
#endif
}
