/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_CMAP_8BITS_H
#define __BINIOU_CMAP_8BITS_H

#include "rgba.h"


typedef struct Cmap8_s {
  uint32_t   id;      /* our unique hash */
  char   *name;       /* colormap name */
  char   *filename;   /* where on the filesystem it was loaded from */
  rgba_t colors[256]; /* the 256 RGBA palette */
  int    min;         /* min intensity color index */
  int    max;         /* max intensity color index */
  u_char compressed;  /* colormap loaded from a binary file */
} Cmap8_t;


/* TODO Cmap8_new(Cmap8 *, uint32_t, const char *, const char *) ? */
Cmap8_t *Cmap8_new(void);
void Cmap8_delete(Cmap8_t *);

int  Cmap8_load(Cmap8_t *, const char *);
int  Cmap8_load_binary(Cmap8_t *, const char *);
void Cmap8_copy(const Cmap8_t *, Cmap8_t *);
void Cmap8_findMinMax(Cmap8_t *);
int  Cmap8_save(Cmap8_t *);
void Cmap8_shift_left(Cmap8_t *);


#endif /* __BINIOU_CMAP_8BITS_H */
