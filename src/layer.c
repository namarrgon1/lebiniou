/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "layer.h"


Layer_t *
Layer_new(Plugin_t *p)
{
  Layer_t *l;

  l = xcalloc(1, sizeof(Layer_t));
  l->plugin = p;
  l->mode = NORMAL;

  return l;
}


void
Layer_delete(Layer_t *l)
{
  json_decref(l->plugin_parameters);
  xfree(l);
}


Layer_t *
Layer_copy(const Layer_t *from)
{
  Layer_t *l = Layer_new(from->plugin);

  l->mode = from->mode;
  l->plugin_parameters = json_deep_copy(from->plugin_parameters);

  return l;
}


enum LayerMode
LayerMode_from_string(const char *mode) {
  if (!strcmp(mode, "none"))
  {
    return NONE;
  }
  if (!strcmp(mode, "normal"))
  {
    return NORMAL;
  }
  if (!strcmp(mode, "overlay"))
  {
    return OVERLAY;
  }
  if (!strcmp(mode, "xor"))
  {
    return XOR;
  }
  if (!strcmp(mode, "average"))
  {
    return AVERAGE;
  }
  if (!strcmp(mode, "random"))
  {
    return RANDOM;
  }

  printf("[!] Failed to parse mode '%s', setting to NORMAL\n", mode);

  return NORMAL;
}


const char *
LayerMode_to_string(const enum LayerMode mode)
{
  switch (mode) {
    case NONE:
      return "none";
      break;

    case NORMAL:
      return "normal";
      break;

    case OVERLAY:
      return "overlay";
      break;

    case XOR:
      return "xor";
      break;

    case AVERAGE:
      return "average";
      break;

    case RANDOM:
      return "random";
      break;

    default:
      xerror("LayerMode_to_string: unknown mode= %d\n", mode);
      break;
  }

  return NULL; /* not reached */
}


const char *
LayerMode_to_OSD_string(const enum LayerMode mode)
{
  if ((unsigned)mode > RANDOM) {
    return "???";
  } else
    return
      &"---\0NOR\0OVL\0XOR\0AVG\0RND\0"[4*mode*sizeof(char)];
}
