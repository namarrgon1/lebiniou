/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_ALARM_H
#define __BINIOU_ALARM_H

#include "utils.h"
#include "btimer.h"


typedef struct Alarm_s {
  BTimer_t *timer;
  int32_t min;
  int32_t max;
  u_short delay;
} Alarm_t;


Alarm_t *Alarm_new(const u_short, const u_short);
void     Alarm_delete(Alarm_t *);
void     Alarm_init(Alarm_t *);
int      Alarm_ring(Alarm_t *);
float    Alarm_elapsed_pct(Alarm_t *); /* percentage of alarm time elapsed */

#endif /* __BINIOU_ALARM_H */
