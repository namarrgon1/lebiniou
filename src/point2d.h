/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_POINT2D_H
#define __BINIOU_POINT2D_H


typedef struct Point2d_s {
  float x;
  float y;
} Point2d_t;


/* operations on 2D points */
static inline Point2d_t
p2d_add(const Point2d_t *p0, const Point2d_t *p1)
{
  Point2d_t p;

  p.x = p0->x + p1->x;
  p.y = p0->y + p1->y;

  return p;
}


static inline Point2d_t
p2d_sub(const Point2d_t *p0, const Point2d_t *p1)
{
  Point2d_t p;

  p.x = p0->x - p1->x;
  p.y = p0->y - p1->y;

  return p;
}


static inline Point2d_t
p2d_mul(const Point2d_t *p0, const float f)
{
  Point2d_t p;

  p.x = p0->x * f;
  p.y = p0->y * f;

  return p;
}


static inline Point2d_t
p2d_div(const Point2d_t *p0, const float f)
{
  Point2d_t p;

  p.x = p0->x / f;
  p.y = p0->y / f;

  return p;
}

#endif /* __BINIOU_POINT2D_H */
