/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_CONSTANTS_H
#define __BINIOU_CONSTANTS_H

/* maximum logical buffer size */
#ifndef FIXED
extern unsigned short WIDTH, HEIGHT;
#endif
#define BUFFSIZE      (unsigned long)(WIDTH*HEIGHT)
#define RGB_BUFFSIZE  (3*BUFFSIZE)
#define RGBA_BUFFSIZE (4*BUFFSIZE)

/* min/max indices */
#define MINX       (0)
#define MINY       (0)
#define MAXX       (WIDTH-1)
#define MAXY       (HEIGHT-1)

/* misc */
#define HWIDTH     (WIDTH>>1)
#define HHEIGHT    (HEIGHT>>1)

#define HMAXX      (MAXX>>1)
#define HMAXY      (MAXY>>1)

#define CENTERX    (HWIDTH-1)
#define CENTERY    (HHEIGHT-1)

#define MINSCREEN  ((WIDTH < HEIGHT) ? WIDTH : HEIGHT)
#define MAXSCREEN  ((WIDTH >= HEIGHT) ? WIDTH : HEIGHT)

#define HMINSCREEN (MINSCREEN>>1)
#define HMAXSCREEN (MAXSCREEN>>1)

/* character buffers */
#define MAXLEN     _POSIX2_LINE_MAX

/* maximum length of a sequence */
#define MAX_SEQ_LEN  254

/* handy shortcuts */
#define DEC(val, maxval) do { if (!val) val = maxval-1; else --val; } while (0)
#define INC(val, maxval) do { if (++val == maxval) val = 0; } while (0)

/* default configuration file */
#define KEYFILE "." PACKAGE_NAME "rc"

/* banks */
#define MAX_BANKS 12

/* 3D z-buffer */
#define ZMAX 6.0

/* volume scaling */
#define VOLUME_SCALE_STEP 0.1

/* 3D scale factor */
#define SCALE_FACTOR_MIN  1.03
#define SCALE_FACTOR_MULT 0.9 // OSD changes via mouse wheel
/* 3D rotations */
#define DEFAULT_ROT_AMOUNT 0.0025
#define ROT_AMOUNT_MIN     0.001
/* rotations factor */
#define DEFAULT_ROT_FACTOR 4

/* delay for ancillary threads (OSD, webcams) */
#define THREADS_DELAY 40

#endif /* __BINIOU_CONSTANTS_H */
