/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_COLORMAPS_H
#define __BINIOU_COLORMAPS_H

#include "cmap_8bits.h"


typedef struct Colormaps_s {
  Cmap8_t  **cmaps;
  uint16_t size;
} Colormaps_t;

extern Colormaps_t  *colormaps;


void Colormaps_new(const char *);
void Colormaps_delete();

const char *Colormaps_name(const uint32_t);
int32_t Colormaps_index(const uint32_t);
int32_t Colormaps_find(const char *);
uint32_t Colormaps_random_id();

#endif /* __BINIOU_COLORMAPS_H */
