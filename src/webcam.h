/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WEBCAM_H
#define __WEBCAM_H

#include "context.h"
#if HAVE_LINUX_VIDEODEV2_H
#include <linux/videodev2.h>
#endif
#if HAVE_SYS_VIDEOIO_H
#include <sys/videoio.h>
#endif
#include "globals.h"

#ifndef CAPTURE_SET
#define CAP_WIDTH  640
#define CAP_HEIGHT 480
#endif /* CAPTURE_SET */

#define CLEAR(x) memset(&(x), 0, sizeof(x))

typedef enum {
  IO_METHOD_READ,
  IO_METHOD_MMAP
} io_method;

typedef struct buffer {
  void   *start;
  size_t length;
} buffer_t;


#define DEFAULT_VIDEO_DEVICE "/dev/video"
#define WEBCAM_MMAP 4

typedef struct {
  int       cam_no;
  Context_t *ctx;
  io_method io;
  int       fd;
  int       n_buffers;
  buffer_t  *buffers;
  int       hflip;
  int       vflip;
} webcam_t;

void parse_options();

int  open_device(webcam_t *, const uint8_t);
int  init_device(webcam_t *);
void enumerate_cids(webcam_t *);
void start_capturing(webcam_t *);
int  list_inputs(const webcam_t *);

void* loop(void *);

void stop_capturing(webcam_t *);
void uninit_device(webcam_t *);
void close_device(const webcam_t *);

int xioctl(int, int, void *);

void cam_hflip(int, const int);
void cam_vflip(int, const int);

#endif /* __WEBCAM_H */
