/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fader.h"


Fader_t *
Fader_new(const u_long size)
{
  Fader_t *fa = NULL;

  fa = xcalloc(1, sizeof(Fader_t));

  fa->target = 0;
  fa->delta = xcalloc(size, sizeof(long));
  fa->tmp = xcalloc(size, sizeof(u_long));
  fa->fading = 1;
  fa->max = 0;
  fa->faded = 0;
  fa->timer = b_timer_new();

  return fa;
}


void
Fader_delete(Fader_t *fa)
{
  xfree(fa->delta);
  xfree(fa->tmp);
  b_timer_delete(fa->timer);
  xfree(fa);
}


u_long
Fader_elapsed(const Fader_t *fa)
{
  return (u_long)(b_timer_elapsed(fa->timer) * MFACTOR);
}
