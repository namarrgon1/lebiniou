/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include <jack/jack.h>

u_long options = BEQ_THREAD;

/* JACK data */
static jack_port_t **input_ports;
static jack_client_t *client;
static char *source_names[2] = { NULL, NULL };
static const char **ports;

// default input size if not defined in the configuration file
#define INSIZE 1024
static uint16_t insize = INSIZE;
// buffer jack samples here before copying to input->data
static double *data[2];
// number of jack frames to be read to reach input->size
static uint8_t chunks;


static void
jack_shutdown(void *arg)
{
  Context_t *ctx = (Context_t *)arg;
  printf("[!] JACK: server shut down, exiting\n");
  ctx->running = 0;
}


static int
process(jack_nframes_t nframes, void *arg)
{
  int chn;
  uint32_t i;
  Context_t *ctx = (Context_t *)arg;
  jack_default_audio_sample_t *in;
  static uint8_t chunk = 0;
  static uint16_t idx = 0;

  if (!ctx->input->mute) {
    uint16_t idx2 = idx;

    for (chn = 0; chn < 2; chn++) {
      in = jack_port_get_buffer(input_ports[chn], nframes);
      if (NULL != in) {
        for (i = 0; i < nframes; i++, idx++) {
          data[chn][idx] = in[i];
        }
        if (chn == 0) {
          idx = idx2;
        }
      }
    }

    chunk++;
#ifdef XDEBUG
    printf("[i] JACK: chunk= %d\n", chunk);
#endif

    if (chunk == chunks) {
#ifdef XDEBUG
      printf("[i] JACK: setting input, idx= %d\n", idx);
#endif
      pthread_mutex_lock(&ctx->input->mutex);
      for (i = 0; i < ctx->input->size; i++) {
        ctx->input->data[A_LEFT][i]  = data[0][i];
        ctx->input->data[A_RIGHT][i] = data[1][i];
      }
      Input_set(ctx->input, A_STEREO);
      pthread_mutex_unlock(&ctx->input->mutex);
      idx = chunk = 0;
    }
  }

  return 0;
}


static void
setup_ports()
{
  int i;

  input_ports = xcalloc(2, sizeof(jack_port_t *));

  for (i = 0; i < 2; i++) {
    char name[64];

    sprintf(name, "input_%d", i);

    if ((input_ports[i] = jack_port_register(client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0)) == 0) {
      fprintf(stderr, "[!] JACK: cannot register input port \"%s\" !\n", name);
      jack_client_close(client);
      exit(1);
    } else {
      printf("[i] JACK: registered %s\n", name);
    }
  }

  ports = jack_get_ports(client, NULL, NULL, JackPortIsPhysical|JackPortIsOutput);
  if (NULL == ports) {
    xerror("JACK: no physical capture ports\n");
  }
}


int8_t
create(Context_t *ctx)
{
  int i;

  if ((client = jack_client_open(PACKAGE, JackNullOption, NULL)) == 0) {
    xerror("JACK server not running ?\n");
  }

  jack_set_process_callback(client, process, ctx);
  jack_on_shutdown(client, jack_shutdown, ctx);

  if (NULL == (source_names[0] = getenv("LEBINIOU_JACK_LEFT"))) {
    source_names[0] = "system:capture_1";
  }
  if (NULL == (source_names[1] = getenv("LEBINIOU_JACK_RIGHT"))) {
    source_names[1] = "system:capture_2";
  }

  printf("[i] JACK: left  capture from %s\n", source_names[0]);
  printf("[i] JACK: right capture from %s\n", source_names[1]);

  setup_ports();

  jack_nframes_t jack_size = jack_get_buffer_size(client);
  printf("[i] JACK: buffer size: %d\n", jack_size);
  if (jack_size >= insize) {
    chunks = 1;
    insize = jack_size;
  } else {
    chunks = insize / jack_size;
  }
  printf("[i] JACK: %d chunks to read\n", chunks);

  ctx->input = Input_new(insize);
  data[0] = xcalloc(insize, sizeof(double));
  data[1] = xcalloc(insize, sizeof(double));

  if (jack_activate(client)) {
    xerror("JACK: cannot activate client\n");
  }

  for (i = 0; i < 2; i++) {
    if (jack_connect(client, ports[i], jack_port_name(input_ports[i]))) {
      fprintf(stderr, "[!] JACK: can not connect input port %s to %s\n", jack_port_name(input_ports[i]), source_names[i]);
      jack_client_close(client);
      exit(1);
    } else {
      printf("[i] JACK: connected %s to %s\n", source_names[i], jack_port_name(input_ports[i]));
    }
  }
  jack_free(ports);

  return 1;
}


void
destroy(Context_t *ctx)
{
  jack_client_close(client);
  Input_delete(ctx->input);
  xfree(data[0]);
  xfree(data[1]);
}
