/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

u_long options = BEQ_THREAD;

#define FACT    0.40
#define DEVICE  "/dev/urandom"

static short  *urndbuff;
static int     urndfd;
static struct  timespec req;


int8_t
create(Context_t *ctx)
{
  req.tv_sec = 0;
  req.tv_nsec = 100000000;

  urndfd = open(DEVICE, O_RDONLY);
  if (urndfd == -1) {
    fprintf(stderr, "Unable to open `%s'\n", DEVICE);
    exit (1);
  }

  uint32_t insize = Context_get_input_size(ctx);
  urndbuff = xcalloc(insize * 2, sizeof(short));
  ctx->input = Input_new(insize);

  return 1;
}


void
destroy(Context_t *ctx)
{
  Input_delete(ctx->input);
  close(urndfd);
  xfree(urndbuff);
}


void *
jthread(void *args)
{
  Context_t *ctx = (Context_t *)args;

  while (ctx->running) {
    int n;

    n = read(urndfd, (void *)urndbuff, ctx->input->size * 2 * sizeof(short));

    if (!ctx->input->mute && (n != -1)) {
      int m, idx;

      pthread_mutex_lock(&ctx->input->mutex);
      for (m = 0, idx = 0; (m < n) && (idx < (int)ctx->input->size); idx++) {
        ctx->input->data[A_LEFT][idx] = FACT * (float)((float)urndbuff[m++] / (float)-SHRT_MIN);
        ctx->input->data[A_RIGHT][idx] = FACT * (float)((float)urndbuff[m++] / (float)-SHRT_MIN);
      }
      Input_set(ctx->input, A_STEREO);
      pthread_mutex_unlock(&ctx->input->mutex);
    }
    nanosleep(&req, NULL);
  }

  return NULL;
}
