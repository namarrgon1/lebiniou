/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

u_long options = BE_NONE;
char desc[] = "MP4 video encoder";

#define FFMPEG_CHECK         "ffmpeg -h >/dev/null 2>&1"
#define MP4_FFMPEG_CMD       "ffmpeg -loglevel quiet -re -framerate %d -vcodec ppm -f image2pipe -i pipe: -vcodec libx264 -pix_fmt yuv420p -r %d -vsync cfr %s"
#define MP4_FFMPEG_CMD_AUDIO "ffmpeg -loglevel quiet -re -framerate %d -vcodec ppm -f image2pipe -i pipe: -i %s -vcodec libx264 -pix_fmt yuv420p -r %d -vsync cfr %s"


#define DIRECTORY      "/videos/"

static FILE *mp4 = NULL;


static char *
video_filename()
{
  gchar *blah = NULL;
  const gchar *home_dir = NULL;

  home_dir = g_get_home_dir();
  blah = g_strdup_printf("%s/." PACKAGE_NAME DIRECTORY, home_dir);
  rmkdir(blah);
  g_free(blah);

  const char *sndfile = getenv("LEBINIOU_SNDFILE");
  if (NULL != sndfile) {
    char *c;
    sndfile = (NULL != (c = strrchr(sndfile, '/'))) ? ++c : sndfile;
    (NULL != (c = strrchr(sndfile, '.'))) && (*c = '\0'); /* spr0tch */
  }
  blah = g_strdup_printf("%s/." PACKAGE_NAME DIRECTORY "%s-%ld.mp4",
                         home_dir, (NULL != sndfile) ? sndfile : PACKAGE_NAME, (long)unix_timestamp());
  VERBOSE(printf("[i] %s: recording video to %s\n", __FILE__, blah));

  return blah;
}


static void
open_mp4(Context_t *ctx)
{
  const char *sndfile = getenv("LEBINIOU_SNDFILE");
  /* strdup() is needed because LEBINIOU_SNDFILE gets modified in video_filename() */
  char *audio = (NULL != sndfile) ? strdup(sndfile) : NULL;
  char *out = video_filename();
  char *cmd;

  if (NULL != audio) {
    cmd = g_strdup_printf(MP4_FFMPEG_CMD_AUDIO, ctx->max_fps, audio, ctx->max_fps, out);
    free(audio);
  } else {
    cmd = g_strdup_printf(MP4_FFMPEG_CMD, ctx->max_fps, ctx->max_fps, out);
  }

  if (NULL == (mp4 = popen(cmd, "w"))) {
    xperror("popen");
  } else {
    VERBOSE(printf("[i] %s: cmd= %s\n", __FILE__, cmd));
  }
  g_free(cmd);
  xfree(out);
}


int8_t
create(Context_t *ctx)
{
  if (check_command(FFMPEG_CHECK) == -1) {
    printf("[!] %s: ffmpeg binary not found, can't create video\n", __FILE__);
  } else {
    open_mp4(ctx);
  }

  return 1;
}


void
destroy(Context_t *ctx)
{
  if (NULL != mp4)
    if (-1 == pclose(mp4)) {
      xperror("pclose");
    }
}


void
run(Context_t *ctx)
{
  if (NULL != mp4) {
    u_char *data;
    char buff[MAXLEN+1];
    size_t res;

    /* get picture */
    data = export_RGB_active_buffer(ctx, 1);

    memset(&buff, '\0', MAXLEN+1);
    g_snprintf(buff, MAXLEN, "P6  %d %d 255\n", WIDTH, HEIGHT);

    /* PPM header */
    res = fwrite((const void *)&buff, sizeof(char), strlen(buff), mp4);
    if (res != strlen(buff)) {
      fprintf(stderr, "[!] %s:write_header: short write (%d of %d)\n", __FILE__, (int)res, (int)strlen(buff));
      exit(1);
    }

    /* PPM data */
    res = fwrite((const void *)data, sizeof(Pixel_t), RGB_BUFFSIZE, mp4);
    xfree(data);
    if (res != RGB_BUFFSIZE) {
      fprintf(stderr, "[!] %s:write_image: short write (%d of %li)\n", __FILE__, (int)res, RGB_BUFFSIZE);
      exit(1);
    }

    fflush(mp4);
  }
}
