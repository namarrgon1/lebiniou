/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL2/SDL.h>
#include "biniou.h"
#include "osd.h"
#include "src/defaults.h"
#include "events.h"

u_long options = BE_NONE;

#define NO_MOUSE_CURSOR

static SDL_Window *window = NULL;
static SDL_Surface *screen = NULL;
static SDL_DisplayMode current; // current screen resolution
static pthread_t thread;


static void
create_window(const Uint32 flags)
{
  char *icon_file;
  SDL_Surface *icon = NULL;
  Uint32 colorkey;
  char *window_title;

  window_title = g_strdup_printf("Le Biniou (%dx%d)", WIDTH, HEIGHT);

  int x0 = WIDTH_ORIGIN == INT32_MIN ? current.w - WIDTH : WIDTH_ORIGIN;
  int y0 = HEIGHT_ORIGIN == INT32_MIN ? 0 : HEIGHT_ORIGIN;
  window = SDL_CreateWindow(window_title, x0, y0, WIDTH, HEIGHT, flags);
  screen = SDL_GetWindowSurface(window);
  g_free(window_title);
  if (NULL == window) {
    xerror("Couldn't set %dx%d video mode: %s\n", WIDTH, HEIGHT, SDL_GetError());
  }

  icon_file = g_strdup_printf("%s/lebiniou.bmp", DEFAULT_DATADIR);
  icon = SDL_LoadBMP(icon_file);
  g_free(icon_file);
  colorkey = SDL_MapRGB(icon->format, 0, 0, 0);
  SDL_SetColorKey(icon, SDL_TRUE, colorkey);
  SDL_SetWindowIcon(window, icon);
  SDL_FreeSurface(icon);
}


static inline void
SDL_refresh_32bpp(Context_t *ctx, SDL_Surface *sc)
{
  const RGBA_t *src;

  Buffer8_flip_v(active_buffer(ctx));
  src = export_RGBA_active_buffer(ctx);
  Buffer8_flip_v(active_buffer(ctx));

  const int depth = 32, pitch = 4 * WIDTH;
  const Uint32 pixel_format = SDL_PIXELFORMAT_RGBA32;

  SDL_Surface *surf = SDL_CreateRGBSurfaceWithFormatFrom((void *)src, WIDTH, HEIGHT,
                      depth, pitch, pixel_format);
  assert(NULL != surf);
  if (SDL_BlitScaled(surf, NULL, screen, NULL) < 0) {
    xerror("SDL_BlitScaled failed\n");
  }
  SDL_FreeSurface(surf);
}


static void
SDL_get_event(Context_t *ctx)
{
  // TODO middle = change color, right = erase (3x3)

  SDL_Event evt;
  memset(&evt, 0, sizeof(SDL_Event));
  SDL_Window *changed = NULL;

  while (SDL_PollEvent(&evt) != 0) {
    BKey_t key;

    switch (evt.type) {
      case SDL_KEYDOWN:
        key.val = evt.key.keysym.sym;
        key.mod = evt.key.keysym.mod;

        on_key(ctx, &key);
        break;

      case SDL_QUIT:
        Context_send_event(ctx, BT_CONTEXT, BC_QUIT, BA_NONE);
        break;

      case SDL_MOUSEMOTION:
        switch (evt.motion.state) {
          case SDL_BUTTON_LEFT:
            ctx->params3d.xe = evt.motion.x;
            ctx->params3d.ye = evt.motion.y;
            Params3d_rotate(&ctx->params3d);
            break;

          case SDL_BUTTON_RIGHT + SDL_BUTTON_LEFT: /* <- WTF ? */
            // printf("right button motion @ %d %d\n",  evt.motion.x, evt.motion.y);
            set_pixel_nc(active_buffer(ctx), evt.motion.x, MAXY-evt.motion.y, 255);
            break;

          default:
            break;
        }
        break;

      case SDL_MOUSEWHEEL:
        if (evt.wheel.y > 0) { // scroll up
          ctx->params3d.scale_factor /= SCALE_FACTOR_MULT;
        } else if (evt.wheel.y < 0) { // scroll down
          if (ctx->params3d.scale_factor > SCALE_FACTOR_MIN) {
            ctx->params3d.scale_factor *= SCALE_FACTOR_MULT;
          }
        }
        printf("[i] 3D scale factor: %.2f\n", ctx->params3d.scale_factor);
        break;

      case SDL_MOUSEBUTTONDOWN:
        /* printf("type= %d, button= %d\n", evt.button.type, evt.button.button); */
        switch (evt.button.button) {
          case SDL_BUTTON_LEFT:
            ctx->params3d.xs = evt.motion.x;
            ctx->params3d.ys = evt.motion.y;
            break;

          case SDL_BUTTON_RIGHT:
            // printf("button down @ %d %d\n", evt.motion.x, evt.motion.y);
            set_pixel_nc(active_buffer(ctx), evt.motion.x, MAXY-evt.motion.y, 255);
            break;

          default:
            break;
        }
        break;

      case SDL_WINDOWEVENT:
        switch (evt.window.event) {
          case SDL_WINDOWEVENT_RESIZED:
#ifdef DEBUG
            SDL_Log("Window %d resized to %dx%d",
                    evt.window.windowID, evt.window.data1,
                    evt.window.data2);
#endif
            changed = SDL_GetWindowFromID(evt.window.windowID);
            if (changed == window) {
              screen = SDL_GetWindowSurface(changed);
            }
            break;

          default:
            break;
        }
        break;

      default:
        break;
    }
  }
}


void
run(Context_t *ctx)
{
  SDL_get_event(ctx);
  SDL_refresh_32bpp(ctx, screen);

  if (SDL_UpdateWindowSurface(window) < 0) {
    SDL_Log("SDL_UpdateWindowSurface failed: %s", SDL_GetError());
    exit(1);
  }
}


int8_t
create(Context_t *ctx)
{
  Uint32 flags = 0;
  Uint32 subsystems;
  int ret;

  /* Initialize SDL */
  subsystems = SDL_WasInit(SDL_INIT_VIDEO);
  if (subsystems == 0) {
    ret = SDL_Init(SDL_INIT_VIDEO);
    if (ret == -1) {
      xerror("Couldn't initialize SDL: %s\n", SDL_GetError());
    }
  }

  /* We assume running on the first screen/display */
  const int screen = 0;
  if (SDL_GetCurrentDisplayMode(screen, &current) == 0) {
#ifdef DEBUG
    printf("[i] SDL Screen resolution: %dx%d\n", current.w, current.h);
#endif
  } else {
    xerror("SDL_GetCurrentDisplayMode failed\n");
  }

  if (ctx->window_decorations == 0) {
    flags |= SDL_WINDOW_BORDERLESS;
  }

#ifndef FIXED
  flags |= SDL_WINDOW_RESIZABLE;
#endif
  create_window(flags);

#ifdef NO_MOUSE_CURSOR
  SDL_ShowCursor(SDL_DISABLE);
#endif

  /* Fill windows on boot */
  /* Initialize main window with target picture */
  /* No colormaps are loaded yet, so we convert to grayscale */
  const int depth = 24, pitch = 3 * WIDTH;
  const Uint32 pixel_format = SDL_PIXELFORMAT_RGB24;
  Pixel_t *target = xmalloc(3 * BUFFSIZE * sizeof(Pixel_t));
  Buffer8_t *pic = Buffer8_new();
  Buffer8_copy(ctx->target_pic->buff, pic);
  Buffer8_flip_v(pic);
  const Pixel_t *src = pic->buffer;
  Pixel_t *dst = target;
  uint32_t i = 0;

  for (i = 0; i < BUFFSIZE; i++, src++, dst += 3) {
    dst[0] = dst[1] = dst[2] = *src;
  }
  SDL_Surface *surf = SDL_CreateRGBSurfaceWithFormatFrom((void *)target, WIDTH, HEIGHT,
                      depth, pitch, pixel_format);
  Buffer8_delete(pic);
  xfree(target);
  assert(NULL != surf);
  if (SDL_BlitScaled(surf, NULL, SDL_GetWindowSurface(window), NULL) < 0) {
    xerror("SDL_BlitScaled failed\n");
  }
  SDL_FreeSurface(surf);
  if (SDL_UpdateWindowSurface(window) < 0) {
    xerror("SDL_UpdateWindowSurface failed: %s\n", SDL_GetError());
  }

  pthread_create(&thread, NULL, osd_thread, (void *)ctx);

  return 1;
}


void
destroy(Context_t *ctx)
{
  pthread_join(thread, NULL);
  SDL_FreeSurface(screen);
  SDL_DestroyWindow(window);
  SDL_Quit();
}


void
fullscreen(const int fs)
{
  if (fs) {
    printf("[S] Set full-screen\n");
  } else {
    printf("[S] Unset full-screen\n");
  }
  SDL_SetWindowFullscreen(window, fs ? SDL_WINDOW_FULLSCREEN : 0);
}


void
switch_cursor()
{
  SDL_ShowCursor(SDL_ShowCursor(SDL_QUERY) ? SDL_DISABLE : SDL_ENABLE);
}
