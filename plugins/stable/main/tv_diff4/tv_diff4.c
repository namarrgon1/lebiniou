/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/* Plugin idea: show only pixels that differ more than MIN_DIFFERENCE
   from reference picture. For showing pixels, we use colorvalue of
   difference between cam and active buffer */


#include "context.h"

#define MIN_DIFFERENCE 40 /* how much color value has to differ from initial
                             to be shown. Lower values show target better,
                             but also show more flickering */

uint32_t version = 0;
u_long options = BE_GFX|BE_LENS|BEQ_MUTE_CAM|BEQ_NORANDOM;
char desc[] = "Show cam pic which differs";
char dname[] = "TV diff 4";

u_long mode = OVERLAY;


void
run(Context_t *ctx)
{
  Pixel_t *src1, *start, *src2, *src3, *dst;

  dst = start = passive_buffer(ctx)->buffer;

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  src1 = ctx->cam_save[ctx->cam][0]->buffer;
  src2 = ctx->cam_ref0[ctx->cam]->buffer;
  src3 = active_buffer(ctx)->buffer;
  for (; dst < start + BUFFSIZE * sizeof(Pixel_t); src1++, src2++, src3++, dst++) {
    if (((*src1 - *src2) > MIN_DIFFERENCE) ||
        ((*src2 - *src1) > MIN_DIFFERENCE) ) {
      *dst = *src1 < *src3 ? (*src3 - *src1) : (*src1 - *src3);
    } else {
      *dst = 0;
    }
  }
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);
}
