/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_MIRROR|BEQ_VER;
char dname[] = "Mirror left";
char desc[] = "Mirror effect";


void
run(Context_t *ctx)
{
  int i, j;
  Buffer8_t *wrk = active_buffer(ctx);

  for (j = 0; j < HEIGHT; j++)
    for (i = 0; i < HWIDTH; i++) {
      set_pixel_nc(wrk, MAXX - i, j, get_pixel_nc(wrk, i, j));
    }
  Buffer8_copy(active_buffer(ctx), passive_buffer(ctx));
}
