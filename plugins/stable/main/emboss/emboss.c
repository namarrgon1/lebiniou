/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_LENS;
char desc[] = "Emboss filter";
char dname[] = "Emboss";

void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  const Pixel_t* nw = src->buffer;
  const Pixel_t* se = src->buffer + 2 * WIDTH + 2;

  Pixel_t *d;

  for (d = dst->buffer + WIDTH + 1; d < dst->buffer + (BUFFSIZE - WIDTH - 1); ) {
    *d++ = ((*nw++ - *se++) / 2) + 128;
  }
}
