/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"

#define PATTERNSIZE 7879 /* prime number, not modulo of any display size */
#define COUNT_MAX  (PATTERNSIZE/6)
/* parameters for defining volume-colorcount curve */
#define CURVE_VOL_MIN  0.01 /* minimum volume on which we work */
#define CURVE_VOL_STEP 0.02 /* volume increment to step up on count */
#define CURVE_VOL_MULT 1.26 /* multiplier on count on one step */

static u_char pattern[PATTERNSIZE];
uint32_t version = 0;
u_long options = BE_GFX|BE_SFX2D|BEQ_IMAGE;
u_long mode = NORMAL;
char desc[] = "Show image colors on beat";
char dname[] = "Image colrot beat 3";

void
on_switch_on(Context_t *ctx)
{
  int i;

  for (i = 0; i < PATTERNSIZE; i++) {
    pattern[i] = 0;
  }
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  Buffer8_copy(active_buffer(ctx), dst);

  if (ctx->input->on_beat) {
    double peak;
    int count = 0;
    int done = 0, looped = 0;
    int i, j;
    u_long k;

    for (peak = ctx->input->curpeak; peak > CURVE_VOL_MIN; peak -= CURVE_VOL_STEP) {
      count = count * CURVE_VOL_MULT;
      count++;
    }

    if (count > COUNT_MAX) {
      count = COUNT_MAX;
    }

    for (i = 0; i < count; i++) {
      j = b_rand_int_range(0, PATTERNSIZE-1);
      done = looped = 0;
      while (done == 0) {
        if (j < PATTERNSIZE && pattern[j] == 0) { /* free */
          done = 1;
          pattern[j] = 1;
        } else { /* not free slot, go to next */
          j++;
          if (j > PATTERNSIZE) {
            if (looped == 1) { /* we have whole pattern full */
              break;
            }
            looped = 1;
            j = 0;
          }
        }
      }

      if ((done == 0) && (looped == 1)) { /* pattern full */
        break;
      }
    }

    i = 0;
    for (k = 0; k < BUFFSIZE; k++) {
      if (pattern[i] == 1) {
        dst->buffer[k] = ctx->imgf->cur->buff->buffer[k];
      }

      i++;
      if (i >= PATTERNSIZE) {
        i = 0;
      }
    }

    if ((done == 0) && (looped == 1)) { /* pattern was full, clear it */
      for (i = 0; i < PATTERNSIZE; i++) {
        pattern[i] = 0;
      }
    } else {
      for (i = 0; i < PATTERNSIZE; i++) {
        if (pattern[i] == 1) {
          pattern[i] = 2;
        }
      }
    }
  }
}
