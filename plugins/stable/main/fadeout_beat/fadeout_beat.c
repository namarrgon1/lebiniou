/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2014-2020 Frantz Balinski
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * idea :
 *   decrease pixel color values on beat. Higher volume, bigger decrement
 */

#include "context.h"

#define MAXCOLORCOUNT  21          /* biggest decrement */
#define CURVE_VOL_MIN  0.04        /* minimum volume on which we work */
#define CURVE_VOL_STEP 0.10        /* volume increment to step up on count */
#define CURVE_VOL_MULT (9.0 / 7.0) /* multiplier on count on one step */

uint32_t version = 0;
u_long options = BE_BLUR|BE_SFX2D|BEQ_NORANDOM;
char desc[] = "Color fade-out on beat";
char dname[] = "Fadeout on beat";


void
run(Context_t *ctx)
{
  if (ctx->input->on_beat) {
    int colorcount = 0;
    const Pixel_t *src = active_buffer(ctx)->buffer;
    Pixel_t *dst = passive_buffer(ctx)->buffer;

    Buffer8_copy(active_buffer(ctx), passive_buffer(ctx));

    /* calculate number of colors to fade */
    for (double peak = ctx->input->curpeak; peak > CURVE_VOL_MIN; peak -= CURVE_VOL_STEP) {
      colorcount = colorcount * CURVE_VOL_MULT;
      colorcount++;
    }

    if (colorcount > MAXCOLORCOUNT) {
      colorcount = MAXCOLORCOUNT;
    }

    for (uint32_t i = 0; i < BUFFSIZE; i++) {
      Pixel_t col = *src++;

      if (col > colorcount) {
        col -= colorcount;
      } else {
        col = PIXEL_MINVAL;
      }
      *dst++ = col;
    }
  }
}
