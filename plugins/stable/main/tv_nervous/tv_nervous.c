/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * EffecTV - Realtime Digital Video Effector
 * Copyright (C) 2001-2006 FUKUCHI Kentaro
 *
 * nervousTV - The name says it all...
 * Copyright (C) 2002 TANNENBAUM Edo
 *
 * 2002/2/9
 *   Original code copied same frame twice, and did not use memcpy().
 *   I modifed those point.
 *   -Kentaro Fukuchi
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_IMAGE|BEQ_MUTE_CAM;
char desc[] = "NervousTV plugin from the EffecTV project";
char dname[] = "TV nervous";

u_long mode = OVERLAY;

#define PLANES 8

static Buffer8_t *nervous = NULL;


int8_t
create(Context_t *ctx)
{
  uint32_t i;

  assert(PLANES <= CAM_SAVE);
  nervous = Buffer8_new();
  for (i = 0; i < BUFFSIZE; i++) {
    nervous->buffer[i] = b_rand_int_range(0, PLANES-1);
  }

  return 1;
}


void
destroy(Context_t *ctx)
{
  Buffer8_delete(nervous);
}


void
run(Context_t *ctx)
{
  static uint32_t r = 0;

  Buffer8_t *src = NULL;
  Buffer8_t *dst = passive_buffer(ctx);

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  src = ctx->cam_save[ctx->cam][nervous->buffer[r]];
  Buffer8_copy(src, dst);
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);

  if (++r == BUFFSIZE) {
    r = 0;
  }
}
