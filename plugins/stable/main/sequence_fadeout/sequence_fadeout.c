/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"

/*
 * "Fade previous sequence" splash effect
 *
 * on_init: Store a copy of last sequence / current buffer
 *
 * Then on each run, mix the buffer at random with current,
 *   and decrease pixels from the copy.
 *
 * This splash will run for 64 frames.
 */

uint32_t version = 0;
u_long options = BEQ_SPLASH|BEQ_FIRST;
char desc[] = "Fade previous sequence";
char dname[] = "Sequence fadeout";


static Pixel_t splashing = 64;
static Buffer8_t *last = NULL;


void
on_switch_on(Context_t *ctx)
{
  splashing = 64;
  Buffer8_copy(active_buffer(ctx), last);
}


int8_t
create(Context_t *ctx)
{
  last = Buffer8_new();

  return 1;
}


void
destroy(Context_t *ctx)
{
  Buffer8_delete(last);
}


static void
splash2()
{
  Pixel_t *p = last->buffer;

  for (uint32_t i = 0; i < BUFFSIZE; i++, p++) {
    if (*p >= 20) {
      *p *= 0.6;
    } else {
      if (*p >= 1) {
        (*p)--;
      }
    }
  }
  splashing--;
}


void
run(Context_t *ctx)
{
  if (splashing) {
    splash2();
    Buffer8_t *buffs[2] = { active_buffer(ctx), last };
    Context_mix_buffers(ctx, buffs);
  }
  Buffer8_copy(active_buffer(ctx), passive_buffer(ctx));
}
