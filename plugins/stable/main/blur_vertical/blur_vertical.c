/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "image_filter.h"
#include "parameters.h"
#include "blur.h"


uint32_t version = 0;
u_long options = BE_BLUR|BEQ_VER;
char dname[] = "Blur vertical";
char desc[] = "Vertical blur";


void
on_switch_on(Context_t *ctx)
{
  border_mode = BM_TOROIDAL;
}


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  image_filter_average(dst, src, FT_VBLUR_3x3, border_mode, 0, NULL);
}
