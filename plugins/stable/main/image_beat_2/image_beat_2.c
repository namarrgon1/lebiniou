/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"

#define MAXCOLOR 255
#define MAXCOLOCOUNT 40
/* parameters for defining volume-colorcount curve */
#define CURVE_VOL_MIN  0.04 /* minimum volume on which we work */
#define CURVE_VOL_STEP 0.08 /* volume increment to step up on count */
#define CURVE_VOL_MULT 1.35 /* multiplier on count on one step */

uint32_t version = 0;
u_long options = BE_GFX|BE_SFX2D|BEQ_IMAGE;
u_long mode = NORMAL;
char desc[] = "Show image colors on beat";
char dname[] = "Image color beat 2";
static u_char colorsel = 0;


void
on_switch_on(Context_t *ctx)
{
  colorsel = 0;
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  Buffer8_copy(active_buffer(ctx), dst);

  if (ctx->input->on_beat) {
    int colorcount = 0;
    double peak;
    u_long k;

    for (peak = ctx->input->curpeak; peak > CURVE_VOL_MIN; peak -= CURVE_VOL_STEP) {
      colorcount = colorcount * CURVE_VOL_MULT;
      colorcount++;
    }

    if (colorcount > MAXCOLOCOUNT) {
      colorcount = MAXCOLOCOUNT;
    }

    for (k = 0; k < BUFFSIZE; k++) {
      Pixel_t col = ctx->imgf->cur->buff->buffer[k];
      if (dst->buffer[k] > col) {
        int step = (dst->buffer[k] - col) > colorcount ? colorcount : dst->buffer[k] - col;
        dst->buffer[k] -= ( step > 3 ? (step / 3) : 1 );
      }

      if (col > (MAXCOLOR - colorsel - colorcount)) {
        if (dst->buffer[k] < col) {
          dst->buffer[k] = (col > (dst->buffer[k] + colorcount)) ?
                           (dst->buffer[k] + colorcount) : col;
        }
      }
    }

    if (colorsel + colorcount > MAXCOLOR) {
      colorsel = 0;
    } else {
      colorsel += colorcount;
    }
  }
}
