/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation.h"
#include "parameters.h"


uint32_t version = 0;
u_long options = BE_DISPLACE;
char dname[] = "Spiral";
char desc[] = "Spiral effect";

/* Pour garder le centre de la translation bien visible dans le screen */
#define SHIFT_X (WIDTH / 10)
#define SHIFT_Y (HEIGHT / 10)


static Translation_t *t_spiral = NULL;

static int cx, cy;
const float q = M_PI / 2;
const float p = 45.0 / 180.0 * M_PI;

static double p_factor = 0;

json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_double(params, BPP_P_FACTOR, p_factor, -0.01, 0.01);

  return params;
}


static Map_t
cth_spiral(const short in_x, const short in_y)
{
  const u_short i = in_x, j = in_y;
  int dx,dy;
  Map_t m;

  if ((j == MINY) || (j == MAXY)) {
    dx = (float)(cx - i) * 0.75;
    dy = cy - j;
  } else {
    int dist;
    float ang;

    dist = sqrt((i-cx)*(i-cx) + (j-cy)*(j-cy));

    if (i==cx) {
      if (j>cx) {
        ang = q;
      } else {
        ang = -q;
      }
    } else {
      ang = atan((float)(j-cy)/(i-cx));
    }

    if (i<cx) {
      ang += M_PI;
    }

    dx = ceil(-sin(ang - p * p_factor) * dist / 10.0);
    dy = ceil( cos(ang - p * p_factor) * dist / 10.0);

    if ((i == MINX) || (i == MAXX)) {
      dx = cx - i;
      dy = (float)(cy - j) * 0.75;
    }
  }

  m.map_x = abs((i+dx) % WIDTH);
  m.map_y = abs((j+dy) % HEIGHT);

  return m;
}


static void
init_params()
{
  cx = b_rand_int_range(SHIFT_X, (MAXX - SHIFT_X));
  cy = b_rand_int_range(SHIFT_Y, (MAXY - SHIFT_Y));
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  int reload = 0;
  reload |= plugin_parameter_parse_double_range(in_parameters, BPP_P_FACTOR, &p_factor, 0, 10);

  if (reload) {
    Translation_delete(t_spiral);
    t_spiral = Translation_new(&cth_spiral, &init_params);
  }
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  p_factor = 1;
  Translation_batch_init(t_spiral);
}


int8_t
create(Context_t *ctx)
{
  t_spiral = Translation_new(&cth_spiral, &init_params);

  return 1;
}


void
destroy(Context_t *ctx)
{
  Translation_delete(t_spiral);
}


void
run(Context_t *ctx)
{
  Translation_run(t_spiral, ctx);
}
