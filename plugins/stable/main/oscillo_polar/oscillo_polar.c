/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "oscillo.h"
#include "parameters.h"


uint32_t version = 0;
u_long options = BE_SFX2D;
u_long mode = OVERLAY;
char desc[] = "Draw an oscillo in a radar way";
char dname[] = "Oscillo polar";


static Porteuse_t *P = NULL;

static Point2d_t last_polar;
static float     polar_theta, polar_inc_theta;
static u_short   polar_radius;
static float     polar_length;
static int       polaroscillo_connect = 1;

static double volume_scale = 0;

json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, -0.01, 0.01);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale, 0, 100);
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


void
init(const Input_t *input)
{
  uint32_t i;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  P->origin = last_polar;

  t.v_j_factor = MAXY/4 * volume_scale;

  polar_inc_theta = 2. * M_PI * polar_length / (float)(input->size);

  for (i = 0; i < P->size; i++) {
    Point2d_t next, dsignal;

    next.x = CENTERX + polar_radius * cos(polar_theta);
    next.y = CENTERY + polar_radius * sin(polar_theta);

    dsignal = p2d_sub(&next, &last_polar);
    t.v_i = dsignal;
    last_polar = p2d_add(&last_polar, &t.v_i);

    P->trans[i] = t;
    polar_theta += polar_inc_theta;
  }
  polar_theta -= 2.0 * M_PI * (int)(polar_theta / (2.0 * M_PI));

  Porteuse_init_alpha(P);
}


int8_t
create(Context_t *ctx)
{
  P = Porteuse_new(ctx->input->size, A_MONO);

  polar_theta = 0.0;
  polar_inc_theta = 0.01;
  polar_length = 0.666;
  polar_radius = HMAXY*2.0/3.0;

  last_polar.x = CENTERX + polar_radius * cos (polar_theta);
  last_polar.y = CENTERY + polar_radius * sin (polar_theta);

  polar_theta += polar_inc_theta;

  init(ctx->input);

  return 1;
}


void
destroy(Context_t *ctx)
{
  if (NULL != P) {
    Porteuse_delete(P);
  }
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 0.85;
}


void
run(Context_t *ctx)
{
  Buffer8_clear(passive_buffer(ctx));
  Porteuse_draw(P, ctx, polaroscillo_connect);
  init(ctx->input);
}
