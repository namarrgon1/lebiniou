/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_WARP|BE_LENS;
char desc[] = "Nested spirals filter";
char dname[] = "Spirals nested";

static u_long *map;

#define DST(x,y) map[(y*WIDTH)+x]
#define SRC(x,y) (reversed ? ((MAXY-y)*WIDTH+(MAXX-x)) : ((y*WIDTH)+x))


static void
init(const u_short startx, const u_short starty,
     char xinc, char yinc,
     short distx, short disty, u_char delta, const int reversed)
{
  u_short donex, doney;
  short x = startx, y = starty;

  while ((distx >= 0) || (disty >= 0)) {
    /* horizontal line: S----->E */
    for (donex = 0; donex < distx; donex++) {
      /*      if (xinc == 1) {
      u_short dx;
      for (dx = 0; dx < SWIDTH; dx++)
      */
      DST(x,y) = SRC(x,y);
      x += xinc;
    }
    xinc = -xinc;
    distx -= delta;

    /* vertical line: S */
    /*                | */
    /*                v */
    /*                E */
    for (doney = 0; doney < disty; doney++) {
      DST(x,y) = SRC(x,y);
      y += yinc;
    }
    yinc = -yinc;
    disty -= delta;
  }
}


int8_t
create(Context_t *ctx)
{
  map = xcalloc(BUFFSIZE, sizeof(u_long));

  init(0, 0, +1, +1, MAXSCREEN-2, MINSCREEN-2, 2, 0);
  init(MAXX, MAXY, -1, -1, MAXSCREEN-2, MINSCREEN-2, 2, 1);

  return 1;
}


void
destroy(Context_t *ctx)
{
  xfree(map);
}


void
run(Context_t *ctx)
{
  u_long k;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  for (k = 0; k < BUFFSIZE; k++) {
    dst->buffer[k] = src->buffer[map[k]];
  }
}
