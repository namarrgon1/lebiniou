/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"


#define SQUARE_SIZE 40
#define SQUARES_PER_TURN_DEFAULT  0
#define SQUARES_PER_TURN_BEAT     1
#define SQUARES_PER_TURN_POWER_COEFF 50
#define SQUARES_MAX 70

uint32_t version = 0;
u_long options = BE_GFX|BE_SFX2D|BEQ_IMAGE;
u_long mode = NORMAL;
char desc[] = "Random image squares in beat";
char dname[] = "Image squares beat";


void
run(Context_t *ctx)
{
  int s;
  int maxsq;

  if (ctx->input->on_beat) {
    maxsq = SQUARES_PER_TURN_BEAT +
            SQUARES_PER_TURN_POWER_COEFF * ctx->input->peakpower;
  } else {
    maxsq = SQUARES_PER_TURN_DEFAULT;
  }

  if (maxsq > SQUARES_MAX) {
    maxsq = SQUARES_MAX;
  }

  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_copy(active_buffer(ctx), dst);

  for (s = 0; s < maxsq; s++) {
    int i, j;

    u_short sx = b_rand_int_range(0, MAXX-SQUARE_SIZE);
    u_short sy = b_rand_int_range(0, MAXY-SQUARE_SIZE);

    for (j = 0; j < SQUARE_SIZE; j++) {
      for (i = 0; i < SQUARE_SIZE; i++) {
        set_pixel_nc(dst, sx+i, sy+j,
                     get_pixel_nc(ctx->imgf->cur->buff, sx+i, sy+j));
      }
    }
  }
}
