/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * EffecTV - Realtime Digital Video Effector
 * Copyright (C) 2001-2006 FUKUCHI Kentaro
 *
 * QuarkTV - motion disolver.
 * Copyright (C) 2001-2002 FUKUCHI Kentaro
 *
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_WEBCAM|BEQ_MUTE_CAM;
char desc[] = "QuarkTV plugin from the EffecTV project";
char dname[] = "TV quark";

u_long mode = OVERLAY;

#define MIN_FRAMES 11

static Buffer8_t *qtv = NULL;


int8_t
create(Context_t *ctx)
{
  uint32_t i;

  assert(MIN_FRAMES <= CAM_SAVE);
  qtv = Buffer8_new();
  for (i = 0; i < BUFFSIZE; i++) {
    qtv->buffer[i] = b_rand_int_range(0, MIN_FRAMES-1);
  }

  return 1;
}


void
destroy(Context_t *ctx)
{
  if (NULL != qtv) {
    Buffer8_delete(qtv);
  }
}


void
run(Context_t *ctx)
{
  uint64_t i;
  Pixel_t *dst;

  dst = passive_buffer(ctx)->buffer;

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  for (i = 0; i < BUFFSIZE; i++, dst++) {
    const Pixel_t rnd = qtv->buffer[i];
    *dst = ctx->cam_save[ctx->cam][rnd]->buffer[i];
  }
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);
}


/* Original code */
#if 0
static int draw(RGB32 *src, RGB32 *dest)
{
  int i;
  int cf;

  memcpy(planetable[plane], src, video_area * PIXEL_SIZE);

  for (i=0; i<video_area; i++) {
    cf = (inline_fastrand()>>24)&(PLANES-1);
    dest[i] = (planetable[cf])[i];
    /* The reason why I use high order 8 bits is written in utils.c
    (or, 'man rand') */
  }

  plane--;
  if (plane<0) {
    plane = PLANES - 1;
  }

  return 0;
}
#endif
