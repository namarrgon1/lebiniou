/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2014-2020 Frantz Balinski
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * idea:
 *   decrement every pixel color value by one
 */

#include "context.h"

uint32_t version = 0;
u_long options = BE_BLUR|BEQ_NORANDOM;
char desc[] = "Slow color fade-out effect";
char dname[] = "Fadeout slow";


void
run(Context_t *ctx)
{
  const Pixel_t *src = active_buffer(ctx)->buffer;
  Pixel_t *dst = passive_buffer(ctx)->buffer;

  for (uint32_t i = 0; i < BUFFSIZE; i++) {
    Pixel_t col = *src++;
    if (col > PIXEL_MINVAL) {
      col--;
    } else {
      col = PIXEL_MINVAL;
    }
    *dst++ = col;
  }
}
