/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_SFX2D|BEQ_NORANDOM;
u_long mode = OVERLAY;
char desc[] = "Test beat detection";
char dname[] = "Test beat detection";

void
run(Context_t *ctx)
{
  Buffer8_t *src = active_buffer(ctx);
  Pixel_t col = PIXEL_MINVAL;

  memmove((void *)src->buffer, (const void *)(src->buffer + sizeof(Pixel_t)), BUFFSIZE - 1);
  pthread_mutex_lock(&ctx->input->mutex);
  if (ctx->input->on_beat) {
    col = PIXEL_MAXVAL;
  }
  pthread_mutex_unlock(&ctx->input->mutex);
  v_line_nc(src, MAXX, MINY, MAXY, col);
  v_line_nc(src, MINX, MINY, MAXY, PIXEL_MINVAL);
}
