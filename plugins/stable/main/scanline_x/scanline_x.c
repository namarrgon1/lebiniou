/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_IMAGE;
char dname[] = "Scanline X";
char desc[] = "Horizontal scanline";


#define MINPCT 0.01
#define DPCT   0.002

/* conf */
typedef struct {
  float size;          /* line is %of screen WIDTH */
  char dir;            /* -1 or +1 */
  u_char ping_pong;    /* bounce */
} xscanline_conf;

static xscanline_conf xscanline_cf;


int8_t
create(Context_t *ctx)
{
  xscanline_cf.size = MINPCT;
  xscanline_cf.dir = 1;
  xscanline_cf.ping_pong = 0;

  return 1;
}


void
on_switch_on(Context_t *ctx)
{
  xscanline_cf.dir = (b_rand_boolean()) ? -1 : 1;
  xscanline_cf.ping_pong = b_rand_boolean();
}


static void
x_scanline(Context_t *ctx, float amount)
{
  u_short s, size = amount * WIDTH; /* number of columns */
  u_short j;
  static int x_line = 0;
  Buffer8_t *dst = NULL;
  ImageFader_t *imgf = ctx->imgf;
  Buffer8_t *src = imgf->cur->buff;

  swap_buffers(ctx);
  dst = passive_buffer(ctx);

  for (s = 0; s < size; s++) {
    for (j = 0; j <= MAXY; j++) {
      set_pixel_nc(dst, x_line, j, get_pixel_nc(src, x_line, j));
    }

    x_line += xscanline_cf.dir;
    if (x_line > MAXX) {
      if (xscanline_cf.ping_pong) {
        x_line = MAXX;
        xscanline_cf.dir = -xscanline_cf.dir;
      } else {
        x_line = 0;
      }
    } else if (x_line < 0) {
      if (xscanline_cf.ping_pong) {
        x_line = 0;
        xscanline_cf.dir = -xscanline_cf.dir;
      } else {
        x_line = MAXX;
      }
    }
  }
}


void
run(Context_t *ctx)
{
  x_scanline(ctx, xscanline_cf.size);
}
