/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_BLUR|BEQ_NORANDOM;
char desc[] = "Horizontal averaging + color cycling";
char dname[] = "Blur h + colrot";


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer (ctx);

  const Pixel_t* w = src->buffer;
  const Pixel_t* c = src->buffer + 1;
  const Pixel_t* e = src->buffer + 2;

  for (Pixel_t *d = dst->buffer + 1; d < dst->buffer + BUFFSIZE - 1; d++) {
    /* uint16_t value = ((*w++ & 15) + (*c & 15) + (*e++ & 15)) / 3 + 1; */
    uint16_t value = (*w++ + *c++ + *e++) / 3 + 1;

    if (value >= 256) {
      value = 0;
    }

    /* value = (*c & 0x1111000) | value; */

    *d = value;
    /*    c++; */
  }
}
