/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_LENS;
char dname[] = "Wave Y";
char desc[] = "Swap rows";


void
run(Context_t *ctx)
{
  int16_t a, b;
  uint16_t i, j;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_init_mask_3x3(active_buffer(ctx));

  pthread_mutex_lock(&ctx->input->mutex);
  a = Input_random_short_range(ctx->input, 0, MAXY);
  for (i = 0; i < (ctx->input->size - 1); i++) {
    b = Input_random_short_range(ctx->input, 0, MAXY);
    for (j = 0; j < WIDTH; j++) {
      set_pixel_nc(dst, j, a, get_pixel_nc(src, j, b));
    }
    a = b;
  }
  pthread_mutex_unlock(&ctx->input->mutex);
}
