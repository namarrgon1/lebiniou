/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

uint32_t version = 0;
u_long options = BE_GFX|BEQ_FIRST;
char desc[] = "Video player";
char dname[] = "Video";

#define FFMPEG_CHECK "ffmpeg -h >/dev/null 2>&1"
#define VIDEO_CMD    "ffmpeg -loglevel quiet -i %s" \
  " -vf \"scale=%d:%d:force_original_aspect_ratio=decrease,pad=%d:%d:(ow-iw)/2:(oh-ih)/2:black,setsar=1,vflip\"" \
  " -pix_fmt gray -vcodec rawvideo -f image2pipe -r %d -vsync cfr -"

extern char *video_filename;
static FILE *video = NULL;


static FILE *
open_video(Context_t *ctx)
{
  FILE *stream = NULL;

  if (check_command(FFMPEG_CHECK) == -1) {
    VERBOSE(printf("[!] %s: ffmpeg binary not found, plugin disabled\n", __FILE__));
    return NULL;
  } else {
    if ((NULL == video_filename) && (NULL == (video_filename = getenv("LEBINIOU_VIDEO")))) {
      VERBOSE(printf("[!] %s: no video defined\n", __FILE__));
      return NULL;
    } else {
      char *cmd = g_strdup_printf(VIDEO_CMD, video_filename, WIDTH, HEIGHT, WIDTH, HEIGHT, ctx->max_fps);
#ifdef DEBUG
      VERBOSE(printf("[i] %s: cmd= %s\n", __FILE__, cmd));
#endif
      if (NULL == (stream = popen(cmd, "r"))) {
        xperror("popen");
      } else {
        VERBOSE(printf("[i] %s: opened stream from %s\n", __FILE__, video_filename));
      }
      g_free(cmd);
    }
  }

  return stream;
}


int8_t
create(Context_t *ctx)
{
  video = open_video(ctx);
  return (NULL != video);
}


void
destroy(Context_t *ctx)
{
  if (NULL != video)
    if (-1 == pclose(video)) {
      xperror("pclose");
    }
}


void
run(Context_t *ctx)
{
  void *dst = passive_buffer(ctx)->buffer;
  size_t res = fread(dst, sizeof(Pixel_t), BUFFSIZE, video);

  if (res == BUFFSIZE) {
#ifdef DEBUG
    VERBOSE(printf("%s: read %ld bytes\n", __FILE__, res));
#endif
  } else {
#ifdef DEBUG
    printf("%s: short count: %ld\n", __FILE__, res);
#endif
    if (feof(video)) {
#ifdef DEBUG
      printf("%s: end of file reached\n", __FILE__);
#endif
      int res = pclose(video);
      if (res != 0) {
#ifdef DEBUG
        printf("%s: command exited with return code %d\n", __FILE__, res);
#endif
      }
      video = open_video(ctx);
    } else if (ferror(video)) {
      xerror("%s: an error occurred\n", __FILE__);
    }
  }
}
