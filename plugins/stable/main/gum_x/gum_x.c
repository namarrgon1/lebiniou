/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "parameters.h"
#include "gum.h"

/* TODO optimize
 * we can precompute an array for the "idx" variable
 * but then we have to handle on_delay_change event
 * --oliv3
 */

uint32_t version = 0;
u_long options = BE_LENS|BEQ_HOR;
char dname[] = "Gum X";
char desc[] = "Gum effect";

static inline void
gum(Context_t *ctx, u_short x, u_short max_y)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  short y;
  float cy, dy;

  dy = (float)HHEIGHT / (float)(HEIGHT - max_y);
  for (cy = y = MAXY; y >= max_y; y--) {
    set_pixel_nc(dst, x, y,
                 get_pixel_nc(src, x, (u_short)cy));
    cy -= dy;
  }

  dy = (float)(HEIGHT - max_y) / (float)HHEIGHT;
  for ( ; y >= 0; y--) {
    set_pixel_nc(dst, x, y,
                 get_pixel_nc(src, x, (u_short)cy));
    cy -= dy;
  }
}


static inline void
do_gum(Context_t *ctx, u_short x, float val)
{
  u_short max_y = HHEIGHT + val * volume_scale * HHEIGHT;
  gum(ctx, x, max_y);
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


void
run(Context_t *ctx)
{
  u_short x;

  pthread_mutex_lock(&ctx->input->mutex);
  for (x = 0; x < WIDTH; x++) {
    u_short idx = (u_short)((float)x / (float)WIDTH * (float)ctx->input->size);
    float value = Input_clamp(ctx->input->data[A_MONO][idx]);

    do_gum(ctx, x, value);
  }
  pthread_mutex_unlock(&ctx->input->mutex);
}
