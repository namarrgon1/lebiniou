/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_UNIQUE;
char desc[] = "Cellular automaton";
char dname[] = "Life";

void
run(Context_t *ctx)
{
  u_short i, j;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  for (j = 0; j < HEIGHT; j++)
    for (i = 0; i < WIDTH; i++) {
      u_char count = 0;

      /* C macros are sooo handy */
#define LIFE(dx, dy) {                            \
        short xdx, ydy;                           \
        xdx = dx;                                 \
        ydy = dy;                                 \
        if (xdx < 0) xdx = MAXX;                  \
        else if (xdx > MAXX) xdx = 0;             \
        if (ydy < 0) ydy = MAXY;                  \
        else if (ydy > MAXY) ydy = 0;             \
        if (get_pixel_nc(src, xdx, ydy)) count++; \
      }

      LIFE(i-1, j-1);
      LIFE(i-1, j+0);
      LIFE(i-1, j+1);
      LIFE(i+0, j-1);
      LIFE(i+0, j+1);
      LIFE(i+1, j-1);
      LIFE(i+1, j+0);
      LIFE(i+1, j+1);

      /* A dead cell with exactly three or six live
      neighbors becomes a live cell (birth). */
      if (!get_pixel_nc(src, i, j) && (count == 3)) {
        set_pixel_nc(dst, i, j, b_rand_int_range(200, 255));
      } else {
        /* A live cell with two or three live neighbors
           stays alive (survival). */
        Pixel_t c;
        if ((c = get_pixel_nc(src, i, j))
            && ((count == 2) || (count == 3))) {
          if (!--c) {
            c = 255;
          }
          set_pixel_nc(dst, i, j, c);
        } else
          /* In all other cases, a cell dies or
             remains dead (overcrowding or loneliness) */
        {
          set_pixel_nc(dst, i, j, 0);
        }
      }
    }
}
