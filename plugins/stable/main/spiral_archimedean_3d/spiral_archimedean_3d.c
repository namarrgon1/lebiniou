/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "parameters.h"

uint32_t version = 0;
u_long options = BE_SFX3D|BEQ_NORANDOM;
char dname[] = "Spiral archimedean";
u_long mode = OVERLAY;
char desc[] = "3D Archimedean spiral";

static double volume_scale = 0;

json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, -0.01, 0.01);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale, 0, 1);
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


void
run(Context_t *ctx)
{
  static float t  = 0;
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_clear(dst);

  float random = Input_get_volume(ctx->input) * volume_scale;

  for (t = 0 ; t < 8 * M_PI ; t += 0.1) {
    Point3d_t P;

    P.pos.x = random * expf(0.15 * t) * cosf(2 * t);
    P.pos.y = random * expf(0.15 * t) * sinf(2 * t);
    P.pos.z = -1 + random * expf(0.15 * t);

    set_pixel_3d(&ctx->params3d, dst, &P, Input_random_color(ctx->input));
  }
}
