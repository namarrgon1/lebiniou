/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * EffecTV - Realtime Digital Video Effector
 * Copyright (C) 2001-2006 FUKUCHI Kentaro
 *
 * StreakTV - afterimage effector.
 * Copyright (C) 2001-2002 FUKUCHI Kentaro
 *
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_WEBCAM|BEQ_MUTE_CAM;
char desc[] = "StreakTV plugin from the EffecTV project";
char dname[] = "TV streak";

u_long mode = OVERLAY;

#define PLANES 32
#define STRIDE 4
#define STRIDE_MASK 0xf8f8f8f8
#define STRIDE_SHIFT 3

static Buffer8_t *planes[PLANES];
static Pixel_t *planetable[PLANES];
static int plane = 0;


int8_t
create(Context_t *ctx)
{
  int i;

  assert(PLANES <= CAM_SAVE);
  for (i = 0; i < PLANES; i++) {
    planes[i] = Buffer8_new();
    planetable[i] = planes[i]->buffer;
  }

  return 1;
}


void
destroy(Context_t *ctx)
{
  int i;

  for (i = 0; i < PLANES; i++) {
    Buffer8_delete(planes[i]);
  }
}


void
run(Context_t *ctx)
{
  uint32_t i;
  uint8_t cf;
  Pixel_t *dst;

  dst = passive_buffer(ctx)->buffer;

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  for (i = 0; i < BUFFSIZE; i++) {
    planetable[plane][i] = (ctx->cam_save[ctx->cam][0]->buffer[i] & STRIDE_MASK) >> STRIDE_SHIFT;
  }
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);

  cf = plane & (STRIDE-1);
  for (i = 0; i < BUFFSIZE; i++) {
    dst[i] = planetable[cf][i]
             + planetable[cf+STRIDE][i]
             + planetable[cf+STRIDE*2][i]
             + planetable[cf+STRIDE*3][i]
             + planetable[cf+STRIDE*4][i]
             + planetable[cf+STRIDE*5][i]
             + planetable[cf+STRIDE*6][i]
             + planetable[cf+STRIDE*7][i];
  }

  plane++;
  plane = plane & (PLANES-1);
}
