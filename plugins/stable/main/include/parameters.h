/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_PLUGIN_PARAMETERS_H
#define __BINIOU_PLUGIN_PARAMETERS_H

#define BPP_BORDER_MODE           "border_mode"
#define BPP_BORDER_X              "border_x"
#define BPP_BORDER_Y              "border_y"
#define BPP_CHANNELS              "channels"
#define BPP_COLOR_FACTOR          "color_factor"
#define BPP_CONNECT               "connect"
#define BPP_DECAY                 "decay"
#define BPP_DELAY                 "delay"
#define BPP_DIRECTION             "direction"
#define BPP_EFFECT                "effect"
#define BPP_FLOW_ID               "flow_id"
#define BPP_LENGTH                "length"
#define BPP_LENGTH_MIN            "length_min"
#define BPP_LENGTH_MAX            "length_max"
#define BPP_MODE                  "mode"
#define BPP_MOVE_FACTOR           "move_factor"
#define BPP_NB_MIN_ROTORS         "nb_min_rotors"
#define BPP_NB_SPHERES            "nb_spheres"
#define BPP_OSCILLO_LENGTH_FACTOR "oscillo_length_factor"
#define BPP_P_FACTOR              "p_factor"
#define BPP_PARTICLES             "particles"
#define BPP_PATH                  "path"
#define BPP_POS_FACTOR            "pos_factor"
#define BPP_PROBA_VISIBLE         "proba_visible"
#define BPP_RADIUS_FACTOR         "radius_factor"
#define BPP_REVERSE               "reverse"
#define BPP_SCALE                 "scale"
#define BPP_SENSITIVITY           "sensitivity"
#define BPP_SPECTRUM_ID_FACTOR    "spectrum_id_factor"
#define BPP_SPAN_SIZE             "span_size"
#define BPP_SPEED                 "speed"
#define BPP_SPEED_FACTOR          "speed_factor"
#define BPP_TTL_FACTOR            "ttl_factor"
#define BPP_USE_ASPECT_RATIO      "use_aspect_ratio"
#define BPP_VEL_FACTOR            "vel_factor"
#define BPP_VOLUME_SCALE          "volume_scale"
#define BPP_WINDOWING_FACTOR      "windowing_factor"
#define BPP_XOFFSET               "x_offset"

#endif /* __BINIOU_PLUGIN_PARAMETERS_H */
