/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FREQ_H
#define __FREQ_H


static double  length_min = 0.01;
static double  length_max = 0.15;
static double  spectrum_id_factor = 6;
static double  speed = 1;


void
get_parameters_freq(json_t *params)
{
  plugin_parameters_add_double(params, BPP_LENGTH_MIN, length_min, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_LENGTH_MAX, length_max, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_SPECTRUM_ID_FACTOR, spectrum_id_factor, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_SPEED, speed, -0.01, 0.01);
}


void
set_parameters_freq(const Context_t *ctx, const json_t *in_parameters)
{
  double __length_min = length_min, __length_max = length_max;

  plugin_parameter_parse_double_range(in_parameters, BPP_LENGTH_MIN, &__length_min, 0.01, 0.2);
  plugin_parameter_parse_double_range(in_parameters, BPP_LENGTH_MAX, &__length_max, 0.02, 10);
  if (__length_min <= __length_max) {
    length_min = __length_min;
    length_max = __length_max;
  }

  plugin_parameter_parse_double_range(in_parameters, BPP_SPECTRUM_ID_FACTOR, &spectrum_id_factor, 0, 20);
  plugin_parameter_parse_double_range(in_parameters, BPP_SPEED, &speed, 0, 10);
}

#endif /* __FREQ_H */
