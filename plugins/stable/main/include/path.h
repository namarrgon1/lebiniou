/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PATH_H
#define __PATH_H

static Path_point_t *path        = NULL; /* current path data */
static uint32_t      path_length = 0;    /* current path length */
static uint16_t      path_idx    = 0;

static int      path_id         = 0; /* current path id */
static uint8_t  path_id_changed = 0;
static double   color_scale     = 2;
static double   scale           = 1; /* scale */


json_t *
get_parameters_path()
{
  json_t *params = json_object();

  plugin_parameters_add_string_list(params, BPP_PATH, paths->size, (const char **)paths_list, path_id);
  plugin_parameters_add_double(params, BPP_COLOR_FACTOR, color_scale, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_SCALE, scale, -0.01, 0.01);

  return params;
}


uint8_t
set_parameters_path(const Context_t *ctx, const json_t *in_parameters)
{
  uint8_t reload = 0;

  if (plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_PATH, paths->size,
      (const char **)paths_list, &path_id, 0, paths->size-1) & PLUGIN_PARAMETER_CHANGED) {
    path_id_changed = 1;
  }
  plugin_parameter_parse_double_range(in_parameters, BPP_COLOR_FACTOR, &color_scale, 0, 100);
  reload |= plugin_parameter_parse_double_range(in_parameters, BPP_SCALE, &scale, 0, 100) & PLUGIN_PARAMETER_CHANGED;

  return reload;
}


#endif /* __PATH_H */
