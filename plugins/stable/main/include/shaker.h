/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SHAKER_H
#define __SHAKER_H

#include "parameters.h"

#define SOUND_DISTURB_K 127


static double volume_scale = 0;

json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, -0.01, 0.01);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale, 0, 100);
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


static inline void
check_width(short *value)
{
  if (*value < 0) {
    *value += WIDTH;
  } else if (*value >= (short)WIDTH) {
    *value -= WIDTH;
  }
}

static inline void
check_height(short *value)
{
  if (*value < 0) {
    *value += HEIGHT;
  } else if (*value >= (short)HEIGHT) {
    *value -= HEIGHT;
  }
}

static inline void
displace(const Buffer8_t *src, Buffer8_t *dst,
         short i, short j, short dx, short dy)
{
  if ((i < SOUND_DISTURB_K) || (i > MAXX - SOUND_DISTURB_K)) {
    check_width(&dx);
  }
  if ((j < SOUND_DISTURB_K) || (j > MAXY - SOUND_DISTURB_K)) {
    check_height(&dy);
  }
  set_pixel_nc(dst, i, j, get_pixel_nc(src, dx, dy));
}

#endif /* __SHAKER_H */
