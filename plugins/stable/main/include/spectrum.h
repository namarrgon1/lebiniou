/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SPECTRUM_H
#define __SPECTRUM_H

#include "parameters.h"

static double volume_scale = 0;

json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, -0.01, 0.01);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale, 0, 100);
}


json_t *
parameters(const Context_t *ctx, json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


static inline u_short
float_to_ushort(float x, u_short minval, u_short maxval)
{
  u_short n;
  x = floor(x); /* rounded to lower int */
  if (x < minval) {
    n = minval;
  } else if (x > maxval) {
    n = maxval;
  } else {
    n = (ushort)x;
  }
  return n;
}


static inline u_short
float_to_nearest_ushort(float x, u_short minval, u_short maxval)
{
  return float_to_ushort(x + 0.5 /* will be rounded to nearest int */, minval, maxval);
}


#endif /* __SPECTRUM_H */

