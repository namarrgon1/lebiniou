/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSCILLO_H
#define __OSCILLO_H

#include "parameters.h"

static Porteuse_t *P[3] = { NULL, NULL, NULL };
static void set_run_ptr();

/* Parameters */
static int channels = 1;
static int do_connect = 1;
static double volume_scale = 0;


void set_parameters(const Context_t *ctx, const json_t *in_parameters);

uint8_t
set_parameters_oscillo(const Context_t *ctx, const json_t *in_parameters)
{
  uint8_t changed = 0;
  changed |= plugin_parameter_parse_int_range(in_parameters, BPP_CHANNELS, &channels, 1, 2) & PLUGIN_PARAMETER_CHANGED;
  changed |= plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale, 0, 100) & PLUGIN_PARAMETER_CHANGED;

  plugin_parameter_parse_int_range(in_parameters, BPP_CONNECT, &do_connect, 0, 1);

  return changed;
}


json_t *
get_parameters()
{
  json_t *params = json_object();

  plugin_parameters_add_int(params, BPP_CHANNELS, channels, -1, 1);
  plugin_parameters_add_int(params, BPP_CONNECT, do_connect, -1, 1);
  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, -0.01, 0.01);

  return params;
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


int8_t
create(Context_t *ctx)
{
  P[0] = Porteuse_new(ctx->input->size, A_MONO);
  P[1] = Porteuse_new(ctx->input->size, A_LEFT);
  P[2] = Porteuse_new(ctx->input->size, A_RIGHT);

  return 1;
}


void
destroy(Context_t *ctx)
{
  for (uint8_t i = 0; i < 3; i++) {
    Porteuse_delete(P[i]);
  }
}


#endif /* __OSCILLO_H */
