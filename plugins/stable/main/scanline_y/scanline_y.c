/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_IMAGE;
char dname[] = "Scanline Y";
char desc[] = "Vertical scanline";


#define MINPCT 0.01
#define DPCT   0.002

/* conf */
typedef struct {
  float size;          /* line is %of screen HEIGHT */
  char dir;            /* -1 or +1 */
  u_char ping_pong;    /* bounce */
} yscanline_conf;

static yscanline_conf yscanline_cf;


int8_t
create(Context_t *ctx)
{
  yscanline_cf.size = MINPCT;
  yscanline_cf.dir = 1;
  yscanline_cf.ping_pong = 0;

  return 1;
}


void
on_switch_on(Context_t *ctx)
{
  yscanline_cf.dir = (b_rand_boolean()) ? -1 : 1;
  yscanline_cf.ping_pong = b_rand_boolean();
}


static void
y_scanline(Context_t *ctx, float amount)
{
  u_short s, size = amount * HEIGHT; /* number of rows */
  u_short i;
  static int y_line = 0;
  Buffer8_t *dst = NULL;
  ImageFader_t *imgf = ctx->imgf;
  Buffer8_t *src = imgf->cur->buff;

  swap_buffers(ctx);
  dst = passive_buffer(ctx);

  for (s = 0; s < size; s++) {
    for (i = 0; i <= MAXX; i++) {
      set_pixel_nc(dst, i, y_line, get_pixel_nc(src, i, y_line));
    }

    y_line += yscanline_cf.dir;
    if (y_line > MAXY) {
      if (yscanline_cf.ping_pong) {
        y_line = MAXY;
        yscanline_cf.dir = -yscanline_cf.dir;
      } else {
        y_line = 0;
      }
    } else if (y_line < 0) {
      if (yscanline_cf.ping_pong) {
        y_line = 0;
        yscanline_cf.dir = -yscanline_cf.dir;
      } else {
        y_line = MAXY;
      }
    }
  }
}


void
run(Context_t *ctx)
{
  y_scanline(ctx, yscanline_cf.size);
}
