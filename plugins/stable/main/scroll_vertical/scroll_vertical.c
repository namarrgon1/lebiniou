/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "parameters.h"


uint32_t version = 0;
u_long options = BE_SCROLL|BEQ_VER;
char dname[] = "Scroll vertical";
char desc[] = "Scroll the screen upwards/downwards";

/* parameters */
enum Direction { DOWNWARDS = 0, UPWARDS, DIRECTION_NB } Mode_e;
const char *direction_list[DIRECTION_NB] = { "Downwards", "Upwards" };
static enum Direction direction = UPWARDS;

static Pixel_t *save = NULL;


static void
scroll_bt(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  memcpy((void *)save, (const void *)src->buffer+(BUFFSIZE-WIDTH-1)*sizeof(Pixel_t), WIDTH*sizeof(Pixel_t));
  memcpy((void *)(dst->buffer+WIDTH*sizeof(Pixel_t)), (const void *)src->buffer, (BUFFSIZE-WIDTH)*sizeof(Pixel_t));
  memcpy((void *)dst->buffer, (const void *)save, WIDTH*sizeof(Pixel_t));
}


static void
scroll_tb(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  memcpy((void *)save, (const void *)(src->buffer+WIDTH*sizeof(Pixel_t)), WIDTH*sizeof(Pixel_t));
  memcpy((void *)dst->buffer, (const void *)(src->buffer+WIDTH*sizeof(Pixel_t)), (BUFFSIZE-WIDTH)*sizeof(Pixel_t));
  memcpy((void *)(dst->buffer+(BUFFSIZE-WIDTH-1)*sizeof(Pixel_t)), (const void *)save, WIDTH*sizeof(Pixel_t));
}


static void (*run_ptr)(struct Context_s *) = &scroll_bt;

static void
set_run_ptr()
{
  if (direction) {
    run_ptr = &scroll_bt;
  } else {
    run_ptr = &scroll_tb;
  }
}


json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_string_list(params, BPP_DIRECTION, DIRECTION_NB, direction_list, direction);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_DIRECTION, DIRECTION_NB, direction_list, (int *)&direction, 0, DIRECTION_NB-1) & PLUGIN_PARAMETER_CHANGED) {
    set_run_ptr();
  }
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  enum Direction rand = b_rand_boolean();

  if (rand != direction) {
    direction = rand;
    set_run_ptr();
  }
}


int8_t
create(Context_t *ctx)
{
  save = xcalloc(WIDTH, sizeof(Pixel_t));

  return 1;
}


void
destroy(Context_t *ctx)
{
  xfree(save);
}


void
run(Context_t *ctx)
{
  run_ptr(ctx);
}
