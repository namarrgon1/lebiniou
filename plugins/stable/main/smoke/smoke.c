/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation.h"


uint32_t version = 0;
u_long options = BE_DISPLACE;
char dname[] = "Smoke";
char desc[] = "Give lines a smoky effect";


static Translation_t *t_smoke = NULL;
static int Speed, Random;


static Map_t
smoke(const short in_x, const short in_y)
{
  const u_short x = in_x, y = in_y;
  int map_x, map_y;
  Map_t m;

  map_x = x - (5 + b_rand_int_range(0, 12 * Random / 100)) * Speed / 100;
  map_y = y - (5 + b_rand_int_range(0, 12 * Random / 100)) * Speed / 100;

  if (map_y > MAXY || map_y < MINY || map_x > MAXX || map_x < MINX) {
    map_x = map_y = 0;
  }

  m.map_x = map_x;
  m.map_y = map_y;

  return m;
}


static void
init_params()
{
  Speed = b_rand_int_range(30, 300);
  Random = b_rand_int_range(12, 100);
}


void
on_switch_on(Context_t *ctx)
{
  Translation_batch_init(t_smoke);
}


int8_t
create(Context_t *ctx)
{
  t_smoke = Translation_new(&smoke, &init_params);

  return 1;
}


void
destroy(Context_t *ctx)
{
  Translation_delete(t_smoke);
}


void
run(Context_t *ctx)
{
  Translation_run(t_smoke, ctx);
}
