/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_IMAGE|BEQ_MUTE_CAM|BEQ_NORANDOM;
char desc[] = "1dTV plugin from the EffecTV project";
char dname[] = "TV 1D";
u_long mode = OVERLAY;

static int line = 0;
static int prevline = 0;
static Pixel_t *linebuf = NULL;


int8_t
create(Context_t *ctx)
{
  line = MAXY;
  linebuf = xcalloc(WIDTH, sizeof(Pixel_t));

  return 1;
}


void
destroy(Context_t *ctx)
{
  xfree(linebuf);
}


static void
blitline_buf(Pixel_t *src, Pixel_t *dest)
{
  memcpy(dest + WIDTH * prevline, linebuf, WIDTH * sizeof(Pixel_t));

  src += WIDTH * line;
  dest += WIDTH * line;
  memcpy(dest, src, WIDTH * sizeof(Pixel_t));
  memcpy(linebuf, src, WIDTH * sizeof(Pixel_t));

  prevline = line;
}


void
run(Context_t *ctx)
{
  int i;
  Pixel_t *src = NULL;
  Pixel_t *dst = passive_buffer(ctx)->buffer;

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  src = ctx->cam_save[ctx->cam][0]->buffer;
  blitline_buf(src, dst);
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);

  if (line) {
    line--;
  } else {
    line = MAXY;
  }

  dst += WIDTH * line;
  for (i = 0; i < WIDTH; i++) {
    dst[i] = 127;
  }
}
