/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"


uint32_t version = 0;
u_long options = BEQ_SPLASH|BEQ_FIRST|BEQ_IMAGE;
char desc[] = "Splash screen effect";
char dname[] = "Splash";

static u_long *splash_map;
static u_long *dst_splash_map;
static u_char splashing = 1;
static BTimer_t *step_splash_timer = NULL;
static BTimer_t *splash_timer = NULL;


#define SPLASH_TIMESLICE 0.1
#define MAX_SPLASH       5.0  /* stop after 5 sec */


void
on_switch_on(Context_t *ctx)
{
  u_long i;

  for (i = 0; i < WIDTH; i++) {
    splash_map[i] = 0;
  }
  for ( ; i < BUFFSIZE-WIDTH; i++) {
    splash_map[i] = (u_long)(WIDTH + drand48 () * (BUFFSIZE-WIDTH));
  }
  for ( ; i < BUFFSIZE; i++) {
    splash_map[i] = 0;
  }

  b_timer_start(step_splash_timer);
  b_timer_start(splash_timer);
  splashing = 1;
}


int8_t
create(Context_t *ctx)
{
  splash_map = xcalloc(BUFFSIZE, sizeof(u_long));
  dst_splash_map = xcalloc(BUFFSIZE, sizeof(u_long));
  step_splash_timer = b_timer_new();
  splash_timer = b_timer_new();

  return 1;
}


void
destroy(Context_t *ctx)
{
  if (NULL != images) {
    xfree(splash_map);
    xfree(dst_splash_map);
    b_timer_delete(step_splash_timer);
    b_timer_delete(splash_timer);
  }
}


static void
splash_blur()
{
  u_long i;

  for (i = WIDTH; i < BUFFSIZE-WIDTH; i++) {
    u_long sum = splash_map[i-WIDTH];
    sum += splash_map[i+WIDTH];
#ifndef TEST
    sum += splash_map[i-1];
    sum += splash_map[i+1];
    sum >>= 2;
#else
    sum >>= 1;
#endif

    sum += i;
    sum >>= 1;

    dst_splash_map[i] = (u_long)sum;
  }

  for (i = WIDTH; i < BUFFSIZE-WIDTH; i++) {
    splash_map[i] = dst_splash_map[i];
  }
}


static void
splash_check_splash()
{
  if (b_timer_elapsed(splash_timer) > MAX_SPLASH) {
    splashing = 0;
  }
}


static void
splash(Context_t *ctx)
{
  u_long i;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  /* FIXME Optimize */
  for (i = 0; i < BUFFSIZE; i++)
    dst->buffer[i] = (u_long)((src->buffer[splash_map[i]]
                               + ctx->imgf->cur->buff->buffer[splash_map[i]]) / 2);
}


void
run(Context_t *ctx)
{
  if (splashing == 1) {
    if (b_timer_elapsed(step_splash_timer) > SPLASH_TIMESLICE) {
      splash_blur();
      splash_check_splash();
      b_timer_start(step_splash_timer);
    }
    splash(ctx);
  } else {
    Buffer8_copy(active_buffer(ctx), passive_buffer(ctx));
  }
}
