/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX|BEQ_MUTE_CAM|BEQ_NORANDOM;
char desc[] = "DiffTV";
char dname[] = "TV diff";

u_long mode = OVERLAY;


void
on_switch_on(Context_t *ctx)
{
  ctx->ref_taken[ctx->cam] = 0;
}


void
run(Context_t *ctx)
{
  Pixel_t *src1, *start, *src2, *dst;

  dst = start = passive_buffer(ctx)->buffer;

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  src1 = ctx->cam_save[ctx->cam][0]->buffer;
  src2 = ctx->cam_ref[ctx->cam]->buffer;
  for (; dst < start + BUFFSIZE * sizeof(Pixel_t); src1++, src2++, dst++) {
    *dst = abs(*src1 - *src2);
  }
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);
}
