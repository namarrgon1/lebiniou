/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is based on the "snake" plugin
 *   - The snake shape is not a line now, but is based on sound waveform
 *
 *   - Color: linked to sound amplitude
 */

#include "context.h"
#include "oscillo.h"
#include "parameters.h"

static Porteuse_t *P = NULL;
static int snake_connect = 1;
u_short sgn_size = 0 ;


uint32_t version = 0;
u_long options = BE_GFX|BEQ_UNIQUE|BE_SFX2D;

u_long mode = OVERLAY;

char dname[] = "Snake oscillo";
char desc[] = "Snake oscillo";


static u_short x = 0, y = 0;

static double volume_scale = 0;
static int snake_mode = 0;
static double length_min = 0; /* minimum length of the snake, in pixels, scales with WIDTH */
static double length_max = 0; /* maximum length of the snake, in pixels, scales with WIDTH */
static double spectrum_id_factor = 0;
static double color_factor = 0; /* scaling of the computed color */

json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, -0.01, 0.01);
  plugin_parameters_add_int(params, BPP_MODE, snake_mode, -1, 1);
  plugin_parameters_add_double(params, BPP_LENGTH_MIN, length_min, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_LENGTH_MAX, length_max, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_SPECTRUM_ID_FACTOR, spectrum_id_factor, -0.01, 0.01);
  plugin_parameters_add_double(params, BPP_COLOR_FACTOR, color_factor, -0.01, 0.01);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale, 0, 100);
  plugin_parameter_parse_int_range(in_parameters, BPP_MODE, &snake_mode, 0, 2);

  double __length_min = length_min, __length_max = length_max;
  plugin_parameter_parse_double_range(in_parameters, BPP_LENGTH_MIN, &__length_min, 0.01, 0.2);
  plugin_parameter_parse_double_range(in_parameters, BPP_LENGTH_MAX, &__length_max, 0.02, 0.5);
  if (__length_min <= __length_max) {
    length_min = __length_min;
    length_max = __length_max;
  }

  plugin_parameter_parse_double_range(in_parameters, BPP_SPECTRUM_ID_FACTOR, &spectrum_id_factor, 0, 4);
  plugin_parameter_parse_double_range(in_parameters, BPP_COLOR_FACTOR, &color_factor, 0, 5);
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


void
init_oscillo(Context_t *ctx, u_short x, u_short y, u_short length, u_short dir, short inc)
{
  double y_factor = 1.0;
  double x_factor = 0.4;

  Porteuse_delete(P);

  u_short sgn_length = round(5.0f * (float)length);
  if (sgn_length > sgn_size) {
    sgn_length = sgn_size;
  }

  P = Porteuse_new(sgn_length, A_MONO);

  // oscillo
  u_short i = 0;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  if (dir) {
    t.v_j_factor = HMAXY * y_factor * volume_scale;
    t.v_i.x = 1.0 / (float)(P->size - 1) * (float)length;

    if (inc < 0) {
      x = x - length;
    }
  } else {
    t.v_j_factor = HMAXX * x_factor * volume_scale;
    t.v_i.y = 1.0 / (float)(P->size - 1) * (float)length;

    if (inc < 0) {
      y = y - length;
    }
  }

  P->origin.x = x;
  P->origin.y = y;

  double win_avg = 0.0;
  double color_factor = 3.0;
  /* approx */
  u_short win_overlap = ctx->input->size >> 1 ;
  u_short win_size = floor((double)(ctx->input->size - win_overlap) / (double)P->size) + win_overlap;

  double tr = 0.1;
  u_short r = floor((double)P->size * tr);
  u_short factor_orig = t.v_j_factor;

  for (i = 0; i < P->size; i++) {
    /* Use a tukey window for smoother join of successive segments */
    double tc ;
    if (i < r/2) {
      tc = cos(2*M_PI * (i - r/2) / r) / 2.0 + 0.5;
    } else if (i > P->size - r/2) {
      tc = cos(2*M_PI * (i - 1.0 + r/2) / r) / 2.0 + 0.5;
    } else {
      tc = 1.0;
    }

    t.v_j_factor = floor((double)factor_orig * tc);
    P->trans[i] = t;

    /* compute color */
    if (i == P->size-1) {
      win_avg = compute_avg_abs(ctx->input->data[A_MONO], i*(win_size-win_overlap), ctx->input->size);
    } else {
      win_avg = compute_avg_abs(ctx->input->data[A_MONO], i*(win_size-win_overlap), i*(win_size-win_overlap)+win_size);
    }
    win_avg = color_factor * win_avg;
    if (win_avg > 1.0) {
      win_avg = 1.0;
    }
    P->color[i] = win_avg * PIXEL_MAXVAL;
  }

  Porteuse_init_alpha(P);
}


int8_t
create(Context_t *ctx)
{
  sgn_size = ctx->input->size;

  return 1;
}


void
destroy(Context_t *ctx)
{
  Porteuse_delete(P);
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 0.1;
  snake_mode = 1;    /* 0: direction changes at each run, 1: direction randomy changes, 2: also randomly changes orientation */
  length_min = 0.01; /* minimum length of the snake, between 0 and 1, 1 meaning WIDTH */
  length_max = 0.08; /* maximum length of the snake, between 0 and 1, 1 meaning WIDTH */
  spectrum_id_factor = 2; /* snake length will be length_max_px - average_frequency * spectrum_id_factor */
  color_factor = 3.0; /* scaling of the computed color */
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  Buffer8_clear(dst);

  u_short original_fft_size = 513; /* FFT size used when below parameters were set */
  u_short length_min_px = round(length_min * WIDTH); /* minimum length of the snake, in pixels, scales with WIDTH */
  u_short length_max_px = round(length_max * WIDTH); /* maximum length of the snake, in pixels, scales with WIDTH */
  double spectrum_low_treshold_factor = 0.1; /* spectrum value higher than this treshold will be used, between 0 and 1 */
  u_short spectrum_id_orientation_factor = 40; /* smaller means changing orientation more often */

  /* choose direction and increment mode */
  u_short change_inc_on_hf = 1; /* 0: no change, 1: change orientation more often on high frequency */

  u_short average_freq_id = compute_avg_freq_id(ctx->input, spectrum_low_treshold_factor);

  /* scale average frequency id depending of input->spectrum_size */
  average_freq_id = round((double)average_freq_id * (double)original_fft_size / (double)ctx->input->spectrum_size);

  /* compute snake length based on average frequency */
  u_short length = length_max - average_freq_id * spectrum_id_factor;
  if (length < length_min_px) {
    length = length_min_px;
  }
  if (length > length_max_px) {
    length = length_max_px;
  }

  static u_short dir = 0; /* direction: 0 is Y and 1 is X */
  short inc = 1;  /* increment: 1 or -1 */

  switch (snake_mode) {
    default:
    case 0:
      dir = !dir;
      break;

    case 1:
      dir = drand48() < .5;
      break;

    /* random dir and inc */
    case 2:
      dir = drand48() < .5;
      inc = drand48() < .5 ? -1 : 1;
      break;
  }

  /* if set, change orientation on high frequency */
  static short inc_hf = 1;
  if (change_inc_on_hf && (drand48() < average_freq_id / (double)spectrum_id_orientation_factor)) {
    inc_hf = -inc_hf;
    inc = inc_hf;
  }

  /* avoid going back on previous path */
  static u_short last_dir = 0;
  static u_short last_inc = -1;
  if (last_dir == dir) {
    inc = last_inc;
  }
  last_dir = dir;
  last_inc = inc;

  /* remove length bias due to different HEIGHT and WIDTH */
  if (!dir) {
    length = (u_short)ceil((double)length * (double)HEIGHT / (double)WIDTH);
  }

  snake_connect = 1;
  init_oscillo(ctx, x, y, length, dir, inc);
  Porteuse_draw(P, ctx, snake_connect);

  /* X direction */
  if (dir) {
    x = (x + inc * length) % WIDTH;
  } else {
    /* Y direction */
    y = (y + inc * length) % HEIGHT;
  }
}
