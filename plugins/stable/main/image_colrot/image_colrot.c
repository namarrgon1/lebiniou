/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/* Colrot, Color Rotation

   basic idea of plugin: Show only part of the colors from image,
   and scroll thru colors. Not visible colors are transparent,
   showing stuff happening under. Using lens mode.

   This plugin is 'normal', pretty agressive version of Colrot
*/


#include "context.h"
#include "images.h"

#define MASK_SIZE  15 /* what size of chunks colorspace is divided */
#define COLORCOUNT  5 /* how many colors are visible in each chunk */

uint32_t version = 0;
u_long options = BE_GFX|BE_LENS|BEQ_IMAGE;
u_long mode = OVERLAY;
char desc[] = "Show image scrolling colors";
char dname[] = "Image colrot";

static Pixel_t min1 = 0, max1 = COLORCOUNT;


void
run(Context_t *ctx)
{
  u_long k;
  Pixel_t *src = ctx->imgf->cur->buff->buffer;
  Pixel_t *dst =  passive_buffer(ctx)->buffer;

  for (k = 0; k < BUFFSIZE; k++, src++) {
    if ( /* max1 is bigger than min, show values between them */
      ((max1 > min1) &&
       ((*src & MASK_SIZE) > min1) && ((*src & MASK_SIZE) < max1)) ||
      /* max is rotated over, show values below max or above min */
      ((max1 < min1) &&
       (((*src & MASK_SIZE)> min1) || ((*src & MASK_SIZE) < max1)))) {
      dst[k] = *src;
    } else {
      dst[k] = 0;
    }
  }

  min1++;
  max1++;
  if (min1 > MASK_SIZE) {
    min1 = 0;
  }
  if (max1 > MASK_SIZE) {
    max1 = 0;
  }
}
