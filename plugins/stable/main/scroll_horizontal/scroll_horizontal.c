/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "parameters.h"


uint32_t version = 0;
u_long options = BE_SCROLL|BEQ_HOR;
char dname[] = "Scroll horizontal";
char desc[] = "Scroll the screen leftwards/rightwards";

/* parameters */
enum Direction { LEFTWARDS = 0, RIGHTWARDS, DIRECTION_NB } Mode_e;
const char *direction_list[DIRECTION_NB] = { "Leftwards", "Rightwards" };
static enum Direction direction = LEFTWARDS;


static void
scroll_rl(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  Pixel_t save = src->buffer[0];

  memcpy((void *)dst->buffer, (const void *)(src->buffer+sizeof(Pixel_t)), (BUFFSIZE-1)*sizeof(Pixel_t));
  dst->buffer[BUFFSIZE-1] = save;
}


static void
scroll_lr(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  Pixel_t save = src->buffer[BUFFSIZE-1];

  memcpy((void *)(dst->buffer+sizeof(Pixel_t)), (const void *)src->buffer, (BUFFSIZE-1)*sizeof(Pixel_t));
  dst->buffer[0] = save;
}


static void (*run_ptr)(struct Context_s *) = &scroll_lr;

static void
set_run_ptr()
{
  if (direction) {
    run_ptr = &scroll_lr;
  } else {
    run_ptr = &scroll_rl;
  }
}


json_t *
get_parameters()
{
  json_t *params = json_object();
  plugin_parameters_add_string_list(params, BPP_DIRECTION, DIRECTION_NB, direction_list, direction);

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_DIRECTION, DIRECTION_NB, direction_list, (int *)&direction, 0, DIRECTION_NB-1) & PLUGIN_PARAMETER_CHANGED) {
    set_run_ptr();
  }
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  enum Direction rand = b_rand_boolean();

  if (rand != direction) {
    direction = rand;
    set_run_ptr();
  }
}


void
run(Context_t *ctx)
{
  run_ptr(ctx);
}
