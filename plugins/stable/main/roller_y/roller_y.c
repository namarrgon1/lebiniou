/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_ROLL|BEQ_VER|BE_LENS;
char dname[] = "Roll Y";
char desc[] = "Rolls the screen horizontally";


static float roll_theta=0.0, roll_freq=0.03;


static void
inc_theta()
{
  roll_theta += roll_freq;

  if (roll_theta > (2 * M_PI)) {
    roll_theta -= 2 * M_PI;
  } else if (roll_theta < (-2 * M_PI)) {
    roll_theta += 2 * M_PI;
  }
}


void
on_switch_on(Context_t *ctx)
{
  roll_freq = Input_random_float_range(ctx->input, 0.01, 0.06);
  if (b_rand_boolean()) {
    roll_freq = -roll_freq;
  }
#ifdef DEBUG
  VERBOSE(printf("[i] %s: roll_freq= %f\r\n", __FILE__, roll_freq));
#endif
}


void
run(Context_t *ctx)
{
  u_short i;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  /* TODO voir si c'est mieux commente --oliv3 */
  /* Buffer8_clear_border(dst); */

  for (i = 0; i < WIDTH; i++) {
    short  p  = i - HWIDTH;
    float phi = acosf((float)p / (float)(HWIDTH));
    short  b  = (short)((roll_theta + phi) / M_PI * (float)WIDTH);
    u_short j;

    b %= (2 * WIDTH);

    if (b < 0) {
      b += (2 * WIDTH);
    }
    if (b > MAXX) {
      b = 2 * WIDTH - b - 1;
    }

    for (j = 0; j < HEIGHT; j++) {
      set_pixel_nc(dst, i, j, get_pixel_nc(src, b, j));
    }
  }

  inc_theta();
}
