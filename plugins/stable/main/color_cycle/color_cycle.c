/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


char dname[] = "Color cycle";

uint32_t version = 0;
u_long options = BEQ_COLORMAP | BEQ_NORANDOM;
u_long mode = NONE;
char desc[] = "Cycle the current colormap";


/* TODO add a Cmap8_shift_right ? */
void
run(Context_t *ctx)
{
  CmapFader_t *cf = ctx->cf;

  Cmap8_shift_left(cf->cur);
  Cmap8_shift_left(cf->dst);

  cf->refresh = 1;
}
