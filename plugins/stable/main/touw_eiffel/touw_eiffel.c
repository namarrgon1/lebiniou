/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BE_GFX;
char dname[] = "Le Touw Eiffel";
u_long mode = OVERLAY;
char desc[] = "Lightning Eiffel tower";


/*
 * XXX holes only tested in 640x480, TODO: constants derived
 * from macros (eg WIDTH HEIGHT) --oliv3
 */

#define TOUW_EIFFEL_WIDTH (WIDTH/5)

static int *boundaries;
static GRand *rnd;


int8_t
create(Context_t *ctx)
{
  int x;

  boundaries = xcalloc(TOUW_EIFFEL_WIDTH, sizeof(int));

  boundaries[0] = MAXY;
  for (x = 1; x < TOUW_EIFFEL_WIDTH; x++) {
    float v = 400 * 3.5 / (0.6 * (float)x / logf((float)(x + 0.000001) * 3.0));
    /*float v = 300 * 5 / ((float)x / log((float)x));*/
    boundaries[x] = (int)v;
  }

  rnd = g_rand_new();

  VERBOSE(printf("[i] Ouh-ouuuh! \\o/ Le Touw Eiffel !!\n"));

  return 1;
}


void
run(Context_t *ctx)
{
  int i;
  Buffer8_t *dst = passive_buffer(ctx);
  Pixel_t max = ctx->cf->cur->max;

  Buffer8_clear(dst);

  for (i = 0; i < TOUW_EIFFEL_WIDTH; i++) {
    int j;
    for (j = 0; j < boundaries[i]; j++) {
      int right = b_rand_boolean();

      /* XXX: ugly --gab */
      if (g_rand_boolean(rnd) || g_rand_boolean(rnd) ||
          g_rand_boolean(rnd) || g_rand_boolean(rnd) ||
          g_rand_boolean(rnd) || g_rand_boolean(rnd)) {
        continue;
      }

      /* Bottom hole */
      if ((sqrtf(i*i + (j-100)*(j-100)) < 60) && (j < 100+HEIGHT/10)) {
        continue;
      }

      /* Middle hole */
      if ((i < 25) && (j >= 100+HEIGHT/5) && (j < 100+(MAXY-HEIGHT/1.5))&& (j-150 < boundaries[i]/5)) {
        continue;
      }

      /* Brave point qui a traverse tant de pieges, tu peux t'afficher */
      if (right) {
        set_pixel(dst, CENTERX + i, j - 100, max);
      } else {
        set_pixel(dst, CENTERX - i, j - 100, max);
      }
    }
  }
}


void
destroy(Context_t *ctx)
{
  xfree(boundaries);
  g_rand_free(rnd);
}

