/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation.h"


uint32_t version = 0;
u_long options = BE_DISPLACE;
char dname[] = "Hurricane";
char desc[] = "Hurricane effect";


static Translation_t *t_hurricane = NULL;

static int speed, Randomness, slowX, slowY, reverse;
static int xCenter, yCenter;


static Map_t
hurricane(const short x, const short y)
{
  int  dx, dy, map_x, map_y;
  int  speedFactor;
  long sp = speed;
  Map_t m;

  if (Randomness) {
    speedFactor = b_rand_int_range(0, (Randomness + 1)) - Randomness / 3;
    sp = speed * (100L + speedFactor) / 100L;
  } else {
    sp = speed;
  }

  dx = x - xCenter;
  dy = y - yCenter;

  if (slowX || slowY) {
    long dSquared = (long)dx*dx + (long)dy*dy + 1;

    if (slowY) {
      dx = (int)(dx * 2500L / dSquared);
    }
    if (slowX) {
      dy = (int)(dy * 2500L / dSquared);
    }
  }

  if (reverse) {
    sp = -sp;
  }

  map_x = (int)(x + (dy * sp) / 700);
  map_y = (int)(y - (dx * sp) / 700);

  m.map_x = map_x;
  m.map_y = map_y;

  return m;
}


static void
init_params()
{
  speed = b_rand_int_range(30, 300);
  Randomness = b_rand_int_range(0, 100);
  slowX = b_rand_boolean();
  slowY = b_rand_boolean();
  reverse = b_rand_boolean();
  xCenter = b_rand_int_range(HWIDTH/4, MAXX-HWIDTH/4);
  yCenter = b_rand_int_range(HHEIGHT/4, MAXY-HHEIGHT/4);
}


void
on_switch_on(Context_t *ctx)
{
  Translation_batch_init(t_hurricane);
}


int8_t
create(Context_t *ctx)
{
  t_hurricane = Translation_new(&hurricane, &init_params);

  return 1;
}


void
destroy(Context_t *ctx)
{
  Translation_delete(t_hurricane);
}


void
run(Context_t *ctx)
{
  Translation_run(t_hurricane, ctx);
}
