/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"

#define COLORCLASSES 128
#define MAXCOLORCOUNT 27
/* parameters for defining volume-colorcount curve */
#define CURVE_VOL_MIN  0.04 /* minimum volume on which we work */
#define CURVE_VOL_STEP 0.08 /* volume increment to step up on count */
#define CURVE_VOL_MULT 1.55 /* multiplier on count on one step */

uint32_t version = 0;
u_long options = BE_GFX|BE_SFX2D|BEQ_IMAGE;
u_long mode = NORMAL;
char desc[] = "Show image colors on beat";
char dname[] = "Image color beat";
static u_char colorsel = 0;


void
on_switch_on(Context_t *ctx)
{
  colorsel = b_rand_int_range(0, COLORCLASSES);
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  Buffer8_copy(active_buffer(ctx), dst);

  if (ctx->input->on_beat) {
    int colorcount = 0;
    double peak;
    u_long k;

    for (peak = ctx->input->curpeak; peak > CURVE_VOL_MIN; peak -= CURVE_VOL_STEP) {
      colorcount = colorcount * CURVE_VOL_MULT;
      colorcount++;
    }
    if (colorcount > MAXCOLORCOUNT) {
      colorcount = MAXCOLORCOUNT;
    }

    for (k = 0; k < BUFFSIZE; k++) {
      if (ctx->input->on_beat &&
          ((ctx->imgf->cur->buff->buffer[k] + colorsel) % COLORCLASSES) < colorcount) {
        dst->buffer[k] = ctx->imgf->cur->buff->buffer[k];
      }
    }

    if (ctx->input->on_beat) {
      colorsel += colorcount;
      if (colorsel > COLORCLASSES - 1) {
        colorsel = 0;
      }
    }
  }
}
