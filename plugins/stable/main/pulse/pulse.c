/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
u_long options = BEQ_COLORMAP;
u_long mode = NONE;
char desc[] = "Pulse the colormap";
char dname[] = "Pulse";

void
on_switch_off(Context_t *ctx)
{
  CmapFader_set(ctx->cf);
}

#define PMIN(X,Y) ((X < Y) ? X : Y)

/* FIXME empirical settings, have fun understanding how this works :) */
#define K 20
#define L 6 //8


void
run(Context_t *ctx)
{
  static short last_beat = -1;
  float somme = 0;
  short val = 0;
  u_char i;

  /* FIXME
     original code:

     for (u_char i = 1; i <= 4; i++)
     somme += log (sqrt (audio.power_spectrum[AUDIO_LEFT][i]) + 1.0) / log (2.0);

     a mon avis ca pecho pas les bonnes datas dans le spectrum
  */

  for (i = 1; i <= L; i++)
    //somme += ctx->input->spectrum_log_norme[A_MONO][i];
  {
    somme += ctx->input->spectrum[A_MONO][i];
  }
  somme /= L;

  /* FIXME */
  /*
    if (somme < log_min_norme_2)
    somme = log_min_norme_2;
    else
    if (somme > log_max_norme_2)
    somme = log_max_norme_2;
  */
  /* tentative de fix */
  /* log_max_norme_2 c'etait un mauvais nom, log_max_norme plutot ? */
  // printf("somme1= %f ", somme);

#ifdef XDEBUG
  if ((somme < 0) || (somme > ctx->input->max_spectrum[A_MONO])) {
    printf("[!] pulse: strange somme= %f\n", somme);
  }
#endif

  /*
    if (somme < 0)
    somme = 0;
    else
    if (somme > ctx->input->max_spectrum_log_norme[A_MONO])
    somme = ctx->input->max_spectrum_log_norme[A_MONO];
  */
  // printf("somme2= %f ", somme);

  val = (short)(somme * K);
  // printf ("val= %d last_beat= %d\n", val, last_beat);

  if (val != last_beat) {
    u_short i;
    short l;

    last_beat = val;
    i = 0;
    l = val;

    for ( ; ((i < 256) && (val > 0)); (i++, l -= K)) {
      ctx->cf->cur->colors[i].col.r =
        PMIN((u_short)(ctx->cf->dst->colors[i].col.r + l), (u_short)(255));
      ctx->cf->cur->colors[i].col.g =
        PMIN((u_short)(ctx->cf->dst->colors[i].col.g + l), (u_short)(255));
      ctx->cf->cur->colors[i].col.b =
        PMIN((u_short)(ctx->cf->dst->colors[i].col.b + l), (u_short)(255));
    }

    for ( ; i < 256; i++) {
      // printf("zouuuuuuuu %d\n", i);
      ctx->cf->cur->colors[i].col.r = ctx->cf->dst->colors[i].col.r;
      ctx->cf->cur->colors[i].col.g = ctx->cf->dst->colors[i].col.g;
      ctx->cf->cur->colors[i].col.b = ctx->cf->dst->colors[i].col.b;
    }

    ctx->cf->refresh = 1;
  }
}

