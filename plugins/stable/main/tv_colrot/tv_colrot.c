/*
 *  Copyright 1994-2020 Olivier Girondel
 *  Copyright 2019-2020 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/* Colrot, Color Rotation

   basic idea of plugin: Show only part of the colors from webcam video,
   and scroll thru colors. Not visible colors are transparent,
   showing stuff happening under. Using lens mode.

   This plugin is 'normal', pretty agressive version of Colrot
*/


#include "context.h"

#define MASK_SIZE  15 /* what size of chunks colorspace is divided */
#define COLORCOUNT 5  /* how many colors are visible in each chunk */

uint32_t version = 0;
u_long options = BE_GFX|BE_LENS|BEQ_MUTE_CAM;
char desc[] = "Show webcam scrolling colors";
char dname[] = "TV colrot";

u_long mode = OVERLAY;

static Pixel_t min1= 0, max1 = COLORCOUNT;


void
run(Context_t *ctx)
{
  Pixel_t *src1, *start, *dst;

  dst = start = passive_buffer(ctx)->buffer;

  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  src1 = ctx->cam_save[ctx->cam][0]->buffer;
  for (; dst < start + BUFFSIZE * sizeof(Pixel_t); src1++, dst++) {
    if ( /* max1 is bigger than min, show values between them */
      ((max1 > min1) &&
       ((*src1 & MASK_SIZE) > min1) && ((*src1 & MASK_SIZE) < max1)) ||
      /* max is rotated over, show values below max or above min */
      ((max1 < min1) &&
       (((*src1 & MASK_SIZE)> min1) || ((*src1 & MASK_SIZE) < max1)))) {
      *dst = *src1;
    } else {
      *dst = 0;
    }
  }
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);
  min1++;
  max1++;
  if (min1 > MASK_SIZE) {
    min1 = 0;
  }
  if (max1 > MASK_SIZE) {
    max1 = 0;
  }
}
